require "Window"
require "GameLib"
require "GroupLib"
require "CColor"
require "ICCommLib"
require "Sound"
local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local ICCommLib = ICCommLib
local Event_FireGenericEvent = Event_FireGenericEvent
local GetCurrentZoneName = GetCurrentZoneName
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local pairs = pairs
local ipairs = ipairs
local table = table
local type = type
local tonumber = tonumber
local string = string
local CColor = CColor
local os = os

local Ftr = {}
local fVersion = 0.4
function Ftr:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
	self.Ftr = self
	self.uPlayer = GameLib.GetPlayerUnit()
	self.ModuleRunning = false
	self.tWarnings = {}
	self.tTimers = {}
	self.Modules = {}
	self.wWindows = {}
	self.tHooks = {}
	self.tLoadConditions = {}
	self.tStartConditions = {}
	self.tStopConditions = {}
	self.tClients = {}
    return o
end

function Ftr:Init()
    Apollo.RegisterAddon(self, true, "Ftr", {})
end

function Ftr:OnLoad()
	self.ClassIcons =
	{
		[GameLib.CodeEnumClass.Warrior] 			= "Icon_Windows_UI_CRB_Warrior",
		[GameLib.CodeEnumClass.Engineer] 			= "Icon_Windows_UI_CRB_Engineer",
		[GameLib.CodeEnumClass.Esper]				= "Icon_Windows_UI_CRB_Esper",
		[GameLib.CodeEnumClass.Medic]				= "Icon_Windows_UI_CRB_Medic",
		[GameLib.CodeEnumClass.Stalker] 			= "Icon_Windows_UI_CRB_Stalker",
		[GameLib.CodeEnumClass.Spellslinger]	 	= "Icon_Windows_UI_CRB_Spellslinger"
	}
	self.ClassColors =
	{
		[GameLib.CodeEnumClass.Warrior] 			= "FFFF7272",
		[GameLib.CodeEnumClass.Engineer] 			= "FFFFA740",
		[GameLib.CodeEnumClass.Esper]				= "FF31B9FF",
		[GameLib.CodeEnumClass.Medic]				= "FFFFDB34",
		[GameLib.CodeEnumClass.Stalker] 			= "FFAE46FF",
		[GameLib.CodeEnumClass.Spellslinger]	 	= "FFC0FFA0"
	}
	self.RoleIcons =
	{
		[GameLib.CodeEnumClass.Warrior] 			= "Icon_Windows_UI_CRB_Warrior",
		[GameLib.CodeEnumClass.Engineer] 			= "Icon_Windows_UI_CRB_Engineer",
		[GameLib.CodeEnumClass.Esper]				= "Icon_Windows_UI_CRB_Esper",
		[GameLib.CodeEnumClass.Medic]				= "Icon_Windows_UI_CRB_Medic",
		[GameLib.CodeEnumClass.Stalker] 			= "Icon_Windows_UI_CRB_Stalker",
		[GameLib.CodeEnumClass.Spellslinger]	 	= "Icon_Windows_UI_CRB_Spellslinger"
	}
	self.tColor = {
		yellow = self:hexToCColor("fff600"),
		orange = self:hexToCColor("feb408"),
		red = self:hexToCColor("c6002a"),
		green = self:hexToCColor("01a825"),
		blue = self:hexToCColor("00b0d8"),
		white = self:hexToCColor("ffffff", 0.6)
	}

	-- ----------------------
	-- Load Libraries
	-- ----------------------
	self.FtrLibModule = Apollo.GetPackage("Ftr:Libs:Module").tPackage
	self.FtrRaidwarning = Apollo.GetPackage("Ftr:Libs:Raidwarning").tPackage
	self.FtrLibHook = Apollo.GetPackage("Ftr:Libs:Hook").tPackage
	self.FtrLibDraw = Apollo.GetPackage("Ftr:Libs:Draw").tPackage
	self.FtrLibUnit = Apollo.GetPackage("Ftr:Libs:Unit").tPackage
	self.FtrLibUnitbox = Apollo.GetPackage("Ftr:Libs:Unitbox").tPackage
	self.Unitbox = self.FtrLibUnitbox.create(self)

	-- ----------------------
	-- Load Forms
	-- ----------------------
	self.wRaidwarningWindow = Apollo.LoadForm("Ftr.xml", "RaidwarningWindow", nil, self)
	self.wRaidwarningWindow:FindChild("WarningWrapper"):DestroyChildren()
	self.wRaidwarningWindow:Show(true)

	self.wBigTimerWindow = Apollo.LoadForm("Ftr.xml", "BigTimerWindow", nil, self)
	self.wBigTimerWindow:FindChild("WarningWrapper"):DestroyChildren()
	self.wBigTimerWindow:Show(true)

	self.wSmallTimerWindow = Apollo.LoadForm("Ftr.xml", "SmallTimerWindow", nil, self)
	self.wSmallTimerWindow:FindChild("WarningWrapper"):DestroyChildren()
	self.wSmallTimerWindow:Show(true)

	if not self.tSave then
		self:UseDefaultOptions(self)
	end

	-- Load Sprites
	Apollo.LoadSprites("FtrSprites.xml", "FtrSprites")

	-- ----------------------
	-- Register Handlers
	-- ----------------------
	Apollo.RegisterSlashCommand("ftrunlock", "OnUnlock", self)
	Apollo.RegisterSlashCommand("ftrtest", "OnTest", self)
	Apollo.RegisterSlashCommand("ftr", "OnSlashcommand", self)
	Apollo.RegisterSlashCommand("rw", "ShowWarning", self)

	self:RegisterEventHandler("Perm", "InterfaceMenuListHasLoaded", "OnInterfaceMenuListHasLoaded")
	-- self:RegisterEventHandler("Perm", "Group_Join", "OnGroupJoin")
	-- self:RegisterEventHandler("Perm", "Group_Left", "OnGroupLeft")
	-- self:RegisterEventHandler("Perm", "Group_Updated", "OnGroupUpdated")
	self:HookAll("Perm")

	self:RegisterEventHandler("Running", "Timer", "On1SecTimer")
	self:RegisterEventHandler("Running", "Timer", "OnFtrBarUpdate", {0.033, true})
	self:RegisterEventHandler("Running", "Timer", "OnWipeCheckTimer", {0.5, true})

	-- Load Conditions
	if self.tLoadConditions then
		for strEvent,tEvents in pairs(self.tLoadConditions) do -- for each event
			for j,tCondition in pairs(tEvents) do -- for each condition
					self:RegisterLoadCondition(strEvent, unpack(tCondition))
					self:HookAll({tCondition[1], "Load"})
			end
		end
	end
	-- fire subzone change for current subzone
	Event_FireGenericEvent("SubZoneChanged", 10, GetCurrentZoneName())
	-- if we are in combat, fire combat enter for self
	if GameLib.GetPlayerUnit():IsInCombat() then
		Event_FireGenericEvent("UnitEnteredCombat", GameLib.GetPlayerUnit(), true)
	end
end

function Ftr:GetName()
	return "Ftr"
end

function Ftr:__GetName()
	return "Ftr"
end

function Ftr:IsRunning()
	return self.ModuleRunning
end

function Ftr:GetEnemyUnits()
	return self.Unitbox:GetEnemies()
end
-- --------------------------------------------------------------
-- ------------------ MODULE FUNCTIONS --------------------------
-- --------------------------------------------------------------
function Ftr:LoadModule(strModule)
	if self:GetModule(strModule) then return end
	local Module = Apollo.GetPackage(strModule).tPackage
	if Module then
		Module:__Init()
		if Module["OnLoad"] then Module:OnLoad() end
		self.Modules[#self.Modules+1] = Module
	else
		Print(("Ftr: Error loading Module %s."):format(strModule))
	end
end

function Ftr:StartModule(mod)
	local Module = self:GetModule(mod)
	if Module then
		if not Module:__IsRunning() then
			self:UnhookAll({Module:__GetName(), "Start"})
			self.Unitbox:Start()
			Module:__StartModule()
			self:HookAll("Running")
			self:HookAll({Module:__GetName(), "Stop"})
			self.ModuleRunning = true
		end
		return Module
	else
		Print("Requested Module not found.")
	end
	return false
end

function Ftr:ResumeModule(mod, tData)
	if self.tLoadConditions then
		for strEvent,tEvents in pairs(self.tLoadConditions) do -- for each event
			for j,tCondition in pairs(tEvents) do -- for each condition
					if tCondition[1]==mod then self:LoadModule(tCondition[1]) end
			end
		end
	end
	local Module = self:GetModule(mod)
	if Module then
		if not Module:__IsRunning() then
			self:UnhookAll({Module:__GetName(), "Start"})
			Module:__SetStatus(tData)
			self:HookAll("Running")
			self.ModuleRunning = true
		end
		return Module
	else
		Print("Requested Module not found.")
	end
	return false
end

function Ftr:StopModule(mod)
	local Module = self:GetModule(mod)
	if Module then
		if Module:__IsRunning() then
			Module:__StopModule()
		end
	end
	self:SafeStop()
end

function Ftr:SafeStop()
	local stillrunning = false
	for k, v in pairs(self.Modules) do
		if v:__IsRunning() then stillrunning = true end
	end
	if not stillrunning then
		self:UnhookAll("Running")
		self:UnhookAll("Wipe")
		self.Unitbox:Stop()
		self.ModuleRunning = false
		-- Rehook Start Conditions
		self:RehookConditions()
	end
end

function Ftr:RehookConditions()
	-- rehook start hooks
	for k,v in pairs(self.Modules) do
		self:UnhookAll({v:__GetName(), "Stop"})
		self:HookAll({v:__GetName(), "Start"})
	end
	if self.tLoadConditions then
		for strEvent,tEvents in pairs(self.tLoadConditions) do -- for each event
			for j,tCondition in pairs(tEvents) do -- for each condition
					if not self:GetModule(tCondition[1]) then self:HookAll({tCondition[1], "Load"}) end
			end
		end
	end
end

function Ftr:GetModule(Module)
	if Module then
		local strModuleName
		if(type(Module)=="string") then
			strModuleName = Module
		elseif(type(Module["__GetName"]) == "function" and type(Module:__GetName())=="string") then
			strModuleName = Module:__GetName()
		end
		if(strModuleName) then
			for k,v in pairs(self.Modules) do
				if v:__GetName() == strModuleName then return v end
			end
		end
	end
	return false
end

function Ftr:StopAllModules()
	for k, v in pairs(self.Modules) do
		if v and v:__IsRunning() then self:StopModule(v:__GetName()) end
	end
end

function Ftr:LoadAndStartModule(strModule)
	if not self:GetModule(strModule) then self:LoadModule(strModule) end
	return self:StartModule(strModule)
end

function Ftr:RegisterModule(strPackName, tLoadConditions)
	if self:GetModule(strPackName) then
		while (self:GetModule(strPackName)) do
			strPackName = strPackName.."_1"
		end
	end

	-- create child class from Ftr:Libs:Module
	local FtrLibModule = Apollo.GetPackage("Ftr:Libs:Module").tPackage
    local new_class = {}
    local class_mt = { __index = new_class }
    function new_class.create()
        local newinst = {}
        setmetatable( newinst, class_mt )
        return newinst
    end
    if FtrLibModule then
        setmetatable( new_class, { __index = FtrLibModule } )
    end
    Apollo.RegisterPackage(new_class, strPackName, 1, {})
    new_class["__GetName"] = function(self) return strPackName end

    -- check for load conditions
	if (type(tLoadConditions) == "table") then -- register load conditions
		if (type(tLoadConditions[1])) == "string" then --only one condition
			local strEvent = tLoadConditions[1] local args = {}
			for k,v in ipairs(tLoadConditions) do
				if(k>1) then args[(k-1)] = v end
			end
			if not self.tLoadConditions[strEvent] then
				self.tLoadConditions[strEvent] = {}
			end
			self.tLoadConditions[strEvent][#self.tLoadConditions[strEvent]+1] = {strPackName, unpack(args)}
		elseif (type(tLoadConditions[1])) == "table" then -- multiple start conditions
			for i, cond in ipairs(tLoadConditions) do
				if (type(cond[1])) == "string" then
					local strEvent = cond[1] local args = {}
					for k,v in ipairs(cond) do
						if(k>1) then args[(k-1)] = v end
					end
					if not self.tLoadConditions[strEvent] then
						self.tLoadConditions[strEvent] = {}
					end
					self.tLoadConditions[strEvent][#self.tLoadConditions[strEvent]+1] = {strPackName, unpack(args)}
				end
			end
		end
	end

    return new_class
end

function Ftr:RegisterLoadCondition(strEvent, strModule, ...)
	if not self[("CheckLoadOn".."_"..strEvent)] then
		self:RegisterEventHandler({strModule, "Load"}, strEvent, ("CheckLoadOn".."_"..strEvent), arg)

		self[("CheckLoadOn".."_"..strEvent)] = function(...)
			-- convert units into unitnames
			local strEvent = strEvent
			if type(arg)=="table" then
				for k=1, #arg do
					if ((type(arg[k])=="userdata") and (arg[k]["GetName"])) then arg[k]=arg[k]:GetName() end
				end
			end
			-- check once for each module thats not running
			self:CheckModuleLoadConditions(strEvent, unpack(arg))
		end
	end
end

function Ftr:CheckModuleLoadConditions(strEvent, ...)
	for k,v in pairs(self.tLoadConditions[strEvent]) do
		if (not self:GetModule(v[1])) then
			local bLoad = true
			for i=2, #v do
				if((type(arg[i]) ~= "nil") and (type(v[i]) ~= "nil") and (v[i] ~= arg[i]) and (v[i] ~= "%")) then
					bLoad = false
				end
			end
			if bLoad then
				self:LoadModule(v[1])
			end
		end
	end
end

function Ftr:RegisterStartCondition(strModule, strEvent, ...)
	if not self.tStartConditions[strEvent] then
		self.tStartConditions[strEvent] = {}
	end
	self.tStartConditions[strEvent][#self.tStartConditions[strEvent]+1] = {strModule, unpack(arg)}
	if not self[("CheckStartOn_%_"..strModule.."_%_"..strEvent)] then
		self:RegisterEventHandler({strModule, "Start"}, strEvent, ("CheckStartOn_%_"..strModule.."_%_"..strEvent))

		self[("CheckStartOn_%_"..strModule.."_%_"..strEvent)] = function(...)
			local strEvent = strEvent
			if type(arg)=="table" then
				for k=1, #arg do
					-- convert units into unitnames
					if ((type(arg[k])=="userdata") and (arg[k]["GetName"])) then arg[k]=arg[k]:GetName() end
				end
			end
			self:CheckModuleStartConditions(strEvent, unpack(arg))
		end
	end
end

function Ftr:RegisterStopCondition(strModule, strEvent, ...)
	if not self.tStopConditions[strEvent] then
		self.tStopConditions[strEvent] = {}
	end
	self.tStopConditions[strEvent][#self.tStopConditions[strEvent]+1] = {strModule, unpack(arg)}
	if not self[("CheckStopOn_%_"..strModule.."_%_"..strEvent)] then
		self:RegisterEventHandler({strModule, "Stop"}, strEvent, ("CheckStopOn_%_"..strModule.."_%_"..strEvent))

		self[("CheckStopOn_%_"..strModule.."_%_"..strEvent)] = function(...)
			local strEvent = strEvent
			if type(arg)=="table" then
				for k=1, #arg do
					-- convert units into unitnames
					if ((type(arg[k])=="userdata") and (arg[k]["GetName"])) then arg[k]=arg[k]:GetName() end
				end
			end
			self:CheckModuleStopConditions(strEvent, unpack(arg))
		end
	end
end

function Ftr:CheckModuleStartConditions(strEvent, ...)
	for k,v in pairs(self.tStartConditions[strEvent]) do
		local Module = self:GetModule(v[1])
		if (Module) and (not Module:__IsRunning()) then
			local bStart = true
			for i=2, #v do
				if((type(arg[i]) ~= "nil") and (type(v[i]) ~= "nil") and (v[i] ~= arg[i]) and (v[i] ~= "%")) then
					bStart = false
				end
			end
			if bStart then
				self:StartModule(v[1])
			end
		end
	end
end

function Ftr:CheckModuleStopConditions(strEvent, ...)
	for k,v in pairs(self.tStopConditions[strEvent]) do
		local Module = self:GetModule(v[1])
		if (Module) and (Module:__IsRunning()) then
			local bStop = true
			for i=2, #v do
				if((type(arg[i]) ~= "nil") and (type(v[i]) ~= "nil") and (v[i] ~= arg[i]) and (v[i] ~= "%")) then
					bStop = false
				end
			end
			if bStop then
				self:StopModule(v[1])
			end
		end
	end
end

function Ftr:LegacyCheckModuleStartConditions(Event, args)
	if Event=="UnitEnteredCombat" then
		if args[2] and args[1]:GetName() == "Experiment X-89" then
			self:LoadAndStartModule("Ftr:Experiment")
		end
		if args[2] and args[1]:GetName() == "Kuralak the Defiler" then
			self:LoadAndStartModule("Ftr:Kuralak")
		end
		if args[2] and args[1]:GetName() == "Phage Maw" then
			self:LoadAndStartModule("Ftr:Phagemaw")
		end
		if args[2] and args[1]:GetName() == "Phagetech Commander" then
			self:LoadAndStartModule("Ftr:Prototypes")
		end
		if args[2] and (args[1]:GetName() == "Golgox the Lifecrusher" or args[1]:GetName() == "Fleshmonger Vratorg" or args[1]:GetName() == "Ersoth Curseform"
			or args[1]:GetName() == "Terax Blightweaver" or args[1]:GetName() == "Noxmind the Insidious") then
			self:LoadAndStartModule("Ftr:Convergence")
		end
		if args[2] and args[1]:GetName() == "Dreadphage Ohmna" then
			self:LoadAndStartModule("Ftr:Ohmna")
		end
	end
end

-- --------------------------------------------------------------
-- ---------------- EVENT BASED FUNCTIONS -----------------------
-- --------------------------------------------------------------

function Ftr:OnTest()
	self:LoadAndStartModule("Ftr:Ohmna")
	-- Print("Registered Load Conditions:")
	-- if self.tLoadConditions then
	-- 	for strEvent,tEvents in pairs(self.tLoadConditions) do -- for each event
	-- 		Print("--- Event "..strEvent.." --- ")
	-- 		for j,tCondition in pairs(tEvents) do -- for each condition
	-- 				Print(j..": "..tCondition[2].." ("..tCondition[1]..")")
	-- 		end
	-- 	end
	-- end
	-- for k,v in pairs(self.tHooks["Ftr:Ohmna"]["Start"]) do
	-- 	Print(v:GetName())
	-- end
end

function Ftr:OnSlashcommand(strCmd, strArg)
	local tArgs = self:Split(strArg)
	if not tArgs then
		-- show options
		return true
	end

	if tArgs[1] == "timer" then -- add custom timer
		local dur
		if (type(tonumber(tArgs[2]))=="number") and (tonumber(tArgs[2])>0) then dur = tonumber(tArgs[2])
		else dur = 10 end
		local strText = ""
		if tArgs[3] then for i=3,#tArgs do strText = strText.." "..tArgs[i] end
		else strText="Countdown" end
		self:ShowTimer(dur, strText)
	elseif tArgs[1] == "rw" then -- show raidwarning
		local strText for i=2,#tArgs do strText = strText.." "..tArgs[i] end
		self:ShowWarning("rw", strText)
	elseif tArgs[1] == "readycheck" then -- show readycheck (with sound)
		Print("Not yet implemented")
	else -- if its a module name
		if self.tLoadConditions then
			for strEvent,tEvents in pairs(self.tLoadConditions) do -- for each event
				for j,tCondition in pairs(tEvents) do -- for each condition
					if tArgs[1] == tCondition[1] and tArgs[2]=="load" then
						self:LoadModule(tArgs[1])
						return true
					end
				end
			end
		end
		for k,v in pairs(self.Modules) do
			if v:__GetName() == tArgs[1] then
				if tArgs[2]=="start" then self:StartModule(v) return true
				elseif tArgs[2]=="stop" then self:StopModule(v) return true
				else -- forward to module
				end
			end
		end
	end
end

function Ftr:OnUnitEnteredCombat(uUnit, bInCombat)
	self:LegacyCheckModuleStartConditions("UnitEnteredCombat", {uUnit, bInCombat})
	if uUnit and not bInCombat then
		if uUnit == self.uPlayer then
			self:HookAll("Wipe")
		end
	end
end

function Ftr:On1SecTimer()
	local counter = 0 local reorder = false
	-- Modules
	for k, v in pairs(self.Modules) do
		if v:__IsRunning() then
			for i, t in pairs(v:__GetWarnings()) do
				counter = counter + 1
				if t:OnTick() then reorder = true end
				if t and t:IsExpired() then v:DestroyWarning(i) end
			end
			for i, t in pairs(v:__GetTimers()) do
				counter = counter + 1
				if t:OnTick() then reorder = true end
				if t and t:IsExpired() then v:DestroyTimer(i) end
			end
		end
	end
	-- Core Stuff
	for i, t in pairs(self.tWarnings) do
		counter = counter + 1
		if t:OnTick() then reorder = true end
		if t and t:IsExpired() then self.tWarnings[i]:Destroy() self.tWarnings[i]=nil end
	end
	for i, t in pairs(self.tTimers) do
		counter = counter + 1
		if t:OnTick() then reorder = true end
		if t and t:IsExpired() then self.tTimers[i]:Destroy() self.tTimers[i]=nil end
	end
	if reorder then
		self:OrderWindow(self.wRaidwarningWindow, true)
		self:OrderWindow(self.wBigTimerWindow)
		self:OrderWindow(self.wSmallTimerWindow)
	elseif counter < 1 then
		self:SafeStop()
	end
end

function Ftr:OnFtrBarUpdate()
	local reorder = false local counter = 0
	for k, v in pairs(self.Modules) do
		if v:__IsRunning() then
			for i, t in pairs(v:__GetWarnings()) do
				counter = counter + 1
				if t:OnBarUpdate() then reorder = true end
				if t and t:IsExpired() then v:DestroyWarning(i) end
			end
			for i, t in pairs(v:__GetTimers()) do
				counter = counter + 1
				if t:OnBarUpdate() then reorder = true end
				if t and t:IsExpired() then v:DestroyTimer(i) end
			end
		end
	end
	-- Core Stuff
	for i, t in pairs(self.tWarnings) do
		counter = counter + 1
		if t:OnBarUpdate() then reorder = true end
		if t and t:IsExpired() then self.tWarnings[i]:Destroy() self.tWarnings[i]=nil end
	end
	for i, t in pairs(self.tTimers) do
		counter = counter + 1
		if t:OnBarUpdate() then reorder = true end
		if t and t:IsExpired() then self.tTimers[i]:Destroy() self.tTimers[i]=nil end
	end
	if reorder then
		self:OrderWindow(self.wRaidwarningWindow, true)
		self:OrderWindow(self.wBigTimerWindow)
		self:OrderWindow(self.wSmallTimerWindow)
	elseif counter < 1 then
		self:SafeStop()
	end
end

function Ftr:OnWipeCheckTimer()
	if GroupLib.InGroup() then
		for i=1, GroupLib.GetMemberCount() do
			local uUnit = GroupLib.GetUnitForGroupMember(i)
			if uUnit and uUnit:IsInCombat() then return end
		end
	else
		if self.uPlayer:IsInCombat() then return end
	end
	--self:StopAllModules()
end

-- --------------------------------------------------------------
-- -------------------DISPLAY FUNCTIONS -------------------------
-- --------------------------------------------------------------
function Ftr:ShowWarning(strCmd, strText)
	local id=("RW_1")
	while(self.tWarnings[id]) do
		id = id.."_1"
	end

	self.tWarnings[id] = self.FtrRaidwarning.create(
		id,
		strText,
		5,
		{strType="warning"},
		"Raidwarning"
	)
	if not self:IsRunning() then
		self:HookAll("Running")
		self.ModuleRunning = true
	end
end

function Ftr:ShowTimer(duration, strText)
	local frame = "SmallTimer"
	if duration <= 5 then frame = "BigTimer" end
	local id="RT_1"
	while(self.tTimers[id]) do
		id = id.."_1"
	end

	self.tTimers[id] = self.FtrRaidwarning.create(
		id,
		strText,
		duration,
		{strType="countdown"},
		frame
	)
	if not self:IsRunning() then
		self:HookAll("Running")
		self.ModuleRunning = true
	end
end

-- --------------------------------------------------------------
-- ---------------- ---HELPER FUNCTIONS -------------------------
-- --------------------------------------------------------------
function Ftr:OrderWindow(wWnd, bAsc)
	-- get warnings into managable array
	local tWarnings = self:GetWarningsByWindow(wWnd)
	if tWarnings then
		local tWarnarray = {}
		for k,v in pairs(tWarnings) do
			tWarnarray[v:GetName()] = v:GetDuration()
		end
		-- sort array
		local tSortedWarnings = {}
		if bAsc then tSortedWarnings = self:OrderTable(tWarnarray, tWarnings, function(a,b) return a>b end)
		else tSortedWarnings = self:OrderTable(tWarnarray, tWarnings) end

		--move warnings into position
		local lastheight = 0
		for k,v in pairs(tSortedWarnings) do
			--get height
			local l,t,r,b = v:GetWindow():GetAnchorOffsets()
			local height = b
			v:GetWindow():SetAnchorOffsets(l, lastheight, r, (lastheight+(b-t) )	)
			lastheight = height
		end
	end
end

function Ftr:RegisterEventHandler(tSubtables, strEvent, strFunction, ...)
	local Module, args
	if(type(arg[2])~="nil") then Module = arg[1] args = arg[2]
	elseif(type(arg[1])=="nil") and (type(arg[2])=="nil") then Module = self args = {}
	elseif(type(arg[2])=="nil") and (not arg[1]["__GetName"]) then Module = self args=arg[1] end

	if type(tSubtables)=="string" then
		if (not self.tHooks[tSubtables]) then self.tHooks[tSubtables] = {} end
		self.tHooks[tSubtables][#self.tHooks[tSubtables]+1] = self.FtrLibHook.create(strEvent, strFunction, Module, args)
		if tSubtables=="Start" then self.tHooks[tSubtables][#self.tHooks[tSubtables]]:Hook() end

	elseif type(tSubtables)=="table" then
		if not self.tHooks[tSubtables[1]] then self.tHooks[tSubtables[1]] = {} end
		if not self.tHooks[tSubtables[1]][tSubtables[2]] then self.tHooks[tSubtables[1]][tSubtables[2]] = {} end
		self.tHooks[tSubtables[1]][tSubtables[2]][#self.tHooks[tSubtables[1]][tSubtables[2]]+1] = self.FtrLibHook.create(strEvent, strFunction, Module, args)
		if (tSubtables[1]=="Start") or (tSubtables[2]=="Start") then self.tHooks[tSubtables[1]][tSubtables[2]][#self.tHooks[tSubtables[1]][tSubtables[2]]]:Hook() end

	end
end

function Ftr:HookAll(tSubtables)
	-- fixes
	if type(tSubtables)=="string" and self.tHooks[tSubtables] then
		for _,hHook in pairs(self.tHooks[tSubtables]) do
			hHook:Hook()
		end
	elseif type(tSubtables)=="table" and self.tHooks[tSubtables[1]] and self.tHooks[tSubtables[1]][tSubtables[2]] then
		for _,hHook in pairs(self.tHooks[tSubtables[1]][tSubtables[2]]) do
			hHook:Hook()
		end
	end

	return true
end

function Ftr:UnhookAll(tSubtables)
	if type(tSubtables)=="string" and self.tHooks[tSubtables] then
		for _,hHook in pairs(self.tHooks[tSubtables]) do
			hHook:Unhook()
		end
	elseif type(tSubtables)=="table" and self.tHooks[tSubtables[1]] and self.tHooks[tSubtables[1]][tSubtables[2]]  then
		for _,hHook in pairs(self.tHooks[tSubtables[1]][tSubtables[2]]) do
			hHook:Unhook()
		end
	end

	return true
end

function Ftr:GetWarningsByWindow(wWnd)
	local tWarnings = {}
	local counter = 0
	for k,v in pairs(self.Modules) do
		for j,w in pairs(v:__GetWarnings()) do
			if w:GetWindow():GetParent():GetParent():GetName() == wWnd:GetName() then tWarnings[w:GetName()] = w counter=counter+1 end
		end
		for j,w in pairs(v:__GetTimers()) do
			if w:GetWindow():GetParent():GetParent():GetName() == wWnd:GetName() then tWarnings[w:GetName()] = w counter=counter+1 end
		end
	end
	for j,w in pairs(self.tWarnings) do
		if w:GetWindow():GetParent():GetParent():GetName() == wWnd:GetName() then tWarnings[w:GetName()] = w counter=counter+1 end
	end
	for j,w in pairs(self.tTimers) do
		if w:GetWindow():GetParent():GetParent():GetName() == wWnd:GetName() then tWarnings[w:GetName()] = w counter=counter+1 end
	end
	if counter > 0 then return tWarnings
	else return false end
end

function Ftr:IsPartyMember(uUnit)
	if (not GroupLib.InGroup() and uUnit == self.uPlayer) then return true
	else
		for i=1, GroupLib.GetMemberCount() do
			local tMemberInfo = GroupLib.GetGroupMember(i)
			if tMemberInfo.strCharacterName == uUnit:GetName() then return true end
		end
	end
	if (uUnit:GetName() == "Diminisherbot") or (uUnit:GetName() == "Artillerybot") or
	   (uUnit:GetName() == "Figment") or (uUnit:GetName() == "Phantom") or (uUnit:GetName() == "Geist") then return true end
	return false
end

function Ftr:GetMemberByName(strMemberName)
	if GroupLib.InGroup() and GroupLib.InRaid() then
		for i=1, GroupLib.GetMemberCount() do
			local uMember = GroupLib.GetUnitForGroupMember(i)
			local tMemberInfo = GroupLib.GetGroupMember(i)

			if tMemberInfo and tMemberInfo.strCharacterName==strMemberName then return uMember end
		end
	end
	return false
end

function Ftr:FindDebuffByName(uUnit, sBuff)
	if not uUnit:GetBuffs() then return false end
	local tBuffs = uUnit:GetBuffs().arHarmful
	if not tBuffs then return false end
	for k, v in pairs(tBuffs) do
		if v.splEffect:GetName():lower():find(sBuff:lower()) then
			return {v.splEffect:GetIcon(), v.splEffect:GetName(), v.fTimeRemaining, v.nCount, v.splEffect:GetId()}
		end
	end
	return false
end

function Ftr:hexToCColor(color, a)
	if not a then a = 1 end
	local r = tonumber(string.sub(color,1,2), 16) / 255
	local g = tonumber(string.sub(color,3,4), 16) / 255
	local b = tonumber(string.sub(color,5,6), 16) / 255
	return CColor.new(r,g,b,a)
end

function Ftr:spairs(t, order)
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

function Ftr:OrderTable(t, tWarnings, cmp)
    local sorted = {}
    local values = {}
    for k,v in pairs(t) do values[#values+1] = v end
    table.sort(values,cmp)
    for k,v in pairs(values) do
    	for j,w in pairs(t) do
    		if w==v then
    			sorted[#sorted+1] = tWarnings[j]
    			t[j]=nil
    		end
    	end
    end
    return sorted
end

function Ftr:Split(pString, pPattern)
	pPattern = pPattern or '%s+'
	local t = {}  -- NOTE: use {n = 0} in Lua-5.0
	local fpat = "(.-)" .. pPattern
	local last_end = 1
	local s, e, cap = pString:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= "" then
		table.insert(t,cap)
	end
	last_end = e+1
	s, e, cap = pString:find(fpat, last_end)
	end
	if last_end <= #pString then
		cap = pString:sub(last_end)
		table.insert(t, cap)
	end
	return t
end

-- --------------------------------------------------------------
-- -------------------- COMMUNICATION ---------------------------
-- --------------------------------------------------------------
function Ftr:OnComFtrHello(tMsg)
	-- register client
	self.tClients[tMsg.strSender] = tMsg.fVersion
	-- respond
	self:SendResponse()
	-- if a module is running, call resync
	for k, v in pairs(self.Modules) do
		if v:__IsRunning() then v:__SendStatus(tMsg.strSender) end
	end
end

function Ftr:OnComFtrHelloBack(tMsg)
	self.bGottenAnswer = true
	self.tClients[tMsg.strSender] = tMsg.fVersion
end

function Ftr:OnComFtrSndStatus(tMsg)
	if (tMsg.target==GameLib.GetPlayerUnit():GetName() and not self:GetModule(tMsg.strModule)) then
		self:ResumeModule(tMsg.strModule, tMsg.tData)
	end
end

function Ftr:OnMessage(channel, tMsg)
	if channel ~= self.CommChannel then return nil end
	if not tMsg.type then return false end

	if tMsg.strModule == "Ftr" then
		if   self[("OnCom"..tMsg.type)] then self[("OnCom"..tMsg.type)](self, tMsg)
		else self:__OnMessage(tMsg) end
	else
		local mod = self:GetModule(tMsg.strModule)
		if     mod and mod[("__OnCom"..tMsg.type)] then mod[("__OnCom"..tMsg.type)](mod, tMsg)
		elseif mod and mod[("OnCom"..tMsg.type)] then mod[("OnCom"..tMsg.type)](mod, tMsg)
		elseif mod then mod:__OnMessage(tMsg)
		elseif not mod then -- maybe ftr knows what to do with this one
			if self[("OnCom"..tMsg.type)] then self[("OnCom"..tMsg.type)](self, tMsg) end
		end
	end
end

function Ftr:SetGroupChannel(sGroupLeader)
	if GroupLib.InGroup then
		if not sGroupLeader then
			local MemberCount = GroupLib.GetMemberCount()
			if MemberCount == 1 then return end
			-- local i = 1
			-- while(not GroupLib.GetGroupMember(i).bIsLeader) do i=i+1 end
			-- sGroupLeader = GroupLib.GetGroupMember(i).strCharacterName
			for i=1, MemberCount do
				local MemberInfo = GroupLib.GetGroupMember(i)
				if MemberInfo.bIsLeader then
					sGroupLeader = MemberInfo.strCharacterName
					break
				end
			end
		end
		local sNewChannel = string.format("Ftr_%s", sGroupLeader)

		if self.sChannelName ~= sNewChannel then
			self.sChannelName = sNewChannel
			self.CommChannel = ICCommLib.JoinChannel(self.sChannelName, "OnMessage", self)
		end
	else self:LeaveGroupChannel()
	end
end

function Ftr:SendResponse()
	self:SendMessage({type="FtrHelloBack", strModule="Ftr", fVersion=self.version, strSender=GameLib.GetPlayerUnit():GetName()})
end

function Ftr:LeaveGroupChannel()
	self.sChannelName = ""
end

function Ftr:SendMessage(message)
	if self.CommChannel then
		--self.CommChannel:SendMessage(message)
	end
end

function Ftr:OnGroupJoin()
	local MemberCount = GroupLib.GetMemberCount()
	if MemberCount == 1 then return end

	local GroupLeader
	for i=1, MemberCount do
		local MemberInfo = GroupLib.GetGroupMember(i)
		if MemberInfo.bIsLeader then
			GroupLeader = MemberInfo.strCharacterName
			break
		end
	end

	self:SetGroupChannel(GroupLeader)
end

function Ftr:OnGroupLeft()
	self:LeaveGroupChannel()
end

function Ftr:OnGroupUpdated()
	self:OnGroupJoin()
	if not self.bGottenAnswer then
		self:SendMessage({type="FtrHello", strModule="Ftr", fVersion=fVersion, strSender=GameLib.GetPlayerUnit():GetName()})
	end
end

-- --------------------------------------------------------------
-- --------------------- FORM FUNCTIONS -------------------------
-- --------------------------------------------------------------
function Ftr:OnUnlock()
	self.tSave.bRaidwarningWindowLocked = false
	self.wRaidwarningWindow:FindChild("lblTitle"):Show(true)
	self.wRaidwarningWindow:FindChild("lblHelpText"):Show(true)
	self.wRaidwarningWindow:SetStyle("Moveable", true)
	self.wRaidwarningWindow:SetStyle("Sizable", true)
	self.wRaidwarningWindow:SetStyle("IgnoreMouse", false)

	self.tSave.bBigTimerWindowLocked = false
	self.wBigTimerWindow:FindChild("lblTitle"):Show(true)
	self.wBigTimerWindow:FindChild("lblHelpText"):Show(true)
	self.wBigTimerWindow:FindChild("Lock"):Show(true)
	self.wBigTimerWindow:SetStyle("Moveable", true)
	self.wBigTimerWindow:SetStyle("Sizable", true)
	self.wBigTimerWindow:SetStyle("IgnoreMouse", false)

	self.tSave.bSmallTimerWindowLocked = false
	self.wSmallTimerWindow:FindChild("lblTitle"):Show(true)
	self.wSmallTimerWindow:FindChild("lblHelpText"):Show(true)
	self.wSmallTimerWindow:SetStyle("Moveable", true)
	self.wSmallTimerWindow:SetStyle("Sizable", true)
	self.wSmallTimerWindow:SetStyle("IgnoreMouse", false)
end

function Ftr:OnInterfaceMenuListHasLoaded()
	Event_FireGenericEvent("InterfaceMenuList_NewAddOn", "Fun Time Raiding", {"ToggleFtrOptions", "", "FtrSprites:FtrMenuIcon32"})
end

function Ftr:OnWindowMove(wHandler)
	self.tSave.tRaidwarningWindowOffsets = {self.wRaidwarningWindow:GetAnchorOffsets()}
	self.tSave.tBigTimerWindowOffsets = {self.wBigTimerWindow:GetAnchorOffsets()}
	self.tSave.tSmallTimerWindowOffsets = {self.wSmallTimerWindow:GetAnchorOffsets()}
end

function Ftr:OnLockButton(wHandler)
	self.tSave.bRaidwarningWindowLocked = true
	self.wRaidwarningWindow:SetStyle("Moveable", false)
	self.wRaidwarningWindow:SetStyle("Sizable", false)
	self.wRaidwarningWindow:SetStyle("IgnoreMouse", true)
	self.wRaidwarningWindow:FindChild("lblTitle"):Show(false)
	self.wRaidwarningWindow:FindChild("lblHelpText"):Show(false)

	self.tSave.bBigTimerWindowLocked = true
	self.wBigTimerWindow:SetStyle("Moveable", false)
	self.wBigTimerWindow:SetStyle("Sizable", false)
	self.wBigTimerWindow:SetStyle("IgnoreMouse", true)
	self.wBigTimerWindow:FindChild("lblTitle"):Show(false)
	self.wBigTimerWindow:FindChild("lblHelpText"):Show(false)
	self.wBigTimerWindow:FindChild("Lock"):Show(false)

	self.tSave.bSmallTimerWindowLocked = true
	self.wSmallTimerWindow:SetStyle("Moveable", false)
	self.wSmallTimerWindow:SetStyle("Sizable", false)
	self.wSmallTimerWindow:SetStyle("IgnoreMouse", true)
	self.wSmallTimerWindow:FindChild("lblTitle"):Show(false)
	self.wSmallTimerWindow:FindChild("lblHelpText"):Show(false)
end

function Ftr:SaveWarning(tSaveData, nSafeIndex)
	--if not self.tSave[tSaveData[6]] then self.tSave[tSaveData[6]]= {} end
	--if not self.tSave[tSaveData[6]][tSaveData[5]:GetName()]	then self.tSave[tSaveData[6]][tSaveData[5]:GetName()] = {} end

	--if not nSafeIndex or nSafeIndex==-1 then index = #self.tSave[tSaveData[6]][tSaveData[5]:GetName()]+1
	--else index = nSafeIndex end
	--tSaveData[4]["nSafeIndex"] = index
	--self.tSave[tSaveData[6]][tSaveData[5]:GetName()][index] = tSaveData

	--return index
	return 0
end

function Ftr:OnSave(eType)
	-- if eType ~= GameLib.CodeEnumAddonSaveLevel.Character then return end
	local tSave = self.tSave
	--Save Anchors
	tSave.tRaidwarningWindowOffsets = {self.wRaidwarningWindow:GetAnchorOffsets()}
	tSave.bRaidwarningWindowLocked = self.tSave.bRaidwarningWindowLocked

	tSave.tBigTimerWindowOffsets = {self.wBigTimerWindow:GetAnchorOffsets()}
	tSave.bBigTimerWindowLocked = self.tSave.bBigTimerWindowLocked

	tSave.tSmallTimerWindowOffsets = {self.wSmallTimerWindow:GetAnchorOffsets()}
	tSave.bSmallTimerWindowLocked = self.tSave.bSmallTimerWindowLocked

	return tSave
end

function Ftr:OnRestore(eType, tSave)
	-- if eType ~= GameLib.CodeEnumAddonSaveLevel.Character then return end

	self.tSave = tSave

	-- ----------------------
	-- Load Settings
	-- ----------------------
	if not self.tSave then
		self:UseDefaultOptions(self)
	end
	self.wRaidwarningWindow:SetAnchorOffsets(unpack(self.tSave.tRaidwarningWindowOffsets))
	if self.tSave.bRaidwarningWindowLocked then
		self.wRaidwarningWindow:SetStyle("Moveable", false)
		self.wRaidwarningWindow:SetStyle("Sizable", false)
		self.wRaidwarningWindow:SetStyle("IgnoreMouse", true)
		self.wRaidwarningWindow:FindChild("lblHelpText"):Show(false)
		self.wRaidwarningWindow:FindChild("lblTitle"):Show(false)
	end

	self.wBigTimerWindow:SetAnchorOffsets(unpack(self.tSave.tBigTimerWindowOffsets))
	if self.tSave.bBigTimerWindowLocked then
		self.wBigTimerWindow:SetStyle("Moveable", false)
		self.wBigTimerWindow:SetStyle("Sizable", false)
		self.wBigTimerWindow:SetStyle("IgnoreMouse", true)
		self.wBigTimerWindow:FindChild("lblHelpText"):Show(false)
		self.wBigTimerWindow:FindChild("lblTitle"):Show(false)
		self.wBigTimerWindow:FindChild("Lock"):Show(false)
	end

	self.wSmallTimerWindow:SetAnchorOffsets(unpack(self.tSave.tSmallTimerWindowOffsets))
	if self.tSave.bSmallTimerWindowLocked then
		self.wSmallTimerWindow:SetStyle("Moveable", false)
		self.wSmallTimerWindow:SetStyle("Sizable", false)
		self.wSmallTimerWindow:SetStyle("IgnoreMouse", true)
		self.wSmallTimerWindow:FindChild("lblHelpText"):Show(false)
		self.wSmallTimerWindow:FindChild("lblTitle"):Show(false)
	end
end

function Ftr:UseDefaultOptions(self)
	--Print("Default Settings loaded")
	self.tSave = {}
	self.tSave.tRaidwarningWindowOffsets = {self.wRaidwarningWindow:GetAnchorOffsets()}
	self.tSave.bRaidwarningWindowLocked = false
	self.tSave.tBigTimerWindowOffsets = {self.wBigTimerWindow:GetAnchorOffsets()}
	self.tSave.bBigTimerWindowLocked = false
	self.tSave.tSmallTimerWindowOffsets = {self.wSmallTimerWindow:GetAnchorOffsets()}
	self.tSave.bSmallTimerWindowLocked = false
end

-----------------------------------------------------------------------------------------------
-- FunTimeRaiding Instance
-----------------------------------------------------------------------------------------------
local FtrInst = Ftr:new()
Ftr:Init()