local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local Sound = Sound
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local pairs = pairs
local ipairs = ipairs
local table = table
local os = os
local type = type
local string = string
local tonumber = tonumber
local tostring = tostring
-- --------------------------------------------------------------
-- ------------------ INITIALIZATION -------------------------
-- --------------------------------------------------------------
local FtrLibRaidwarning = {}
FtrLibRaidwarning.__index = FtrLibRaidwarning

function FtrLibRaidwarning.create(strName, strText, fDuration, tOptions, windowArgs)
	local self = setmetatable({}, FtrLibRaidwarning)
	Apollo.LoadSprites("FtrIconSprites.xml", "FtrIconSprites")
	Apollo.LoadSprites("FtrBarSprites.xml", "FtrBarSprites")

	self.Ftr = Apollo.GetAddon("FunTimeRaiding")
	self.tWindowTypes = {
		Raidwarning= {
			owner=self.Ftr,
			wParent=self.Ftr.wRaidwarningWindow:FindChild("WarningWrapper"),
			strXmlName="Ftr.xml",
			strFormName="RaidwarningWindow.WarningWrapper.Warning"
		},
		BigTimer= {
			owner=self.Ftr,
			wParent=self.Ftr.wBigTimerWindow:FindChild("WarningWrapper"),
			strXmlName="Ftr.xml",
			strFormName="BigTimerWindow.WarningWrapper.Warning"
		},
		SmallTimer= {
			owner=self.Ftr,
			wParent=self.Ftr.wSmallTimerWindow:FindChild("WarningWrapper"),
			strXmlName="Ftr.xml",
			strFormName="SmallTimerWindow.WarningWrapper.Warning"
		}
	}

	-- set values based on flags
	--[[
		------------------ Display Style -----------------
		strType("freeze", "countdown" or "countup"): type of timer
		strFont: font
		strTextColor: warning text color
		strBarEmptyColor: background color
		strBarFillColor: bar color
		strBarTexture: Bar Texture sprite
		strIcon: sprite for icon
		strTimerColor: color for timer text
		------------------ Options -----------------
		bSave: bool, whether the raidwarning should be saved
		bKeepAlive: bool, whether the raidwarning should stay up after expiration
		bHideTimer: bool, will hide timer window if true
		bHide: bool, will hide the warning on creation
		bMute: bool, disables default sounds if set
		bCountforever: bool, override all stopping orders and keeps the timer counting
		------------------ Triggers -----------------
		onCreate: {function()} or {string [,{args}]} for member function
		onExpire: {function()} or {string [,{args}]} for member function
		onMove: {"Raidwarning"|"BigTimer"|"SmallTimer", "Raidwarning"|"BigTimer"|"SmallTimer", function()}
			 or {"Raidwarning"|"BigTimer"|"SmallTimer", "Raidwarning"|"BigTimer"|"SmallTimer", string [,{args}]} for member function
		onTimeLeft: {fTimeLeft, function()} or {fTimeLeft, string [,{args}]} for member function
		onTimeSpent: {fTimeLeft, function()} or {fTimeLeft, string [,{args}]} for member function
		onResume: {function()} or {string [,{args}]} for member function
		onPause: {function()} or {string [,{args}]} for member function
	]]
	self.tOptions = tOptions
	if not self.tOptions["strType"] 	then self.tOptions["strType"] 		= "countdown" end
	if not self.tOptions["nSafeIndex"] 	then self.tOptions["nSafeIndex"] 	= -1 end
	if not self.tOptions["nInstance"] 	then self.tOptions["nInstance"] 	= 1 end -- for showing count on recall

	-- Set Window
	self.windowArgs = windowArgs
	self.tWindow = self:GetWindowTable(windowArgs)
	self.Module = self.tWindow.owner

	self.strName = strName
	self.strText = strText
	if type(fDuration) ~= "number" then Print("Could not create Raidwarning - Invalid Timer Value given") return nil end
	self.fDuration = fDuration
	self.fLastClock = os.clock()

	-- load form
	self.wnd = Apollo.LoadForm(self.tWindow.strXmlName, self.tWindow.strFormName, self.tWindow.wParent, self.tWindow.owner)
	self.wnd:Show(false)
	self.wnd:SetText("")
	self:SetText(self.strText)
	self:SetTimerText(("+%ss"):format(self.fDuration))

	-- save original data for saving and recalls
	self.SaveData = {self.strName, self.strText, self.fDuration, self.tOptions, self.tWindow}
	-- formating
	self:FormatWarning(self.tOptions)

	-- set triggers
	self.tTriggers = self:ParseTriggers(self.tOptions)
	self.tTriggers = self:SetDefaultSounds(self.tTriggers)
	self:OnCreate()

	-- Show Raidwarning and reorder Warnings in Window
	if not self.tOptions["bHide"] then
		if self.tOptions["strType"] == "warning" then self:OnShowRaidwarning() end
		self.wnd:Show(true)
	else self.wnd:Show(false) end

	-- saving
	if self.tOptions["bSave"] then
		self.tOptions["nSafeIndex"] = self.Ftr:SaveWarning(self.SaveData, self.tOptions["nSafeIndex"])
	end

	return self
end

-- --------------------------------------------------------------
-- ------------------ SET FUNCTIONS -------------------------
-- --------------------------------------------------------------

function FtrLibRaidwarning:SetText(strText)
	self.strText = strText
	if self.wnd:FindChild("lblText") then self.wnd:FindChild("lblText"):SetText(strText) end
end

function FtrLibRaidwarning:SetTimerText(strText)
	if self.wnd:FindChild("lblTimer") then
		self.wnd:FindChild("lblTimer"):SetText(strText)
	end
end

function FtrLibRaidwarning:SetTimer(fSeconds)
	if self.wnd:FindChild("lblTimer") and not self.tOptions["bHideTimer"] then
		self.fDuration = fSeconds
		self.wnd:FindChild("lblTimer"):SetText(fSeconds)
	end
end

function FtrLibRaidwarning:SetFont(strFont)
	if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetFont(strFont) end
	if self.wnd:FindChild("lblText") then self.wnd:FindChild("lblText"):SetFont(strFont) end
end

function FtrLibRaidwarning:SetTextColor(strTextColor)
	if self.wnd:FindChild("lblText") then self.wnd:FindChild("lblText"):SetTextColor(strTextColor) end
end

function FtrLibRaidwarning:SetTimerColor(strTimerColor)
	if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetTextColor(strTimerColor) end
end

function FtrLibRaidwarning:SetIcon(strIcon)
	if self.wnd:FindChild("wIcon") then self.wnd:FindChild("wIcon"):SetSprite(strIcon) end
end

-- --------------------------------------------------------------
-- ------------------ GET FUNCTIONS -------------------------
-- --------------------------------------------------------------

function FtrLibRaidwarning:GetSaveData()
	return self.SaveData
end

function FtrLibRaidwarning:GetName()
	return self.strName
end

function FtrLibRaidwarning:GetDuration()
	return self.fDuration
end

function FtrLibRaidwarning:GetTimer()
	if self.wnd:FindChild("lblTimer") then return self.wnd:FindChild("lblTimer"):GetText() end
end

function FtrLibRaidwarning:GetWindowTable(windowArgs, bUnpack)
	if type(windowArgs)=="string" then
		if self.tWindowTypes[windowArgs] then
			if bUnpack then return unpack(self.tWindowTypes[windowArgs])
			else return self.tWindowTypes[windowArgs] end
		end
	elseif type(windowArgs)=="table" and windowArgs[1] and windowArgs[2] and windowArgs[3] and windowArgs[4] then
		local tWindow = {
			owner = windowArgs[1],
			wParent = windowArgs[2],
			strXmlName = windowArgs[3],
			strFormName = windowArgs[4]
		}
		-- try to create testwindow with these arguments
		local wnd = Apollo.LoadForm(tWindow.strXmlName, tWindow.strFormName, tWindow.wParent, tWindow.owner)
		if wnd then
			wnd:Show(false)
			wnd:Destroy()
			if bUnpack then return unpack(tWindow)
			else return tWindow end
		end
	elseif self.tOptions["strType"]=="warning" then -- if type is warning, fallback on raidwarning
		local tWindow = {}
		tWindow.owner = self.Ftr
		tWindow.wParent = self.Ftr.wRaidwarningWindow:FindChild("WarningWrapper")
		tWindow.strXmlName = "Ftr.xml"
		tWindow.strFormName = "RaidwarningWindow.WarningWrapper.Warning"
		if bUnpack then return unpack(tWindow)
		else return tWindow end
	else -- else fallback on SmallTimer
		local tWindow = {}
		tWindow.owner = self.Ftr
		tWindow.wParent = self.Ftr.wSmallTimerWindow:FindChild("WarningWrapper")
		tWindow.strXmlName = "Ftr.xml"
		tWindow.strFormName = "SmallTimerWindow.WarningWrapper.Warning"
		if bUnpack then return unpack(tWindow)
		else return tWindow end
	end
	return false
end

function FtrLibRaidwarning:GetWindow()
	return self.wnd
end

function FtrLibRaidwarning:IsFrozen()
	if self.strType == "freeze" then return true
	else return false
	end
end

function FtrLibRaidwarning:IsPaused()
	if self.bPaused then return true
	else return false end
end

function FtrLibRaidwarning:IsShown()
	return self.bShown
end

function FtrLibRaidwarning:IsMatchingWindow(w1, w2)
	if (type(w1)=="string" and w1=="all") or (type(w2)=="string" and w2=="all") then return true end
	if (type(w1)=="string") and (type(w2)=="string") and w1==w2 then return true end

	-- set table if window is in string form
	if type(w1)=="string" then w1 = self:GetWindowTable(w1) end
	if type(w2)=="string" then w2 = self:GetWindowTable(w1) end

	-- iterate window tables
	for k1,v1 in pairs(w1) do
		for k2, v2 in pairs(w2) do
			if k1 ~= k2 or v1~=v2 then return false end
		end
	end

	return true
end

function FtrLibRaidwarning:IsExpired()
	if not self.tOptions.bKeepAlive then return self.Expired
	else return false end
end

-- --------------------------------------------------------------
-- ------------------ TRIGGER FUNCTIONS -------------------------
-- --------------------------------------------------------------

function FtrLibRaidwarning:OnTimeLeft(fSeconds)
	if not self.tTriggers["OnTimeLeft"] then return end
	for k, v in pairs(self.tTriggers["OnTimeLeft"]) do
		if (not v.bTriggered) and ((type(v[1])=="string" and v[1]=="all") or ((type(v[1])=="number") and (fSeconds <= v[1]))) then
			if (type(v) == "table") then
				if (type(v[2]) == "function") then
					if v[3] then -- with arguments
						if type(v[3]) == "table" then v[2](unpack(v[3]))
						else v[2](v[3]) end
					else --without arguments
						v[2]()
					end
					v["bTriggered"] = true
				elseif type(v[2]) == "string" and type(self[v[2]]) == "function" then --member function
					if v[3] then -- with arguments
						if type(v[3]) == "table" then
							self[v[2]](self, unpack(v[3]))
						else
							self[v[2]](self, v[3])
						end
					else --without arguments
						self[v[2]](self)
					end
					v["bTriggered"] = true
				end
			end
		end
	end
	return true
end

function FtrLibRaidwarning:OnTimeSpent(fSeconds)
	if not self.tTriggers["OnTimeSpent"] then return end
	for k, v in pairs(self.tTriggers["OnTimeSpent"]) do
		if (not v.bTriggered) and ((type(v[1])=="string" and v[1]=="all") or ((type(v[1])=="number") and (fSeconds >= v[1]))) then
			if (type(v) == "table") then
				if (type(v[2]) == "function") then
					if v[3] then -- with arguments
						if type(v[3]) == "table" then v[2](unpack(v[3]))
						else v[2](v[3]) end
					else --without arguments
						v[2]()
					end
					v["bTriggered"] = true
				elseif type(v[2]) == "string" and type(self[v[2]]) == "function" then --member function
					if v[3] then -- with arguments
						if type(v[3]) == "table" then
							self[v[2]](self, unpack(v[3]))
						else
							self[v[2]](self, v[3])
						end
					else --without arguments
						self[v[2]](self)
					end
					v["bTriggered"] = true
				end
			end
		end
	end
	return true
end

function FtrLibRaidwarning:OnMove(from, to)
	if not self.tTriggers["OnMove"] then return end
	for k, v in pairs(self.tTriggers["OnMove"]) do
		-- if table only has 1 or 2 arguments, assume 1=function or 1=function, 2=arguments
		if 	(not v.bTriggered) and (
				(type(v)=="string") or
				(type(v)=="function") or
				(not v[3]) or
				(not v[3] and not v[4]) or
				(self:IsMatchingWindow(from, v[1]) and self:IsMatchingWindow(to, v[2]))
			) then
			if (type(v) == "table") then
				-- if table only has 1 or 2 arguments, assume 1=function or 1=function, 2=arguments
				if not v[4] then
					if not v[2] then -- only function
						if type(v[1])=="function" then
							v[1]()
							v["bTriggered"] = true
						elseif type(v[1])=="string" and type(self[v[1]])=="function" then
							self[v[1]](self)
							v["bTriggered"] = true
						end
					else -- only function and arguments
						if type(v[1])=="function" then
							if type(v[2])=="table" then v[1](unpack(v[2]))
							else v[1](v[2]) end
							v["bTriggered"] = true
						elseif type(v[1])=="string" and type(self[v[1]])=="function" then
							if type(v[2])=="table" then self[v[1]](self, unpack(v[2]))
							else self[v[1]](self, v[2]) end
							v["bTriggered"] = true
						end
					end
				elseif type(v[3]) == "function" then
					if v[4] then -- with arguments
						if type(v[4]) == "table" then v[3](unpack(v[4]))
						else v[3](v[4]) end
					else --without arguments
						v[3]()
					end
					v["bTriggered"] = true
				elseif type(v[3]) == "string" then --member function
					if v[4] then -- with arguments
						if type(v[4]) == "table" then self[v[3]](self, unpack(v[4]))
						else self[v[3]](self, v[4]) end
					else --without arguments
						self[v[3]](self)
					end
					v["bTriggered"] = true
				end
			elseif (type(v) == "function") then
				v()
				v = {v, bTriggered=true}
			elseif (type(v) == "string") and type(self[v])=="function" then --member function
				Print("OnMove triggered")
				self[v](self)
				v = {v, bTriggered=true}
			end
		end
	end
	return true
end

function FtrLibRaidwarning:OnCreate()
	if not self.tTriggers["OnCreate"] then return end
	for k, v in pairs(self.tTriggers["OnCreate"]) do
		if (type(v) == "table") and not v.bTriggered then --member function
			if type(v[1]) == "function" then
				if v[2] and type(v[2])=="table" then v[1](unpack(v[2]))
				elseif v[2] then v[1](v[2])
				else v[1]() end
				v["bTriggered"] = true
			elseif type(v[1]) == "string" and type(self[v[1]])=="function" then
				if v[2] and type(v[2])=="table" then self[v[1]](self, unpack(v[2]))
				elseif v[2] then self[v[1]](self, v[2])
				else self[v[1]](self) end
				v["bTriggered"] = true
			end
		elseif (type(v) == "function") then
			v()
			v = {v, bTriggered=true}
		elseif (type(v) == "string") and type(self[v])=="function" then
			self[v](self)
			v = {v, bTriggered=true}
		end
	end
	return true
end

function FtrLibRaidwarning:OnPause()
	if not self.tTriggers["OnPause"] then return end
	for k, v in pairs(self.tTriggers["OnPause"]) do
		if (type(v) == "table") and not v.bTriggered then --member function
			if type(v[1]) == "function" then
				if v[2] and type(v[2])=="table" then v[1](unpack(v[2]))
				elseif v[2] then v[1](v[2])
				else v[1]() end
				v["bTriggered"] = true
			elseif type(v[1]) == "string" and type(self[v[1]])=="function" then
				if v[2] and type(v[2])=="table" then self[v[1]](self, unpack(v[2]))
				elseif v[2] then self[v[1]](self, v[2])
				else self[v[1]](self) end
				v["bTriggered"] = true
			end
		elseif (type(v) == "function") then
			v()
			v = {v, bTriggered=true}
		elseif (type(v) == "string") and type(self[v])=="function" then
			self[v](self)
			v = {v, bTriggered=true}
		end
	end
	return true
end

function FtrLibRaidwarning:OnResume()
	if not self.tTriggers["OnResume"] then return end
	for k, v in pairs(self.tTriggers["OnResume"]) do
		if (type(v) == "table") and not v.bTriggered then --member function
			if type(v[1]) == "function" then
				if v[2] and type(v[2])=="table" then v[1](unpack(v[2]))
				elseif v[2] then v[1](v[2])
				else v[1]() end
				v["bTriggered"] = true
			elseif type(v[1]) == "string" and type(self[v[1]])=="function" then
				if v[2] and type(v[2])=="table" then self[v[1]](self, unpack(v[2]))
				elseif v[2] then self[v[1]](self, v[2])
				else self[v[1]](self) end
				v["bTriggered"] = true
			end
		elseif (type(v) == "function") then
			v()
			v = {v, bTriggered=true}
		elseif (type(v) == "string") and type(self[v])=="function" then
			self[v](self)
			v = {v, bTriggered=true}
		end
	end
	return true
end

function FtrLibRaidwarning:OnShowRaidwarning()
	if not self.tTriggers["OnShowRaidwarning"] then return end
	for k, v in pairs(self.tTriggers["OnShowRaidwarning"]) do
		if (type(v) == "table") and not v.bTriggered then --member function
			if type(v[1]) == "function" then
				if v[2] and type(v[2])=="table" then v[1](unpack(v[2]))
				elseif v[2] then v[1](v[2])
				else v[1]() end
				v["bTriggered"] = true
			elseif type(v[1]) == "string" and type(self[v[1]])=="function" then
				if v[2] and type(v[2])=="table" then self[v[1]](self, unpack(v[2]))
				elseif v[2] then self[v[1]](self, v[2])
				else self[v[1]](self) end
				v["bTriggered"] = true
			end
		elseif (type(v) == "function") then
			v()
			v = {v, bTriggered=true}
		elseif (type(v) == "string") and type(self[v])=="function" then
			self[v](self)
			v = {v, bTriggered=true}
		end
	end
	return true
end

function FtrLibRaidwarning:OnExpire()
	self.Expired = true
	if not self.tTriggers["OnExpire"] then return end
	for k, v in pairs(self.tTriggers["OnExpire"]) do
		if (type(v) == "table") and not v.bTriggered then --member function
			if type(v[1]) == "function" then
				if v[2] and type(v[2])=="table" then v[1](unpack(v[2]))
				elseif v[2] then v[1](v[2])
				else v[1]() end
				v["bTriggered"] = true
			elseif type(v[1]) == "string" and type(self[v[1]])=="function" then
				if v[2] and type(v[2])=="table" then self[v[1]](self, unpack(v[2]))
				elseif v[2] then self[v[1]](self, v[2])
				else self[v[1]](self) end
				v["bTriggered"] = true
			end
		elseif (type(v) == "function") then
			v()
			v = {v, bTriggered=true}
		elseif (type(v) == "string") and type(self[v])=="function" then
			self[v](self)
			v = {v, bTriggered=true}
		end
	end
	return true
end

-- --------------------------------------------------------------
-- ------------------ TIMER FUNCTIONS -------------------------
-- --------------------------------------------------------------

function FtrLibRaidwarning:OnBarUpdate()
	if not self.wnd then return nil end
	if (not self.bHasProgressBar) then return nil end

	local strTimerText, roundedDuration, fSecondsLeft, fSecondsSpent
	if 	((self.tOptions["strType"] == "countdown" ) and
		((tonumber(self.tOptions["fStartAt"]) and tonumber(self.fDuration) <= tonumber(self.tOptions["fStartAt"]))
			or (not self.tOptions["fStartAt"] and tonumber(self.fDuration) <= tonumber(self.SaveData[3])) and
	  	((tonumber(self.tOptions["fStopAt"])  and tonumber(self.fDuration) >= tonumber(self.tOptions["fStopAt"]))
	  		or (not self.tOptions["fStopAt"]  and tonumber(self.fDuration) >= 0)))
	) then
		-- tick duration
		if not self.Paused then self.fDuration = self.fDuration - (os.clock()-self.fLastClock) end
		-- call OnExpire if Expired
		if (self.fDuration<=0) then self.fDuration=0 return self:OnExpire() end
		-- Update Timer
		self.wnd:SetProgress(100/self.SaveData[3]*(self.SaveData[3]-self.fDuration))
		if (self.fDuration) <= 10 then strTimerText = ("%.1f"):format(math.floor((self.fDuration)*10)*0.1)
		else strTimerText = ("%.0f"):format(math.floor(self.fDuration+0.5)) end
		if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetText(strTimerText) end

	elseif 	((self.tOptions["strType"] == "countup" ) and
			((self.tOptions["fStartAt"] and self.fDuration >= self.tOptions["fStartAt"]) or
			(not self.tOptions["fStartAt"])) and
			(self.tOptions["fStopAt"] and self.fDuration <= self.tOptions["fStopAt"])
	) then
		-- tick duration
		if not self.Paused then self.fDuration = self.fDuration + (os.clock()-self.fLastClock) end
		-- call OnExpire if Expired
		if (self.fDuration >= self.tOptions["fStopAt"]) then self.fDuration = self.tOptions["fStopAt"] return self:OnExpire() end
		-- Update Timer
		if (self.SaveData[3]+self.fDuration) <= self.tOptions["fStopAt"]-10 then roundedDuration = math.floor((self.SaveData[3]+self.fDuration)*10)*0.1
		else roundedDuration = math.floor(self.SaveData[3]+self.fDuration+0.5) end
		self.wnd:SetProgress(100/self.SaveData[3]*(self.SaveData[3]+self.fDuration))
		if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetText(("%ss"):format(roundedDuration)) end
	end
	-- If Bar wasnt drawn yet
	if (not self:IsShown()) and (not self.tOptions["bHide"]) then
		self:Show()
	end

	-- calculate seconds left
	if     (self.tOptions["strType"] == "countup" and self.tOptions["fStopAt"]) then fSecondsLeft = ((self.tOptions["fStopAt"] - self.fDuration) )
	elseif (self.tOptions["strType"] == "countdown" and self.tOptions["fStopAt"]) then fSecondsLeft = ((self.fDuration - self.tOptions["fStopAt"]))
	elseif (self.tOptions["strType"] == "countdown" and not self.tOptions["fStopAt"]) then fSecondsLeft = (self.fDuration)  end
	-- calculate seconds spent
	if     (self.tOptions["strType"] == "countup") then fSecondsSpent = (self.fDuration)
	elseif (self.tOptions["strType"] == "countdown") then fSecondsSpent = ((self.SaveData[3] - self.fDuration)) end
	-- call event handlers
	if fSecondsLeft then self:OnTimeLeft(fSecondsLeft) end
	if fSecondsSpent then self:OnTimeSpent(fSecondsSpent) end

	-- update last clock
	self.fLastClock = os.clock()
	return true
end

function FtrLibRaidwarning:OnTick()
	if not self.wnd then return nil end
	if (self.bHasProgressBar) then return nil end

	if 	((self.tOptions["strType"] == "countdown" ) and
		((self.tOptions["fStartAt"] and self.fDuration <= self.tOptions["fStartAt"]) or (not self.tOptions["fStartAt"] and self.fDuration <= self.SaveData[3])) and
	  	((self.tOptions["fStopAt"]  and self.fDuration >= self.tOptions["fStopAt"])  or (not self.tOptions["fStopAt"]  and self.fDuration >= 0))
	) then
		--tick timer
		if not self.Paused then self.fDuration = self.fDuration - (os.clock()-self.fLastClock) end
		if (self.fDuration<=0) then self.fDuration=0 return self:OnExpire() end
		self:SetTimerText(("+%ss"):format(math.floor(self.fDuration+0.5)))
	elseif(	 (self.tOptions["strType"] == "countup")
		 and ((self.tOptions["fStartAt"] and self.fDuration >= self.tOptions["fStartAt"]) or (not self.tOptions["fStartAt"]))
		 and ((self.tOptions["fStopAt"]  and self.fDuration <= self.tOptions["fStopAt"])  or (not self.tOptions["fStopAt"]))
		 and (self.fDuration ~= self.tOptions["fStopAt"])
	) then
		--tick timer
		if not self.Paused then self.fDuration = self.fDuration + (os.clock()-self.fLastClock) end
		if (self.tOptions["fStopAt"] and self.fDuration >= self.tOptions["fStopAt"]) then self.fDuration = self.tOptions["fStopAt"] return self:OnExpire() end
		self:SetTimerText(("+%ss"):format(math.floor(self.fDuration+0.5)))
	elseif (self.tOptions["strType"] == "warning" ) then
		if not self.Paused then self.fDuration = self.fDuration - (os.clock()-self.fLastClock) end
		if (self.fDuration<=0) then self.fDuration=0 return self:OnExpire() end
		self:SetTimerText(("+%ss"):format(math.floor(self.fDuration+0.5)))
	end

	-- calculate seconds left
	if self.tOptions["strType"] ~= "freeze" then
		local fSecondsLeft, fSecondsSpent
		if     (self.tOptions["strType"] == "countup" and self.tOptions["fStopAt"]) then fSecondsLeft = ((self.tOptions["fStopAt"] - self.fDuration) )
		elseif (self.tOptions["strType"] == "countdown" and self.tOptions["fStopAt"]) then fSecondsLeft = ((self.fDuration - self.tOptions["fStopAt"]))
		elseif (self.tOptions["strType"] == "countdown" and not self.tOptions["fStopAt"]) then fSecondsLeft = (self.fDuration)  end
		-- calculate seconds spent
		if     (self.tOptions["strType"] == "countup") then fSecondsSpent = (self.fDuration)
		elseif (self.tOptions["strType"] == "countdown") then fSecondsSpent = ((self.SaveData[3] - self.fDuration)) end
		-- call event handlers
		if fSecondsLeft then self:OnTimeLeft(fSecondsLeft) end
		if fSecondsSpent then self:OnTimeSpent(fSecondsSpent) end

		-- it not shown yet
		if (not self:IsShown()) and (not self.tOptions["bHide"]) then
			self:Show()
		end

		-- saving
		if self.tOptions["bSave"] then
			self.tOptions["nSafeIndex"] = self.self.Ftr:SaveWarning(self.SaveData, self.tOptions["nSafeIndex"])
		end
	end

	self.fLastClock = os.clock()
	return true
end

-- --------------------------------------------------------------
-- ------------------ CONTROL FUNCTIONS -------------------------
-- --------------------------------------------------------------

function FtrLibRaidwarning:MoveTo(windowArgs)
	if not windowArgs then windowArgs = "BigTimer" end
	-- Try to create new window
	local newtWindow
	newtWindow = self:GetWindowTable(windowArgs)
	local newwnd = Apollo.LoadForm(newtWindow.strXmlName, newtWindow.strFormName, newtWindow.wParent, newtWindow.owner)
	if not newwnd then Print("Could not move Timer!") return false end
	newwnd:Show(false)

	-- Worked, so destroy old window
	self.wnd:Show(false)
	self.wnd:Destroy()

	-- Trigger OnMove
	self:OnMove(self.tWindow, newtWindow)
	-- Format new window
	self.tWindow = newtWindow
	self.wnd = newwnd
	self.wnd:Show(false)
	self.wnd:SetText("")
	if self.SaveData[4].nInstance > 1 then self:SetText(self.strText.." #"..self.SaveData[4].nInstance)
	else self:SetText(self.strText) end
	self:SetTimerText(("+%ss"):format(self.fDuration))
	self:FormatWarning(self.tOptions)

	if not self.tOptions["bHide"] then
		if self.tOptions["strType"] == "warning" then self:OnShowRaidwarning() end
		self.wnd:Show(true)
	else self.wnd:Show(false) end
end

function FtrLibRaidwarning:Play(soundfile)
	Sound.Play(soundfile)
end

function FtrLibRaidwarning:Show()
	if self.wnd then
		if self.tOptions["strType"] == "freeze" then
			self:Autosize()
		end
		self.wnd:Show(true)
	end
	self.bShown = true
end

function FtrLibRaidwarning:Hide()
	if self.wnd then
		self.wnd:Show(false)
	end
	self.bShown = false
end

function FtrLibRaidwarning:Resume()
	self.Paused = false
	self:OnResume()
end

function FtrLibRaidwarning:Pause()
	self.Paused = true
	self:OnPause()
end

function FtrLibRaidwarning:Recall()
	self.Expired = false
	-- Destroy old window
	self.wnd:Show(false)
	self.wnd:Destroy()

	-- Set Window
	self.tWindow = self.SaveData[5]
	self.Module = self.tWindow.owner

	-- Set Variables
	self.strName = self.SaveData[1]
	self.strText = self.SaveData[2]
	self.fDuration = self.SaveData[3]
	self.fLastClock = os.clock()
	self.tTriggers = {}
	self.SaveData[4].nInstance = self.SaveData[4].nInstance +1
	self.tOptions = self.SaveData[4]

	-- load form
	self.wnd = Apollo.LoadForm(self.tWindow.strXmlName, self.tWindow.strFormName, self.tWindow.wParent, self.tWindow.owner)
	self.wnd:Show(false)
	self.wnd:SetText("")
	self.wnd:FindChild("lblText"):SetText(self.strText.." #"..self.tOptions.nInstance)
	if self.wnd:FindChild("lblTimer") then
		if self.tOptions["bHideTimer"] then self.wnd:FindChild("lblTimer"):Show(false) end
		self.wnd:FindChild("lblTimer"):SetText(("+%ss"):format(self.fDuration))
	end

	-- formating
	self:FormatWarning(self.tOptions)

	-- set triggers
	self.tTriggers = self:ParseTriggers(self.tOptions)
	self.tTriggers = self:SetDefaultSounds(self.tTriggers)
	self:OnCreate()

	-- Show Raidwarning and reorder Warnings in Window
	if not self.tOptions["bHide"] then
		if self.tOptions["strType"] == "warning" then self:OnShowRaidwarning() end
		self.wnd:Show(true)
	else self.wnd:Show(false) end

	-- saving
	if self.tOptions["bSave"] then
		self.tOptions["nSafeIndex"] = self.Ftr:SaveWarning(self.SaveData, self.tOptions["nSafeIndex"])
	end

	return self
end

function FtrLibRaidwarning:Destroy(override)
	if self.tOptions.bKeepAlive and not override then
		-- do nothing
	else
		if self.wnd then
			self.Expired = true
			self.wnd:Show(false)
			self.wnd:DestroyChildren()
			self.wnd:Destroy()
			self.wnd = nil
		end
	end
	return nil
end

-- --------------------------------------------------------------
-- ------------------- HELPER FUNCTIONS -------------------------
-- --------------------------------------------------------------

function FtrLibRaidwarning:FormatWarning(tOptions)
	if tOptions["strFont"] then	self:SetFont(tOptions["strFont"]) end

	if tOptions["strTextColor"] then self:SetTextColor(tOptions["strTextColor"])
	else self:SetTextColor("UI_TextHoloBody") end

	if tOptions["strTimerColor"] then self:SetTimerColor(tOptions["strTimerColor"])
	else self:SetTimerColor("UI_BtnTextBlueNormal") end

	if tOptions["strIcon"] then self:SetIcon(tOptions["strIcon"])
	elseif tOptions["strType"] ~= "warning" then self:SetIcon("FtrIconSprites:Default") -- default timer icon
	else self:SetIcon("") end

	-- check if warning has a progress bar
	if tOptions["strType"] == "countdown" and not self.tOptions["countforever"] then
		if not tOptions.fStartAt then self.tOptions.fStartAt=self.fDuration end
		if not tOptions.fStopAt then self.tOptions.fStopAt=0 end
		self.bHasProgressBar = true
		self.wnd:SetMax(100) --if stopat then stopat else fDuration end braucht 0 ---3---------- 10
		if tOptions["fElapsed"] then
			self.wnd:SetProgress(100/self.SaveData[3]*(tOptions["fElapsed"]))
			self.fDuration = self.SaveData[3]-tOptions["fElapsed"]
			if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetText(("+%ss"):format(self.fDuration)) end
		else self.wnd:SetProgress(0) end
		--rdwrn.wnd:EnableGlow(bEnableGlow)
	elseif tOptions["strType"] == "countup" and tOptions["fStopAt"] then
		if not tOptions.fStartAt then self.tOptions.fStartAt=self.fDuration end
		if not tOptions.fStopAt then self.tOptions.fStopAt=0 end
		self.bHasProgressBar = true
		self.wnd:SetMax(100) --if stopat then stopat else fDuration end
		if tOptions["fElapsed"] then
			self.wnd:SetProgress(100/self.SaveData[3]*(tOptions["fElapsed"]))
			self.fDuration = tOptions["fElapsed"]
			if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetText(("+%ss"):format(self.fDuration)) end
		else self.wnd:SetProgress(0) end
	else
		self.bHasProgressBar = false
	end

	-- setup bar
	if self.bHasProgressBar then
		if tOptions["strBarTextureFull"] then self.wnd:SetFillSprite(tOptions["strBarTextureFull"])
		else self.wnd:SetFillSprite("FtrBarSprites:Minimalist") end
		if tOptions["strBarFillColor"] then self.wnd:SetBarColor(tOptions["strBarFillColor"])
		else self.wnd:SetBarColor("FFFF0000") end

		if tOptions["strTextColor"] then self.wnd:FindChild("lblText"):SetTextColor(tOptions["strTextColor"])
		else self.wnd:FindChild("lblText"):SetTextColor("FFFFFFFF") end
		if self.wnd:FindChild("lblTimer") then
			if tOptions["strTimerColor"] then self.wnd:FindChild("lblTimer"):SetTextColor(tOptions["strTimerColor"])
			else self.wnd:FindChild("lblTimer"):SetTextColor("FFFFFFFF") end
		end
	end
end

function FtrLibRaidwarning:ParseTriggers(tOptions)
	local tT = {}
	for k,v in pairs(tOptions) do
		if (type(v)=="table") then -- check if current argument is a table
			if (type(v[1])=="string") and (v[1]:sub(1,2) == "On") then -- looks like a trigger
				if not tT[v[1]] then tT[v[1]] = {} end -- create trigger subtable if doesnt exist

				-- if table has one member besides trigger type, save as single value
				if not v[3] then tT[v[1]][#tT[v[1]]+1] = v[2]
				else -- else save as table
					tT[v[1]][#tT[v[1]]+1] = {}
					for i, sv in ipairs(v) do
						if i>1 then tT[v[1]][#tT[v[1]]][i-1] = sv end
					end
				end
			end
		end
	end
	return tT
end

function FtrLibRaidwarning:SetDefaultSounds(tTrigger)
	if self.tOptions["bMute"] or self.tOptions.strType == "freeze" then return tTrigger end
	if self.tOptions.strType ~= "warning" then
		if not tTrigger["OnTimeLeft"] then tTrigger["OnTimeLeft"] = {} end
		if not tTrigger["OnExpire"] then tTrigger["OnExpire"] = {} end
		if not tTrigger["OnMove"] then tTrigger["OnMove"] = {} end

		tTrigger["OnTimeLeft"][#tTrigger["OnTimeLeft"]+1] = {3, "Play", Sound.PlayUIButtonHoloLarge}
		tTrigger["OnTimeLeft"][#tTrigger["OnTimeLeft"]+1] = {2, "Play", Sound.PlayUIButtonHoloLarge}
		tTrigger["OnTimeLeft"][#tTrigger["OnTimeLeft"]+1] = {1, "Play", Sound.PlayUIButtonHoloLarge}
		tTrigger["OnExpire"][#tTrigger["OnExpire"]+1] = {"Play", Sound.PlayUIExplorerButtonEnabled}
		tTrigger["OnMove"][#tTrigger["OnMove"]+1] = {"Play", Sound.PlayUIMemoryButton2}
	else -- default raidwarning sounds
		if not tTrigger["OnMove"] then tTrigger["OnMove"] = {} end
		if not tTrigger["OnShowRaidwarning"] then tTrigger["OnShowRaidwarning"] = {} end
		tTrigger["OnMove"][#tTrigger["OnMove"]+1] = {"all", "Play", Sound.PlayUIMemoryButton2}
		tTrigger["OnShowRaidwarning"][#tTrigger["OnShowRaidwarning"]+1] = {"Play", Sound.PlayUICraftingOverchargeWarning}
	end
	return tTrigger
end

function FtrLibRaidwarning:CheckSwitchAt()
	if self.tOptions["tSwitchAt"] then
		-- if table, switch multiple times
		if (type(self.tOptions["tSwitchAt"][1]) == "table") then
			for k, v in pairs(self.tOptions["tSwitchAt"]) do
				if ((self.tOptions["strType"] == "countup") and v[1] < self.fDuration and not self.tSwitches[k]) or
				   ((self.tOptions["strType"] == "countdown") and v[1] > self.fDuration and not self.tSwitches[k]) then
					self:OnSwitch(k)
					local strText = self.wnd:FindChild("lblText"):GetText()
					-- destroy old form and create new one
					self.wnd:Show(false)
					self.wnd:Destroy()
					self.wnd = Apollo.LoadForm(v[2], v[3], v[4], v[5])
					self.wnd:FindChild("lblText"):SetText(strText)
					if self.wnd:FindChild("lblTimer") then self.wnd:FindChild("lblTimer"):SetText(("+%ss"):format(math.floor(self.fDuration+0.5))) end
					-- format new bar
					self:FormatWarning(self.tOptions)

					-- sort
					if self.tOptions["strType"] == "freeze" and not self.bSized then
						self:Autosize()
					end

					-- render
					if not self.tOptions["bHide"] then
						self.wnd:Show(true)
					else self.wnd:Show(false) end
					return self
				end
			end
		elseif (type(self.tOptions["tSwitchAt"][1]) == "number") then
			--Print(("%s > %s"):format(self.tOptions["tSwitchAt"][1], self.fDuration))
			if ((self.tOptions["strType"] == "countup") and self.tOptions["tSwitchAt"][1] < self.fDuration and not self.tSwitches[1]) or
			   ((self.tOptions["strType"] == "countdown") and self.tOptions["tSwitchAt"][1] > self.fDuration and not self.tSwitches[1]) then
				--Print("switch now")
				self:OnSwitch(1)
				local strText = self.wnd:FindChild("lblText"):GetText()
				-- destroy old form and create new one
				self.wnd:Show(false)
				self.wnd:Destroy()
				self.wnd = Apollo.LoadForm(self.tOptions["tSwitchAt"][2], self.tOptions["tSwitchAt"][3], self.tOptions["tSwitchAt"][4], self.tOptions["tSwitchAt"][5])
				self.wnd:FindChild("lblText"):SetText(strText)
				self.wnd:FindChild("lblTimer"):SetText(("+%ss"):format(math.floor(self.fDuration+0.5)))
				-- format new bar
				self:FormatWarning(self.tOptions)
				if (self.bHasProgressBar) then
					self.wnd:SetProgress(100/self.SaveData[3]*(self.SaveData[3]-self.fDuration))
				end
				-- sort
				if self.tOptions["strType"] == "freeze" and not self.bSized then
					self:Autosize()
				end
				-- render
				if not self.tOptions["bHide"] then
					self.wnd:Show(true)
				else self.wnd:Show(false) end
				return self
			end
		end
	end
end

function FtrLibRaidwarning:Autosize()
	-- shitty way of adjusting height
	if string.len(self.wnd:FindChild("lblText"):GetText())*6 > self.wnd:FindChild("lblText"):GetWidth() then
		local newheight = math.ceil((string.len(self.wnd:FindChild("lblText"):GetText())*6) / self.wnd:FindChild("lblText"):GetWidth())*(13)+13

		local l,t,r,b = self.wnd:FindChild("lblText"):GetAnchorOffsets()
		self.wnd:FindChild("lblText"):SetAnchorOffsets(l,t,r,newheight)

		if self.wnd:FindChild("lblTimer") then
			local l,t,r,b = self.wnd:FindChild("lblTimer"):GetAnchorOffsets()
			self.wnd:FindChild("lblTimer"):SetAnchorOffsets(l,t,r,newheight)
		end

		local l,t,r,b = self.wnd:GetAnchorOffsets()
		self.wnd:SetAnchorOffsets(l, t, r, newheight)
	end
	self.bSized = true
end

function FtrLibRaidwarning:ListAllTriggers()
	-- List all Triggers
	Print("Listing all Triggers:")
	Print("=====================")
	for k,v in pairs(self.tTriggers) do
		Print(k.."-Triggers:")
		for sk,sv in pairs(v) do
			local trstring = "#"..sk..": "
			for ssk, ssv in pairs(sv) do
				trstring = trstring .. tostring(ssv) .. " ( " .. type(ssv) .. " ), "
			end
			Print(trstring:sub(1, -2))
		end
		Print("--------")
	end
end

function FtrLibRaidwarning:Export()
	return {self.strName, self.strText, self.fDuration, self.tOptions, self.windowArgs}
end

Apollo.RegisterPackage(FtrLibRaidwarning, "Ftr:Libs:Raidwarning", 1, {})