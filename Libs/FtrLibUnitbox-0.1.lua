local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local pairs = pairs
local ipairs = ipairs
local table = table
local string = string
local type = type
local os = os
-- --------------------------------------------------------------
-- ------------------ MODULE CLASS -------------------------
-- --------------------------------------------------------------
local FtrLibUnitbox  = {}
FtrLibUnitbox .__index = FtrLibUnitbox

setmetatable(FtrLibUnitbox, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function FtrLibUnitbox.create(Ftr)
	local self = setmetatable({}, FtrLibUnitbox)
	self.tEnemies = {}
	self.tFriendlies = {}
	self.Running = false
	self.FtrLibUnit = Ftr.FtrLibUnit
	self.Ftr = Ftr
	return self
end

function FtrLibUnitbox:Start()
	if not self.Running then
		Apollo.RegisterEventHandler("UnitEnteredCombat", "OnUnitEnteredCombat", self)
		-- Apollo.RegisterEventHandler("CombatLogDamage", "OnCombatLogDamage", self)
		-- Apollo.RegisterEventHandler("CombatLogImmunity", "OnCombatLogImmunity", self)
		-- Apollo.RegisterEventHandler("CombatLogAbsorption", "OnCombatLogAbsorption", self)
		-- Apollo.RegisterEventHandler("AlternateTargetUnitChanged", "OnAlternateTargetUnitChanged", self)
		-- Apollo.RegisterEventHandler("TargetedByUnit", "OnTargetedByUnit", self)
		-- Apollo.RegisterEventHandler("TargetUnitChanged", "OnTargetUnitChanged", self)
		-- Apollo.RegisterEventHandler("UnitCreated", "OnUnitCreated", self)
		-- Apollo.RegisterEventHandler("UnitDestroyed", "OnUnitDestroyed", self)
		self.Running = true
	end
end

function FtrLibUnitbox:Stop()
	if self.Running then
		-- Apollo.RemoveEventHandler("UnitEnteredCombat", self)
		-- Apollo.RemoveEventHandler("CombatLogDamage", self)
		-- Apollo.RemoveEventHandler("CombatLogImmunity", self)
		-- Apollo.RemoveEventHandler("CombatLogAbsorption", self)
		-- Apollo.RemoveEventHandler("AlternateTargetUnitChanged", self)
		-- Apollo.RemoveEventHandler("TargetedByUnit", self)
		-- Apollo.RemoveEventHandler("TargetUnitChanged", self)
		for k,v in pairs(self.tEnemies) do v:Destroy() self.tEnemies[k] = nil end
		for k,v in pairs(self.tFriendlies) do v:Destroy() self.tFriendlies[k] = nil end
		self.tEnemies = {}
		self.tFriendlies = {}
		self.Running = false
	end
end

function FtrLibUnitbox:GetByName(strName)
	local tResult = {}
	for k,v in pairs(self.tEnemies) do
		if (v:GetName():find(strName)) then
			tResult[#tResult+1] = v
		end
	end
	for k,v in pairs(self.tFriendlies) do
		if (v:GetName():find(strName)) then
			tResult[#tResult+1] = v
		end
	end
	return tResult
end

function FtrLibUnitbox:Print()
	Print("Enemy List:")
	for k,v in pairs(self.tEnemies) do
		Print(v:GetName().." ("..k..")")
	end
end

function FtrLibUnitbox:GetById(nId)
	if self.tEnemies[nId] then return self.tEnemies[nId] end
	if self.tFriendlies[nId] then return self.tFriendlies[nId] end
	return false
end

function FtrLibUnitbox:GetAsTable(nId)
	if self.tEnemies[nId] then return self.tEnemies[nId]:GetTable() end
	if self.tFriendlies[nId] then return self.tFriendlies[nId]:GetTable() end
	return false
end

function FtrLibUnitbox:GetCount(strTable)
	local counter=0
	if strTable=="Enemies" then
		for _,_ in pairs(self.tEnemies) do counter=counter+1 end
	elseif strTable=="Friendlies" then
		for _,_ in pairs(self.tFriendlies) do counter=counter+1 end
	else
		for _,_ in pairs(self.tEnemies) do counter=counter+1 end
		for _,_ in pairs(self.tFriendlies) do counter=counter+1 end
	end
	return counter
end

function FtrLibUnitbox:GetEnemies()
	return self.tEnemies
end

function FtrLibUnitbox:GetFriendlies()
	return self.tFriendlies
end

function FtrLibUnitbox:Add(uUnit)
	if ((not self.tEnemies[uUnit:GetId()]) and (not self.tFriendlies[uUnit:GetId()])) then
		local tCluster = uUnit:GetClusterUnits()
		for k,v in pairs(tCluster) do
			if v:IsDead() and not v:IsInYourGroup() then self:Remove(v)
			else
				if not v:IsInYourGroup() then
					if v:GetName()=="Null System Daemon" then Print("Added "..v:GetName()) end
					if v:GetName()=="Binary System Daemon" then Print("Added "..v:GetName()) end
					if self.tEnemies[v:GetId()] then self.tEnemies[v:GetId()]:Destroy() self.tEnemies[v:GetId()] = nil end
					self.tEnemies[v:GetId()] = self.FtrLibUnit.createFromUnit(v)
				else
					if self.tFriendlies[v:GetId()] then self.tFriendlies[v:GetId()]:Destroy() self.tFriendlies[v:GetId()] = nil end
					self.tFriendlies[v:GetId()] = v
				end
			end
		end
		if uUnit:IsDead() and not uUnit:IsInYourGroup() then self:Remove(uUnit)
		else
			if not uUnit:IsInYourGroup() then
				if uUnit:GetName()=="Null System Daemon" then Print("Added "..uUnit:GetName()) end
				if uUnit:GetName()=="Binary System Daemon" then Print("Added "..uUnit:GetName()) end
				if self.tEnemies[uUnit:GetId()] then self.tEnemies[uUnit:GetId()]:Destroy() self.tEnemies[uUnit:GetId()] = nil end
				self.tEnemies[uUnit:GetId()] = self.FtrLibUnit.createFromUnit(uUnit)
			else
				if self.tFriendlies[uUnit:GetId()] then self.tFriendlies[uUnit:GetId()]:Destroy() self.tFriendlies[uUnit:GetId()] = nil end
				self.tFriendlies[uUnit:GetId()] = self.FtrLibUnit.createFromUnit(uUnit) end
		end
	end
end

function FtrLibUnitbox:Remove(uUnit)
	if self.tEnemies[uUnit:GetId()] then self.tEnemies[uUnit:GetId()]:Destroy() self.tEnemies[uUnit:GetId()] = nil end
	if self.tFriendlies[uUnit:GetId()] then self.tFriendlies[uUnit:GetId()]:Destroy() self.tFriendlies[uUnit:GetId()] = nil end
end

function FtrLibUnitbox:OnUnitEnteredCombat(uUnit, bInCombat)
	if bInCombat then self:Add(uUnit)
	else self:Remove(uUnit) end
end

function FtrLibUnitbox:OnCombatLogDamage(tLogEvent)
	if tLogEvent.unitCaster then self:Add(tLogEvent.unitCaster) end
	if tLogEvent.unitTarget then self:Add(tLogEvent.unitTarget) end
end

function FtrLibUnitbox:OnCombatLogImmunity(tLogEvent)
	if tLogEvent.unitCaster then self:Add(tLogEvent.unitCaster) end
	if tLogEvent.unitTarget then self:Add(tLogEvent.unitTarget) end
end

function FtrLibUnitbox:OnCombatLogAbsorption(tLogEvent)
	if tLogEvent.unitCaster then self:Add(tLogEvent.unitCaster) end
	if tLogEvent.unitTarget then self:Add(tLogEvent.unitTarget) end
end

function FtrLibUnitbox:OnAlternateTargetUnitChanged(uUnit)
	if uUnit then self:Add(uUnit) end
end

function FtrLibUnitbox:OnTargetedByUnit(uUnit)
	if uUnit then self:Add(uUnit) end
end

function FtrLibUnitbox:OnTargetUnitChanged(uUnit)
	if uUnit then self:Add(uUnit) end
end

function FtrLibUnitbox:OnUnitCreated(uUnit)
	if uUnit then self:Add(uUnit) end
end

function FtrLibUnitbox:OnUnitDestroyed(uUnit)
	if uUnit then self:Remove(uUnit) end
end

function FtrLibUnitbox:Export()
	local tData = {}
	tData["tEnemies"] = {}
	tData["tFriendlies"] = {}
	for k,v in pairs(self.tEnemies) do tData["tEnemies"][k] = self.tEnemies[k]:GetTable() end
	for k,v in pairs(self.tFriendlies) do tData["tFriendlies"][k] = self.tFriendlies[k]:GetTable() end
	return tData
end

function FtrLibUnitbox:Import(tData)
	for k,v in pairs(tData["tEnemies"]) do
		if not self.tEnemies[k] then self.tEnemies[k] = self.FtrLibUnit.create(tData["tEnemies"][k]) end
	end
	for k,v in pairs(tData["tFriendlies"]) do
		if not self.tFriendlies[k] then self.tFriendlies[k] = self.FtrLibUnit.create(tData["tFriendlies"][k]) end
	end
	return true
end

Apollo.RegisterPackage(FtrLibUnitbox, "Ftr:Libs:Unitbox", 1, {})