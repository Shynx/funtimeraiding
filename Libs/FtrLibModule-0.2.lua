local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local pairs = pairs
local table = table
local type = type
local os = os
-- --------------------------------------------------------------
-- ------------------ MODULE CLASS -------------------------
-- --------------------------------------------------------------
local FtrLibModule  = {}
FtrLibModule .__index = FtrLibModule

setmetatable(FtrLibModule, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function FtrLibModule.new(strName)
	if not strName then strName = ("newModule_"..os.time()) end
	local self = setmetatable({}, FtrLibModule)
	self.__strName = strName
	return self
end

function FtrLibModule:__Init()
	self.__ModuleRunning = false
	self.__EncounterPhase = 0
	self.__tWarnings = {}
	self.__tTimers = {}
	self.__tDrawings = {}
	self.__nDrawings = 0
	self.__tHooks = {}
	self.__tEvents = {}
	self.__fRunningSince = os.clock()
	self.__Ftr = Apollo.GetAddon("FunTimeRaiding")
	self.__Trigonometry = self.__Ftr.FtrLibDraw
	self.uPlayer = self.__Ftr.uPlayer
end

function FtrLibModule:__IsRunning()
	return self.__ModuleRunning
end

function FtrLibModule:__GetName()
	return self.__strName
end

function FtrLibModule:__GetDrawings()
	if self.__tDrawings then return self.__tDrawings
	else return false end
end

function FtrLibModule:__GetWarnings()
	return self.__tWarnings
end

function FtrLibModule:__GetTimers()
	return self.__tTimers
end

function FtrLibModule:__GetHooksByEvent(Event)
	local strEventName
	if(type(Event)=="string") then
		strEventName = Event
	elseif(Event:GetName() and type(Event:GetName())=="string") then
		strEventName = Event:GetName()
	end

	local tReturn = {}
	for k, v in pairs(self.__tHooks[strEventName]) do
		if (v:GetName() == strEventName) then tReturn[#tReturn+1] = v end
	end
	return tReturn
end

function FtrLibModule:__GetUnitById(strName)
	return self.__Ftr.Unitbox:GetById(nId)
end

function FtrLibModule:__GetUnitByName(strName)
	return self.__Ftr.Unitbox:GetByName(strName)
end

function FtrLibModule:__SetWarnings(tWarnings)
	self.__tWarnings = tWarnings
end

function FtrLibModule:__SetTimers(tTimers)
	self.__tTimers = tTimers
end

function FtrLibModule:__SetDrawings(tDrawings)
	self.__tDrawings = tDrawings
end

function FtrLibModule:__HookEvent(Event)
	local tHooks = self:__GetHooksByEvent(Event)
	for _,hHook in pairs(tHooks) do
		if hHook then hHook:Hook() end
	end
	return true
end

function FtrLibModule:__UnhookEvent(Event)
	local tHooks = self:__GetHooksByEvent(Event)
	for _,hHook in pairs(tHooks) do
		if hHook then hHook:Unhook() end
	end
	return true
end

function FtrLibModule:__Fire(Event, args)
	local tHooks = self:__GetHooksByEvent(Event)
	for _,hHook in pairs(tHooks) do
		if hHook then
			if(type(args)=="table") then return hHook:Fire(unpack(args))
			elseif(type(args)~="nil") then hHook:Fire(args)
			else hHook:Fire() end
		end
	end
	return true
end

function FtrLibModule:__HookAll()
	for _,v in pairs(self.__tHooks) do
		for _,hHook in pairs(v) do
			hHook:Hook()
		end
	end
	return true
end

function FtrLibModule:__UnhookAll()
	for _,v in pairs(self.__tHooks) do
		for _,hHook in pairs(v) do
			hHook:Unhook()
		end
	end
	return true
end

function FtrLibModule:__IsHookRegistered(Event)
	local strEventName
	if(type(Event)=="string") then
		strEventName = Event
	elseif(Event:GetName() and type(Event:GetName())=="string") then
		strEventName = Event:GetName()
	end

	if self.__tHooks[strEventName] then return true end
	return false
end

function FtrLibModule:__IsHooked(Event)
	local tHooks = self:__GetHooksByEvent(Event)
	for _,hHook in pairs(tHooks) do
		if hHook then
			if not hHook.Hooked then return true end
		end
	end
	return false
end

function FtrLibModule:__SendEvent(strEvent, strHookId, tEventData)
	if not self.__tEvents[strEvent] then self.__tEvents[strEvent] = {} end
	if not self.__tEvents[strEvent][strHookId] then self.__tEvents[strEvent][strHookId] = {} end
	local count=1 for _,_ in pairs(self.__tEvents[strEvent][strHookId]) do count=count+1 end
	self.__tEvents[strEvent][strHookId][count] = tEventData

	self.__Ftr:SendMessage({
		type="FtrEvent",
		strModule=self:__GetName(),
		strEvent=strEvent,
		strHookId=strHookId,
		nHookCount=count,
		strSender=self.__Ftr.uPlayer:GetName()
	})
end

function FtrLibModule:__OnComFtrEvent(tMsg)
	if (tMsg.strEvent and tMsg.strHookId and tMsg.nHookCount) then
		if(tMsg.strEvent ~=  "SubZoneChanged") then
			--check if we have this event
			if  (not self.__tEvents[tMsg.strEvent]) or
				(not self.__tEvents[tMsg.strEvent][tMsg.strHookId]) or
				(not self.__tEvents[tMsg.strEvent][tMsg.strHookId][tMsg.nHookCount]) then
				--if not, request event data from sender
				self:__RequestEventData(tMsg.strEvent, tMsg.strHookId, tMsg.nHookCount, tMsg.strSender)
			end
		end
	else
		Print("Message of type 'FtrEvent' is corrupted.")
	end
end

function FtrLibModule:__RequestEventData(strEvent, strHookId, nHookCount, strSender)
	self.__Ftr:SendMessage({
		type="FtrReqEvData",
		target=strSender,
		strModule=self:__GetName(),
		strEvent=strEvent,
		strHookId=strHookId,
		nHookCount=nHookCount,
		strSender=self.__Ftr.uPlayer:GetName()
	})
end

function FtrLibModule:__OnComFtrReqEvData(tMsg)
	if (tMsg.strEvent and tMsg.strHookId and tMsg.nHookCount) then
		if((tMsg.target and tMsg.target == GameLib.GetPlayerUnit():GetName()) or (not tMsg.target)) then
			self.__Ftr:SendMessage({
				type="FtrSndEvData",
				target=tMsg.strSender,
				strModule=self:__GetName(),
				strEvent=tMsg.strEvent,
				strHookId=tMsg.strHookId,
				tEventData=self.__tEvents[tMsg.strEvent][tMsg.strHookId][tMsg.nHookCount]
			})
		end
	else
		Print("Message of type 'FtrReqEvData' is corrupted.")
	end
end

function FtrLibModule:__OnComFtrSndEvData(tMsg)
	if (tMsg.strEvent and tMsg.strHookId and tMsg.tEventData) then
		if((tMsg.target and tMsg.target == GameLib.GetPlayerUnit():GetName()) or (not tMsg.target)) then
			-- look for object handles and convert those to Objects
			for k,v in pairs(tMsg.tEventData) do
				if(type(v)=="table" and v["GetType"]) then
					if v.GetType=="tUnitData" then tMsg.tEventData[k] = self.__Ftr.FtrLibUnit.create(v) end
					if v.GetType=="tSpellData" then tMsg.tEventData[k] = GameLib.GetSpell(v.id) end
					if v.GetType=="tBuffData" then tMsg.tEventData[k].splEffect = GameLib.GetSpell(v.splEffect.id) end
				elseif type(v)=="table" and (v["unitCaster"] or v["unitTarget"] or v["splCallingSpell"]) then
					if v["unitCaster"] then tMsg.tEventData[k].unitCaster = self.__Ftr.FtrLibUnit.create(v.unitCaster) end
					if v["unitTarget"] then tMsg.tEventData[k].unitTarget = self.__Ftr.FtrLibUnit.create(v.unitTarget) end
					if v["splCallingSpell"] then tMsg.tEventData[k].splCallingSpell = GameLib.GetSpell(v.splCallingSpell.id) end
				end
			end

			if not self.__tEvents[tMsg.strEvent] then self.__tEvents[tMsg.strEvent] = {} end
			if not self.__tEvents[tMsg.strEvent][tMsg.strHookId] then self.__tEvents[tMsg.strEvent][tMsg.strHookId] = {} end
			local count=1 for _,_ in pairs(self.__tEvents[tMsg.strEvent][tMsg.strHookId]) do count=count+1 end
			self.__tEvents[tMsg.strEvent][tMsg.strHookId][count] = tMsg.tEventData

			for _,v in pairs(self:__GetHooksByEvent(tMsg.strEvent)) do
				v:Fire(unpack(tMsg.tEventData))
			end
		end
	else
		Print("Message of type 'FtrSndEvData' is corrupted.")
	end
end

function FtrLibModule:__SendStatus(strRecipient)
	-- send module status
	local tData = {}
	--packaging drawings, warnings and timer
	tData["__tDrawings"] = {}
	for k,v in pairs(self.__tDrawings) do
		tData["__tDrawings"][k] = v:Export()
	end
	tData["__tWarnings"] = {}
	for k,v in pairs(self.__tWarnings) do
		tData["__tWarnings"][k] = v:Export()
	end
	tData["__tTimers"] = {}
	for k,v in pairs(self.__tTimers) do
		tData["__tTimers"][k] = v:Export()
	end

	for k,v in pairs(self) do
		if k~="__tHooks" and k~="__Ftr" and k~="__Trigonometry" and k~="__Ftr" and type(v)~="function" and
		   k~="__tDrawings" and k~="__tWarnings" and k~="__tTimers" then
			if type(v)=="table" then
				for j, w in pairs(v) do
					if type(w)=="userdata" then
						if w["IsACharacter"] then tData[k][j] = self.__Ftr.FtrLibHook:GetUnitTable(w) end -- unit
						if w["GetCastTime"] then tData[k][j] = self.__Ftr.FtrLibHook:GetSpellTable(w) end -- spell
						if w["idBuff"] then tData[k][j] = self.__Ftr.FtrLibHook:GetBuffTable(w) end -- buff
					end
				end
			elseif type(v)=="userdata" then
				if v["IsACharacter"] then tData[k] = self.__Ftr.FtrLibHook:GetUnitTable(v) end -- unit
				if v["GetCastTime"] then tData[k] = self.__Ftr.FtrLibHook:GetSpellTable(v) end -- spell
				if v["idBuff"] then tData[k] = self.__Ftr.FtrLibHook:GetBuffTable(v) end -- buff
			else tData[k] = v
			end
		end
	end
	tData["__tUnits"] = self.__Ftr.Unitbox:Export()

	self.__Ftr:SendMessage({
		type="FtrSndStatus",
		target=strRecipient,
		strModule=self:__GetName(),
		tData=tData
	})
end

function FtrLibModule:__SetStatus(tData)
	if not self.__ModuleRunning then
		-- setting module status
		self.__Ftr.Unitbox:Import(tData["__tUnits"])

		self.__tDrawings = {}
		for k,v in pairs(tData.__tDrawings) do
			self.__tDrawings[k] = self.__Ftr.FtrLibDraw.create(unpack(v))
		end
		self.__tWarnings = {}
		for k,v in pairs(tData.__tWarnings) do
			self.__tWarnings[k] = self.__Ftr.FtrRaidwarning.create(unpack(v))
		end
		self.__tTimers = {}
		for k,v in pairs(tData.__tTimers) do
			self.__tTimers[k] = self.__Ftr.FtrRaidwarning.create(unpack(v))
		end

		for k,v in pairs(tData) do
			if k~="__tDrawings" and k~="__tWarnings" and k~="__tTimers" then
				if type(v)=="table" then
					for j, w in pairs(v) do
						if type(w)=="userdata" then
							if w["IsACharacter"] then self[k][j] = self.__Ftr.FtrLibHook:GetUnitTable(w) end -- unit
							if w["GetCastTime"] then self[k][j] = self.__Ftr.FtrLibHook:GetSpellTable(w) end -- spell
							if w["idBuff"] then self[k][j] = self.__Ftr.FtrLibHook:GetBuffTable(w) end -- buff
						end
					end
				elseif type(v)=="userdata" then
					if v["IsACharacter"] then self[k] = self.__Ftr.FtrLibHook:GetUnitTable(v) end -- unit
					if v["GetCastTime"] then self[k] = self.__Ftr.FtrLibHook:GetSpellTable(v) end -- spell
					if v["idBuff"] then self[k] = self.__Ftr.FtrLibHook:GetBuffTable(v) end -- buff
				else self[k] = v
				end
			end
		end
		self:__HookAll()
		self.__ModuleRunning = true
		if self:__IsHookRegistered("Resume") then self:__Fire("Resume") end
	end
end

function FtrLibModule:__StartModule()
	if not self.__ModuleRunning then
		self:__HookAll()
		self.__ModuleRunning = true
		self.__tWarnings = {}
		self.__tTimers = {}
		self.__tDrawings = {}
		if self:__IsHookRegistered("Start") then self:__Fire("Start") end
	end
end

function FtrLibModule:__StopModule()
	if self.__ModuleRunning then
		self:__UnhookAll()
		if self:__IsHookRegistered("Stop") then self:__Fire("Stop") end
		self.__ModuleRunning = false
		for k,v in pairs(self.__tWarnings) do v:Destroy() self.__tWarnings[k] = nil end
		for k,v in pairs(self.__tTimers) do v:Destroy() self.__tTimers[k] = nil end
		for k,v in pairs(self.__tDrawings) do v:Destroy() self.__tDrawings[k] = nil end
		self.__tWarnings = nil
		self.__tTimers = nil
		self.__tDrawings = nil
	end
end

function FtrLibModule:__Redraw()
	local counter = 0
	if self:__IsRunning() and self:__GetDrawings() then
		for i, t in pairs(self:__GetDrawings()) do
			counter = counter+1
			t:Render()
			if ((t:GetDuration()) and (t:GetDuration() < 0)) then self:DestroyDrawing(t:GetName()) end
		end
	end
	if counter <= 0 then Apollo.RemoveEventHandler("NextFrame", "__Redraw", self) end
end
-- ----------------------------------------
-- ------ Module Helper Functions ---------
-- ----------------------------------------
function FtrLibModule:RegisterEventHandler(strEvent, strFunction, ...)
	local Module = false
	local args = {}
	for k,v in pairs(arg) do
		if ((type(v) == "userdata") and (v["__GetName"])) then Module = v
		else args[#args+1] = v end
	end
	if not Module then Module = self end

	if not self.__tHooks[strEvent] then self.__tHooks[strEvent] = {} end
	self.__tHooks[strEvent][#self.__tHooks[strEvent]+1] = self.__Ftr.FtrLibHook.create(strEvent, strFunction, Module, args)
end

function FtrLibModule:RegisterStartCondition(strEvent, ...)
	self.__Ftr:RegisterStartCondition(self:__GetName(), strEvent, unpack(arg))
end

function FtrLibModule:RegisterStopCondition(strEvent, ...)
	self.__Ftr:RegisterStopCondition(self:__GetName(), strEvent, unpack(arg))
end

function FtrLibModule:ShowDrawing(id, type, args, options)
	if self.__tDrawings[id] then self:DestroyDrawing(id) end

	self.__tDrawings[id] = self.__Ftr.FtrLibDraw.create(id, type, args, options)
	self.__tDrawings[id]:Render()
	if self.__nDrawings == 0 then Apollo.RegisterEventHandler("NextFrame", "__Redraw", self) end
	self.__nDrawings = self.__nDrawings + 1
end

function FtrLibModule:ShowWarning(text, id, duration, options)
	if (type(options)=="table") then options["strType"] = "warning"
	else options = {strType="warning"} end
	if not duration then duration = 3 end
	if not id then
		id=("RW_1")
		while(self:GetWarning(id)) do
			id = id.."_1"
		end
	end

	self.__tWarnings[id] = self.__Ftr.FtrRaidwarning.create(
		id,
		text,
		duration,
		options,
		"Raidwarning"
	)
end

function FtrLibModule:ShowTimer(id, text, duration, options, frame)
	if not frame then frame = "SmallTimer" end
	if(type(options)=="table") then
		if not options.strType then
			options.strType = "countdown" end
		if not options.Window then
			frame = "SmallTimer"
		else
			frame=options.Window
			options.Window = nil
		end
	else
		options = {strType="countdown"}
		frame = "SmallTimer"
	end
	if self:GetTimer(id) and options["strType"] ~= "freeze" then self:DestroyTimer(id)
	else
		while(self:GetTimer(id)) do
			id = id.."_1"
		end
	end
	self.__tTimers[id] = self.__Ftr.FtrRaidwarning.create(
		id,
		text,
		duration,
		options,
		frame
	)
end

function FtrLibModule:DestroyWarning(id)
	if(type(id)=="table") then
		for k,_ in pairs(id) do
			if(self.__tWarnings[k]) then
				self.__tWarnings[k]:Destroy()
				self.__tWarnings[k] = nil
			end
		end
	elseif(type(id) ~= "nil") then
		if(self.__tWarnings[id]) then
			self.__tWarnings[id]:Destroy()
			self.__tWarnings[id] = nil
		end
	end
end

function FtrLibModule:DestroyTimer(id)
	if(type(id)=="table") then
		for k,_ in pairs(id) do
			if(self.__tTimers[k]) then
				self.__tTimers[k]:Destroy()
				self.__tTimers[k] = nil
			end
		end
	elseif(type(id) ~= "nil") then
		if(self.__tTimers[id]) then
			self.__tTimers[id]:Destroy()
			self.__tTimers[id] = nil
		end
	end
end

function FtrLibModule:DestroyDrawing(id)
	if(type(id)=="table") then
		for k,_ in pairs(id) do
			if(self.__tDrawings and self.__tDrawings[k]) then
				self.__tDrawings[k]:Destroy()
				self.__tDrawings[k] = nil
				self.__nDrawings = self.__nDrawings - 1
				if self.__nDrawings == 0 then Apollo.RemoveEventHandler("NextFrame", "__Redraw", self) end
			end
		end
	elseif(type(id) ~= "nil") then
		if(self.__tDrawings and self.__tDrawings[id]) then
			self.__tDrawings[id]:Destroy()
			self.__tDrawings[id] = nil
		end
	end
end

function FtrLibModule:DestroyAllDrawings()
	for k,v in pairs(self.__tDrawings) do
		v:Destroy() self.__tDrawings[k] = nil
	end
	self.__tDrawings = {}
end

function FtrLibModule:GetWarning(id)
	if self.__tWarnings and self.__tWarnings[id] then return self.__tWarnings[id]
	else return false end
end

function FtrLibModule:GetTimer(id)
	if self.__tTimers and self.__tTimers[id] then return self.__tTimers[id]
	else return false end
end

function FtrLibModule:GetDrawing(id)
	if self.__tDrawings and self.__tDrawings[id] then return self.__tDrawings[id]
	else return false end
end

function FtrLibModule:GetUnit(mixed)
	if type(mixed)=="number" then return self:__GetUnitById(mixed)
	elseif type(mixed)=="string" then return self:__GetUnitByName(mixed)
	else return false end
end

function FtrLibModule:FindWarning(searchstring)
	local tResults
	for k,v in pairs(self.__tWarnings) do
		if (v:GetName():find(searchstring)) then tResults[#tResults+1] = v end
	end
	return tResults
end

function FtrLibModule:FindTimer(searchstring)
	local tResults
	for k,v in pairs(self.__tTimers) do
		if (v:GetName():find(searchstring)) then tResults[#tResults+1] = v end
	end
	return tResults
end

function FtrLibModule:FindDrawing(searchstring)
	local tResults
	for k,v in pairs(self.__tDrawings) do
		if (v:GetName():find(searchstring)) then tResults[#tResults+1] = v end
	end
	return tResults
end

Apollo.RegisterPackage(FtrLibModule, "Ftr:Libs:Module", 1, {})