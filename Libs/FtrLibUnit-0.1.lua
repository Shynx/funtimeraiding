local Apollo = Apollo
local GameLib = GameLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
-- --------------------------------------------------------------
-- ------------------ UNIT CLASS -------------------------
-- --------------------------------------------------------------
local FtrLibUnit  = {}
FtrLibUnit .__index = FtrLibUnit

setmetatable(FtrLibUnit, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function FtrLibUnit.create(tUnitData)
	local self = setmetatable({}, FtrLibUnit)
	self.uUnit = nil
	self.nUnitId = tUnitData.GetId
	self.tUnitData = tUnitData

	setmetatable(FtrLibUnit,{__index=function(t, k, rest)
		return function(...)
			local args = {...}
			-- try to get unit and forward call
			local uUnit = GameLib.GetUnitById(self.nUnitId)
			if not uUnit and self.tUnitData.k then return self.tUnitData.k
			elseif uUnit then
				self.uUnit = uUnit
				if k:sub(1,3)=="Get" then
					self.tUnitData.k = uUnit[k](uUnit, unpack(args))
					return self.tUnitData.k
				else
					return uUnit[k](uUnit, unpack(args))
				end
			else return false
			end
		end
	end})

	return self
end

function FtrLibUnit:Print()
	Print(self:GetName())
end

function FtrLibUnit:GetTable()
	return self.tUnitData
end

function FtrLibUnit:GetUnit()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if uUnit then return uUnit
	else return false end
end

function FtrLibUnit.createFromUnit(uUnit)
	local self = setmetatable({}, FtrLibUnit)
	self.uUnit = uUnit
	self.nUnitId = uUnit:GetId()
	self.tUnitData = {
		GetType="tUnitData",
		GetId=uUnit:GetId(),
		GetName=uUnit:GetName(),
		GetClassId=uUnit:GetClassId(),
		GetPosition=uUnit:GetPosition(),
		GetHeading=uUnit:GetHeading(),
		GetFacing=uUnit:GetFacing(),
		GetHealth=uUnit:GetHealth(),
		GetMaxHealth=uUnit:GetMaxHealth(),
		GetTargetMarker=uUnit:GetTargetMarker(),
		GetBuffs=uUnit:GetBuffs()
	}

	setmetatable(FtrLibUnit,{__index=function(t, k, rest)
		return function(...)
			local args = {...}
			-- try to get unit and forward call
			local uUnit = GameLib.GetUnitById(self.nUnitId)
			if not uUnit and self.tUnitData.k then return self.tUnitData.k
			else
				if k:sub(1,3)=="Get" then
					self.tUnitData.k = uUnit[k](uUnit, unpack(args))
					return self.tUnitData.k
				else
					return uUnit[k](uUnit, unpack(args))
				end
			end
		end
	end})
	return self
end

function FtrLibUnit:GetId()
	return self.nUnitId
end

function FtrLibUnit:GetName()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetName then return self.tUnitData.GetName
	elseif uUnit then
		self.uUnit = uUnit
		self.tUnitData.GetName=uUnit:GetName()
		return self.tUnitData.GetName
	else return false
	end
end

function FtrLibUnit:GetFacing()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetFacing then return self.tUnitData.GetFacing
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetFacing=uUnit:GetFacing() return self.tUnitData.GetFacing
	else return false
	end
end

function FtrLibUnit:GetHeading()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetHeading then return self.tUnitData.GetHeading
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetHeading=uUnit:GetHeading() return self.tUnitData.GetHeading
	else return false
	end
end

function FtrLibUnit:GetPosition()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetPosition then return self.tUnitData.GetPosition
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetPosition=uUnit:GetPosition() return self.tUnitData.GetPosition
	else return false
	end
end

function FtrLibUnit:GetHealth()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetHealth then return self.tUnitData.GetHealth
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetHealth=uUnit:GetHealth() return self.tUnitData.GetHealth
	else return false
	end
end

function FtrLibUnit:GetMaxHealth()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetMaxHealth then return self.tUnitData.GetMaxHealth
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetMaxHealth=uUnit:GetMaxHealth() return self.tUnitData.GetMaxHealth
	else return false
	end
end

function FtrLibUnit:GetTargetMarker()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetTargetMarker then return self.tUnitData.GetTargetMarker
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetTargetMarker=uUnit:GetTargetMarker() return self.tUnitData.GetTargetMarker
	else return false
	end
end

function FtrLibUnit:GetClassId()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetClassId then return self.tUnitData.GetClassId
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetClassId=uUnit:GetClassId() return self.tUnitData.GetClassId
	else return false
	end
end

function FtrLibUnit:GetBuffs()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetBuffs then return self.tUnitData.GetBuffs
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetBuffs=uUnit:GetBuffs() return self.tUnitData.GetBuffs
	else return false
	end
end

function FtrLibUnit:GetTarget()
	local uUnit = GameLib.GetUnitById(self.nUnitId)
	if not uUnit and self.tUnitData.GetTarget then return self.tUnitData.GetTarget
	elseif uUnit then self.uUnit = uUnit self.tUnitData.GetTarget=uUnit:GetTarget() return self.tUnitData.GetTarget
	else return false
	end
end

function FtrLibUnit:Destroy()
	self.tUnitData = nil
	self.uUnit = nil
	self.nUnitId = nil
end

Apollo.RegisterPackage(FtrLibUnit, "Ftr:Libs:Unit", 1, {})