local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local setmetatable = setmetatable
local unpack = unpack
local tonumber = tonumber
local Print = Print
local math = math
local pairs = pairs
local ipairs = ipairs
local table = table
local string = string
local type = type
local os = os
-- --------------------------------------------------------------
-- ------------------ MODULE CLASS -------------------------
-- --------------------------------------------------------------
local FtrLibHook  = {}
FtrLibHook .__index = FtrLibHook

setmetatable(FtrLibHook, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function FtrLibHook.create(strEvent, strFunction, Module, args)
	local self = setmetatable({}, FtrLibHook)
	self.ActionCheckInterval = 0.2
	self.strEvent = strEvent
	self.strName  = strEvent
	self.strFunction = strFunction
	if not args then args={} end
	self.args = args
	self.bHooked = false
	if Module.__Ftr then self.Ftr = Module.__Ftr
	else self.Ftr = Apollo.GetAddon("FunTimeRaiding") end
	self.Module = Module
	if args.Blacklist then self.Blacklist = args.Blacklist
	else self.Blacklist = {} end
	self.tPrevious = {}

	if(strEvent == "Timer") then
		if not args[2] then self.args.Repeating = true else self.args.Repeating = args[2] end
		if not args[1] then self.args.Interval = 1 else self.args.Interval = args[1] end
		self.strName = "FtrTimer"
	elseif strEvent == "PartyDebuff" then
		if not args.Blacklist then self.Blacklist = {"Radiate", "Gather Focus", "Surge Focus Drain", "Boosted Armor", "Quick Draw", "Figment", "Pounce", "Onslaught"} end
	elseif(self.strEvent == "Health") then
		self.strUnit = args[1]
		if args[2]:sub(-1)=="%" then self.strHealthType = "Percent" self.fThreshold = tonumber(args[2]:sub(1, -2))
		else self.strHealthType = "Absolute" self.fThreshold = tonumber(args[2]) end

		if args[2]:sub(1)=="-" then self.strThresholdType = "Deficit" self.fThreshold = tonumber(self.fThreshold:sub(2))
		else self.strThresholdType = "Left" end
	end

	return self
end

function FtrLibHook:GetName()
	return self.strName
end

function FtrLibHook:Fire(...)
	self.Module[self.strFunction](self.Module, unpack(arg))
end

function FtrLibHook:Hook()
	if self.strEvent == "Timer" then
		self.fLastClock = os.clock()
		Apollo.RegisterEventHandler("NextFrame", "OnNextFrame", self)
	elseif(	(self.strEvent == "EnemyTargetChange") or (self.strEvent == "EnemyCast") or (self.strEvent == "PartyDebuff") or
			(self.strEvent == "EnemyBuff") or (self.strEvent == "Health")) then
		self.fLastClock = os.clock()
		Apollo.RegisterEventHandler("NextFrame", "OnNextFrame", self)
	elseif(self.strEvent=="Story") then
		--hook event to here, so we can preformat output
		Apollo.RegisterEventHandler("StoryPanelDialog_Show", "OnStory", self)
	elseif(self.strEvent=="Chat") then
		--hook event to here, so we can preformat output
	elseif(self.strEvent=="SlashCommand") then
		--hook event to here, so we can preformat output
		Apollo.RegisterSlashCommand("ftr", "OnSlashCommand", self)
	elseif((self.strEvent~="Start") and (self.strEvent~="Stop")) then
		-- regular apollo event
		Apollo.RegisterEventHandler(self.strEvent, "OnApolloTrigger", self)
	end
	self.bHooked = true
	return true
end

function FtrLibHook:Unhook()
	if self.strEvent == "Timer" then
		Apollo.RemoveEventHandler("NextFrame", self)
	elseif((self.strEvent == "EnemyTargetChange") or (self.strEvent == "EnemyCast") or (self.strEvent == "PartyDebuff") or (self.strEvent == "EnemyBuff") or (self.strEvent == "Health")) then
		self.tPrevious = {}
		Apollo.RemoveEventHandler("NextFrame", self)
	elseif(self.strEvent=="Story") then
		--hook event to here, so we can preformat output
		Apollo.RemoveEventHandler("StoryPanelDialog_Show", self)
	elseif(self.strEvent=="Chat") then
		--hook event to here, so we can preformat output
		Apollo.RemoveEventHandler("ChatMessage", self)
	elseif(self.strEvent=="SlashCommand") then
		--hook event to here, so we can preformat output
		Apollo.RemoveEventHandler("ftr", "OnSlashCommand", self)
	elseif((self.strEvent~="Start") and (self.strEvent~="Stop")) then
		-- regular apollo event
		Apollo.RemoveEventHandler(self.strEvent, self)
	end
	self.bHooked = false
	return true
end

function FtrLibHook:OnNextFrame()
	if self.strEvent=="Timer" then
		if (os.clock()-self.fLastClock >= self.args.Interval) then
			self.fLastClock = os.clock()
			self.Module[self.strFunction](self.Module)
			if not self.args.Repeating then Apollo.RemoveEventHandler("NextFrame", self) end
		end
	else
		if (os.clock()-self.fLastClock >= self.ActionCheckInterval) then
			self.fLastClock = os.clock()
			self:OnActionTimer()
		end
	end
end

function FtrLibHook:OnStory(eWindowType, tLines, nDisplayLength)
	local strMessage = ""
	for idx, strCurr in ipairs(tLines) do
		if strCurr then strMessage = strMessage .. strCurr end
	end
	self.Module[self.strFunction](self.Module, strMessage)
end

function FtrLibHook:OnChatMessage(channelCurrent, tMessage)
	-- get message
	local strMessage = ""
	for idx, tSegment in ipairs(tMessage.arMessageSegments) do
		strMessage = strMessage .. tSegment.strText
	end
	self.Module[self.strFunction](self.Module, channelCurrent, strMessage, tMessage.strSender)
end

function FtrLibHook:OnSlashCommand(strCmd, strArg)
	if strArg == self.args[1] then
		-- trigger
		self.Module[self.strFunction](self.Module, strArg)
	end
end

function FtrLibHook:OnActionTimer()
	if(self.strEvent == "EnemyTargetChange") then self:CheckEnemyTargetChange()
	elseif(self.strEvent == "EnemyCast") then self:CheckEnemyCast()
	elseif(self.strEvent == "PartyDebuff") then self:CheckPartyDebuff()
	elseif(self.strEvent == "EnemyBuff") then self:CheckEnemyBuff()
	elseif(self.strEvent == "Health") then self:CheckHealth()
	end
end

function FtrLibHook:OnApolloTrigger(...)
	-- trigger event
	self.Module[self.strFunction](self.Module, unpack(arg))
	-- broadcast event
	local hookid = ""
	for k,v in pairs(arg) do
		-- piece together hook id
		if type(v)=="userdata" and v["GetId"] then hookid = hookid.."_"..v:GetId()
		elseif type(v)=="number" then hookid = hookid.."_"..v
		elseif type(v)=="boolean" then if v then hookid = hookid.."_TRUE" else hookid = hookid.."_FALSE" end
		elseif type(v)=="string" then hookid = hookid.."_"..(v:gsub('%W',''))
		end
		-- if unit
		if type(v)=="userdata" and v["IsACharacter"] and v["GetLevel"] then arg[k] = self:GetUnitTable(v)
		-- if combatlog event
		elseif type(v)=="table" and (v["unitCaster"] or v["unitTarget"] or v["splCallingSpell"]) then
			if v["unitCaster"] then arg[k].unitCaster = self:GetUnitTable(arg[k].unitCaster) hookid = hookid.."_"..arg[k].unitCaster:GetId() end
			if v["unitTarget"] then arg[k].unitTarget = self:GetUnitTable(arg[k].unitTarget) hookid = hookid.."_"..arg[k].unitTarget:GetId() end
			if v["splCallingSpell"] then arg[k].splCallingSpell = self:GetSpellTable(arg[k].splCallingSpell) hookid = hookid.."_"..arg[k].splCallingSpell:GetId() end
		end
	end
	if hookid == "" then hookid = os.time() end
	local data = {unpack(arg)}
	self.Module:__SendEvent(self.strEvent, hookid, data)
end

function FtrLibHook:CheckHealth()
	local tUnits = self.Module:GetUnit(self.strUnit)
	for _,uUnit in pairs(tUnits) do self:CheckHealthSingle(uUnit) end
end

function FtrLibHook:CheckHealthSingle(uUnit)
	if uUnit:GetUnit() then uUnit=uUnit:GetUnit() else return end
	if self.strHealthType == "Absolute" then -- absolute
		if self.strThresholdType == "Left" then -- health left
			if (uUnit:GetHealth() <= self.fThreshold) then
				-- trigger
				if not self.tPrevious[uUnit:GetId()] then
					self.Module[self.strFunction](self.Module, uUnit)
					self.tPrevious[uUnit:GetId()] = true
					local hookid = uUnit:GetId()
					local data = {self:GetUnitTable(uUnit)}
					self.Module:__SendEvent(self.strEvent, hookid, data)
				end
			else
				-- remove trigger
				self.tPrevious[uUnit:GetId()] = false
			end
		else -- health lost
			if ((uUnit:GetMaxHealth()-uUnit:GetHealth()) >= self.fThreshold) then
				-- trigger
				if not self.tPrevious[uUnit:GetId()] then
					self.Module[self.strFunction](self.Module, uUnit)
					self.tPrevious[uUnit:GetId()] = true
					local hookid = uUnit:GetId()
					local data = {self:GetUnitTable(uUnit)}
					self.Module:__SendEvent(self.strEvent, hookid, data)
				end
			else
				-- remove trigger
				self.tPrevious[uUnit:GetId()] = false
			end
		end
	else -- percent
		if self.strThresholdType == "Left" then -- health left
			if ((100/uUnit:GetMaxHealth()*uUnit:GetHealth()) <= self.fThreshold) then
				-- trigger
				if not self.tPrevious[uUnit:GetId()] then
					self.Module[self.strFunction](self.Module, uUnit)
					self.tPrevious[uUnit:GetId()] = true
					local hookid = uUnit:GetId()
					local data = {self:GetUnitTable(uUnit)}
					self.Module:__SendEvent(self.strEvent, hookid, data)
				end
			else
				-- remove trigger
				self.tPrevious[uUnit:GetId()] = false
			end
		else -- health lost
			if ((100/uUnit:GetMaxHealth()*(uUnit:GetMaxHealth()-uUnit:GetHealth())) >= self.fThreshold) then
				-- trigger
				if not self.tPrevious[uUnit:GetId()] then
					self.Module[self.strFunction](self.Module, uUnit)
					self.tPrevious[uUnit:GetId()] = true
					local hookid = uUnit:GetId()
					local data = {self:GetUnitTable(uUnit)}
					self.Module:__SendEvent(self.strEvent, hookid, data)
				end
			else
				-- remove trigger
				self.tPrevious[uUnit:GetId()] = false
			end
		end
	end
end

function FtrLibHook:CheckEnemyTargetChange()
	local tEnemyUnits = self.Ftr.Unitbox:GetEnemies()
	if tEnemyUnits then
		for k, v in pairs(tEnemyUnits) do
			-- check for target change
			if(v and v:GetTarget()) then
				-- make sure he had a different target before
				if(v:GetTarget():GetId() ~= self.tPrevious[v:GetId()])  then -- new target
					-- trigger event
					self.Module[self.strFunction](self.Module, v, v:GetTarget())
					self.tPrevious[v:GetId()] = v:GetTarget():GetId()
					-- broadcast event
					local hookid = v:GetId()..'_'..v:Target():GetId()
					local data = {self:GetUnitTable(v), self:GetUnitTable(v:GetTarget())}
					self.Module:__SendEvent(self.strEvent, hookid, data)
				end
			end
		end
	end
end

function FtrLibHook:CheckEnemyCast()
	local tEnemyUnits = self.Ftr.Unitbox:GetEnemies()
	if tEnemyUnits then
		for k, v in pairs(tEnemyUnits) do
			if v:GetUnit() then
				v = v:GetUnit()
				-- check for cast
				if v:IsCasting() then
					--make sure this event is not reported already
					if (v:GetCastElapsed() > v:GetCastDuration()) then self.tPrevious[v:GetId()].fDuration = (v:GetCastDuration()+(v:GetCastElapsed()-v:GetCastDuration())) end
					if 	((not self.tPrevious[v:GetId()]) or
						(not self.tPrevious[v:GetId()].strCastName) or
						(self.tPrevious[v:GetId()].strCastName ~= v:GetCastName()) or
						(os.clock() > (self.tPrevious[v:GetId()].fTimestamp+(self.tPrevious[v:GetId()].fDuration/1000)-(self.tPrevious[v:GetId()].fElapsed/1000)))) then
							-- trigger
							self.Module[self.strFunction](self.Module, v, v:GetTarget(), v:GetCastName(), v:GetCastElapsed(), v:GetCastDuration())
							self.tPrevious[v:GetId()] = {strCastName=v:GetCastName(), fElapsed=v:GetCastElapsed(), fDuration=v:GetCastDuration(), fTimestamp=os.clock()}
							-- broadcast event
							local hookid = v:GetId()..'_'..(v:GetCastName():gsub('%W',''))
							local data = {self:GetUnitTable(v), self:GetUnitTable(v:GetTarget()), v:GetCastName(), v:GetCastElapsed(), v:GetCastDuration()}
							self.Module:__SendEvent(self.strEvent, hookid, data)
					end
				else
					self.tPrevious[v:GetId()] = nil
				end
			end
		end
	end
end

function FtrLibHook:CheckPartyDebuff()
	local tUnits = {}
	if (not GroupLib.InGroup()) then
		tUnits[1] = self.Ftr.uPlayer
	else
		for i=1, GroupLib.GetMemberCount() do
			if GroupLib.GetUnitForGroupMember(i) then
				tUnits[i] = GroupLib.GetUnitForGroupMember(i)
			end
		end
	end

	for i=1, #tUnits do
		local uUnit = tUnits[i]

		-- iterate through members, then debuffs
		if(not self.tPrevious[uUnit:GetName()]) then self.tPrevious[uUnit:GetName()] = {} end
		local tBuffs = uUnit:GetBuffs()
		if not tBuffs then return end
		local tCurrentlyApplied = {}
		for k, v in pairs(tBuffs.arHarmful) do
			tCurrentlyApplied[v.splEffect:GetId()] = true
			if(
				(not self:IsBlacklisted(v))
				and (
					(not self.tPrevious[uUnit:GetName()][v.splEffect:GetId()])
					or (self.tPrevious[uUnit:GetName()][v.splEffect:GetId()].nStacks ~= v.nCount)
					or ((self.tPrevious[uUnit:GetName()][v.splEffect:GetId()].fDuration > 0)
						and (os.clock() > (self.tPrevious[uUnit:GetName()][v.splEffect:GetId()].fTimestamp + self.tPrevious[uUnit:GetName()][v.splEffect:GetId()].fDuration)))
				)
			) then
					-- trigger event
					self.Module[self.strFunction](self.Module, uUnit, v)
					self.tPrevious[uUnit:GetName()][v.splEffect:GetId()] = {strName=v.splEffect:GetName() ,nStacks=v.nCount , fDuration=v.fTimeRemaining, fTimestamp=os.clock()}
					-- broadcast event
					local hookid = uUnit:GetId()..'_'..v.splEffect:GetId()
					local data = {self:GetUnitTable(uUnit), self:GetBuffTable(v)}
					self.Module:__SendEvent(self.strEvent, hookid, data)
			end
		end
		-- remove buffs not found anymore
		for k,v in pairs(self.tPrevious[uUnit:GetName()]) do
			if (not tCurrentlyApplied[k]) then self.tPrevious[uUnit:GetName()][k] = nil end
		end
	end
end

function FtrLibHook:CheckEnemyBuff()
	local tEnemyUnits = self.Ftr.Unitbox:GetEnemies()
	if tEnemyUnits then
		for i, uUnit in pairs(tEnemyUnits) do
			if(not self.tPrevious[i]) then self.tPrevious[i] = {} end
			local tBuffs = uUnit:GetBuffs()
			if not tBuffs then return end
			local tCurrentlyApplied = {}
			for k, v in pairs(tBuffs.arBeneficial) do
				tCurrentlyApplied[v.splEffect:GetId()] = true
				if(
					(not self:IsBlacklisted(v))
					and (
						(not self.tPrevious[i][v.splEffect:GetId()])
						or (self.tPrevious[i][v.splEffect:GetId()].nStacks ~= v.nCount)
						or ((self.tPrevious[i][v.splEffect:GetId()].fDuration > 0)
							and (os.clock() > (self.tPrevious[i][v.splEffect:GetId()].fTimestamp + self.tPrevious[i][v.splEffect:GetId()].fDuration)))
					)
				) then
						-- trigger event
						self.Module[self.strFunction](self.Module, uUnit, v)
						self.tPrevious[i][v.splEffect:GetId()] = {nStacks=v.nCount , fDuration=v.fTimeRemaining, fTimestamp=os.clock()}
						-- broadcast event
						local hookid = uUnit:GetId()..'_'..v.splEffect:GetId()
						local data = {self:GetUnitTable(uUnit), self:GetBuffTable(v)}
						self.Module:__SendEvent(self.strEvent, hookid, data)
				end
			end
			-- remove buffs not found anymore
			for k,v in pairs(self.tPrevious[i]) do
				if (not tCurrentlyApplied[k]) then self.tPrevious[i][k] = nil end
			end
		end
	end
end

function FtrLibHook:GetUnitTable(uUnit)
	local tReturn = {
		GetType="tUnitData",
		GetId=uUnit:GetId(),
		GetName=uUnit:GetName(),
		GetClassId=uUnit:GetClassId(),
		GetPosition=uUnit:GetPosition(),
		GetHeading=uUnit:GetHeading(),
		GetFacing=uUnit:GetFacing(),
		GetHealth=uUnit:GetHealth(),
		GetMaxHealth=uUnit:GetMaxHealth(),
		GetTargetMarker=uUnit:GetTargetMarker(),
		GetBuffs=uUnit:GetBuffs()
	}
	return tReturn
end

function FtrLibHook:GetSpellTable(splSpell)
	local tReturn = {
		GetType="tSpellData",
		id=splSpell:GetId()
	}
	return tReturn
end

function FtrLibHook:GetBuffTable(bfBuff)
	local tReturn = {
		GetType="tBuffData",
		fTimeRemaining=bfBuff.fTimeRemaining,
		idBuff=bfBuff.idBuff,
		nCount=bfBuff.nCount,
		splEffect=self:GetSpellTable(bfBuff.splEffect)
	}
	return tReturn
end

function FtrLibHook:IsBlacklisted(buff)
	for k,v in pairs(self.Blacklist) do
		if buff.splEffect:GetName() == v then return true end
	end
	return false
end
Apollo.RegisterPackage(FtrLibHook, "Ftr:Libs:Hook", 1, {})