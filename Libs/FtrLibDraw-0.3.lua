local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local pairs = pairs
local table = table
local Vector3 = Vector3
local tonumber = tonumber
local type = type
local os = os
local Vector = Vector
-- --------------------------------------------------------------
-- ------------------ MODULE CLASS -------------------------
-- --------------------------------------------------------------
local FtrLibDraw  = {}
FtrLibDraw .__index = FtrLibDraw

function FtrLibDraw.create(strName, strType, args, tFormat)
	local self = setmetatable({}, FtrLibDraw)
	self.Points = {}
	self.Endpoints = {}
	self.strName = strName
	self.strType = strType
	self.Args = args
	self.tFormat = tFormat
	self.DistanceFactor = 1
	if tFormat["DistanceFactor"] then self.DistanceFactor = tFormat["DistanceFactor"] end
	if tFormat["NoUpdate"] then self.NoUpdate = true end
	if tFormat["Duration"] then
			self.fDuration = tonumber(tFormat["Duration"])
			self.fLastClock = os.clock()
	end
	if tFormat["Delay"] then
			self.fDelay = tonumber(tFormat["Delay"])
			self.fLastClock = os.clock()
	end
	if self.NoUpdate then self:Render() end
	return self
end

function FtrLibDraw:GetName()
	return self.strName
end

function FtrLibDraw:SetColor(cColor)
	if self.Points then
		for k,v in pairs(self.Points) do
			v:SetBGColor(cColor)
			self.tFormat["BGColor"] = cColor
		end
		return true
	end
	return false
end

function FtrLibDraw:SetSprite(Sprite)
	if self.Points then
		for k,v in pairs(self.Points) do
			v:SetSprite(Sprite)
		end
		return true
	end
	return false
end

-- --------------------------------------------------------------
-- ----------------- LINE FUNCTIONS ---------------------------
-- --------------------------------------------------------------
function FtrLibDraw:DrawLineToUnitFacing(uUnit, ang, dst)
	--Print(("Draw from Unit %s (%s/%s/%s"):format())
	if not uUnit then return false end
	if not dst then return false end
	if not ang then return false end
	local vF = uUnit:GetFacing() if not vF then return false end
	local vP = uUnit:GetPosition() if not vP then return false end
	ang = (math.atan2(vF.x, vF.z))+math.rad(ang)

	local dst = math.floor(dst/self.DistanceFactor)
	for k=1, dst do
		if not self.Points[k] then self.Points[k] = self:LoadPoint(self.tFormat) end
		if not self.Points[k] then return false end
		local v1 = Vector3.New(vP.x, vP.y, vP.z)
		local v2 = Vector3.New(vP.x+dst*math.sin(ang), vP.y, vP.z+dst*math.cos(ang))
		self.Points[k]:SetWorldLocation(Vector3.InterpolateLinear(v1, v2, (1/dst)*k))
		if not self.Points[k]:IsOnScreen() then self.Points[k]:Show(false)
		else self.Points[k]:Show(true) end
	end
	return true
end

function FtrLibDraw:DrawLineBetweenUnits(u1, u2)
	if not u1 then return false end
	if not u2 then return false end
	local vu1 = u1:GetPosition() if not vu1 then return false end
	local vu2 = u2:GetPosition() if not vu2 then return false end

	-- get distance between units
	local dst = math.floor(self:DistanceBetweenPoints(vu1, vu2)/self.DistanceFactor)
	for k=1, dst+1 do
		if not self.Points[k] then self.Points[k] = self:LoadPoint(self.tFormat) end
		local v1 = Vector3.New(vu1.x, vu1.y, vu1.z)
		local v2 = Vector3.New(vu2.x, vu2.y, vu2.z)
		self.Points[k]:SetWorldLocation(Vector3.InterpolateLinear(v1, v2, (1/dst*k)))
		if ((u1 == GameLib.GetPlayerUnit()) or (u2 == GameLib.GetPlayerUnit())) then
			self.Points[k]:SetRotation(self:CalculateRotation(v2, v1))
		end
		if not self.Points[k]:IsOnScreen() then self.Points[k]:Show(false)
		else self.Points[k]:Show(true) end
	end
	if (dst < #self.Points) then
		for k=dst, #self.Points do
			if self.Points[k] then self.Points[k]:Destroy() self.Points[k] = nil end
		end
	end
	return true
end

function FtrLibDraw:DrawLineBetweenUnitPoint(u, p)
	if not u then return false end
	if not p then return false end
	local vu = u:GetPosition() if not vu then return false end
	if not p then return false end

	local dst = math.floor(self:DistanceBetweenPoints(vu, p)/self.DistanceFactor)
	for k=1, dst+1 do
		if not self.Points[k] then self.Points[k] = self:LoadPoint(self.tFormat) end
		local v1 = Vector3.New(vu.x, vu.y, vu.z)
		local v2 = Vector3.New(p.x, p.y, p.z)
		self.Points[k]:SetWorldLocation(Vector3.InterpolateLinear(v1, v2, (1/dst)*k))
		if (u == GameLib.GetPlayerUnit()) then
			self.Points[k]:SetRotation(self:CalculateRotation(v2, v1))
		end
		if not self.Points[k]:IsOnScreen() then self.Points[k]:Show(false)
		else self.Points[k]:Show(true) end
	end
	if (dst < #self.Points) then
		for k=dst, #self.Points do
			if self.Points[k] then self.Points[k]:Destroy() self.Points[k] = nil end
		end
	end
	return true
end

function FtrLibDraw:DrawLineBetweenPoints(p1, p2)
	if not p1 then return false end
	if not p2 then return false end

	local dst = math.floor(self:DistanceBetweenPoints(p1, p2)/self.DistanceFactor)
	for k=1, dst+1 do
		if not self.Points[k] then self.Points[k] = self:LoadPoint(self.tFormat) end
		local v1 = Vector3.New(p1.x, p1.y, p1.z)
		local v2 = Vector3.New(p2.x, p2.y, p2.z)
		self.Points[k]:SetWorldLocation(Vector3.InterpolateLinear(v1, v2, (1/dst)*k))
		if not self.Points[k]:IsOnScreen() then self.Points[k]:Show(false)
		else self.Points[k]:Show(true) end
	end
	if (dst < #self.Points) then
		for k=dst, #self.Points do
			if self.Points[k] then self.Points[k]:Destroy() self.Points[k] = nil end
		end
	end
	return true
end

-- --------------------------------------------------------------
-- ----------------- CIRCLE FUNCTIONS ---------------------------
-- --------------------------------------------------------------

function FtrLibDraw:DrawCircleAroundUnit(u, rad)
	if not u then return false end
	if not rad then return false end
	local vu = Vector3.New(u:GetPosition().x, u:GetPosition().y, u:GetPosition().z) if not vu then return false end
	local vf = u:GetFacing() if not vf then return false end
	if not rad then return false end


	local baseAng = math.atan2(u:GetFacing().x, u:GetFacing().z)

	for k=0, math.floor(360/self.DistanceFactor) do
		if not self.Points[k] then self.Points[k] = self:LoadPoint(self.tFormat) end

		local thisAng = baseAng+(math.rad(k*self.DistanceFactor))
		if thisAng > 180 then thisAng = 180-thisAng end

		local v = Vector3.InterpolateLinear(vu, Vector3.New(vu.x+rad*math.sin(thisAng), vu.y, vu.z+rad*math.cos(thisAng)), 1)
		self.Points[k]:SetWorldLocation(v)
		if not self.Points[k]:IsOnScreen() then self.Points[k]:Show(false)
		else self.Points[k]:Show(true) end
	end

	return true
end

function FtrLibDraw:DrawArcAroundUnit(u, curvescale, ang1, ang2, rad1, rad2)
	if not u then return false end
	if not curvescale then return false end
	if not ang1 then return false end
	if not ang2 then return false end
	if not rad1 then return false end
	if not rad2 then rad2=rad1 end
	local vu = Vector3.New(u:GetPosition().x, u:GetPosition().y, u:GetPosition().z) if not vu then return false end
	local vf = u:GetFacing() if not vf then return false end
	--Print("Drawing Arc")
	local baseAng = math.atan2(vf.x, vf.z)

	local p1 = Vector3.InterpolateLinear(vu, Vector3.New((vu.x+rad1*math.sin(baseAng+math.rad(ang1))), vu.y, (vu.z+rad1*math.cos(baseAng+math.rad(ang1)))), 1)
	local p2 = Vector3.InterpolateLinear(vu, Vector3.New((vu.x+rad2*math.sin(baseAng+math.rad(ang2))), vu.y, (vu.z+rad2*math.cos(baseAng+math.rad(ang2)))), 1)
	local bezier = Vector3.InterpolateLinear(vu, Vector3.New((vu.x+(rad1+rad2)/2*curvescale*math.sin(baseAng+math.rad((ang1+ang2)/2))), vu.y, (vu.z+(rad1+rad2)/2*curvescale*math.cos(baseAng+math.rad((ang1+ang2)/2)))), 1)

	local iv = 0.01*self.DistanceFactor
	for t=0.00, 1, iv do
		local x = ((1-t)*(1-t)*p1.x + 2*(1-t)*t*bezier.x+t*t*p2.x);
		local z = ((1-t)*(1-t)*p1.z + 2*(1-t)*t*bezier.z+t*t*p2.z);

		if not self.Points[t*100] then self.Points[t*100] = self:LoadPoint(self.tFormat) end
		local v = Vector3.New(x, p1.y, z)
		self.Points[t*100]:SetWorldLocation(v)
		if not self.Points[t*100]:IsOnScreen() then self.Points[t*100]:Show(false)
		else self.Points[t*100]:Show(true) end
	end

	return true
end

function FtrLibDraw:DrawSpikedArcAroundUnit(u, rad, angstart, angend, arcsc)
	if not u then return false end
	if not rad then return false end
	local vu = Vector3.New(u:GetPosition().x, u:GetPosition().y, u:GetPosition().z) if not vu then return false end
	local vf = u:GetFacing() if not vf then return false end
	if not rad then return false end

	local baseAng = math.atan2(u:GetFacing().x, u:GetFacing().z)

	for k=angstart, angend do
		local degreediff
		if not self.Points[k] then self.Points[k] = self:LoadPoint(self.tFormat) end

		local thisAng = baseAng+(math.rad(k))
		if thisAng > 180 then thisAng = 180-thisAng end

		if (k <= ((angstart+angend)/2)) then degreediff = math.sqrt(math.pow(k-angstart, 2))
		else degreediff = math.sqrt(math.pow(angend-k, 2)) end
		local scale = (0.1/((angstart+angend)/2)*degreediff*arcsc)+1

		local v = Vector3.InterpolateLinear(vu, Vector3.New((vu.x+rad*scale*math.sin(thisAng)), vu.y, (vu.z+rad*scale*math.cos(thisAng))), 1)
		self.Points[k]:SetWorldLocation(v)
		if not self.Points[k]:IsOnScreen() then self.Points[k]:Show(false)
		else self.Points[k]:Show(true) end
	end

	return true
end

-- --------------------------------------------------------------
-- ----------------- RECTANGLE FUNCTIONS ---------------------------
-- --------------------------------------------------------------

function FtrLibDraw:DrawRegPolyAroundUnit(u, dst, rad, corners)
	if not u then return false end
	if not rad then return false end
	local vu = Vector3.New(u:GetPosition().x, u:GetPosition().y, u:GetPosition().z) if not vu then return false end
	local vf = u:GetFacing() if not vf then return false end
	if not rad then return false end
	if not corners then corners = 4 end

	-- calculate corners
	local baseRad = math.atan2(vf.x, vf.z)+math.rad(rad)
	for k=0, (corners-1) do
		local thisRad = baseRad+(math.rad(360/corners)*k)
		if thisRad > 180 then thisRad = 180-thisRad end
		self.Endpoints[k+1]=Vector3.InterpolateLinear(vu, Vector3.New(vu.x+dst*math.sin(thisRad), vu.y, vu.z+dst*math.cos(thisRad)), 1)
	end

	-- draw lines between corners
	local lastdst = 0
	for k=1, corners do
		local to
		if (k == corners) then to = 1
		else to = k+1 end
		local dst = math.floor(self:DistanceBetweenPoints(self.Endpoints[k], self.Endpoints[to])/self.DistanceFactor)
		for i=(lastdst+1), (dst+(lastdst)) do
			if not self.Points[i] then self.Points[i] = self:LoadPoint(self.tFormat) end
			local v1 = Vector3.New(self.Endpoints[k].x, self.Endpoints[k].y, self.Endpoints[k].z)
			local v2 = Vector3.New(self.Endpoints[to].x, self.Endpoints[to].y, self.Endpoints[to].z)
			self.Points[i]:SetWorldLocation(Vector3.InterpolateLinear(v1, v2, (1/dst*(i-lastdst))))
			if not self.Points[i]:IsOnScreen() then self.Points[i]:Show(false)
			else self.Points[i]:Show(true) end
		end
		lastdst = dst+lastdst
	end

	return true
end

function FtrLibDraw:CalcRegPolyAroundUnit(u, dst, rad, corners)
	if not u then return false end
	if not rad then return false end
	local vu = Vector3.New(u:GetPosition().x, u:GetPosition().y, u:GetPosition().z) if not vu then return false end
	local vf = u:GetFacing() if not vf then return false end
	if not rad then return false end
	if not corners then corners = 4 end

	-- calculate corners
	local Endpoints = {}
	local baseRad = math.atan2(vf.x, vf.z)+math.rad(rad)
	for k=0, (corners-1) do
		local thisRad = baseRad+(math.rad(360/corners)*k)
		if thisRad > 180 then thisRad = 180-thisRad end
		Endpoints[k+1]=Vector3.InterpolateLinear(vu, Vector3.New(vu.x+dst*math.sin(thisRad), vu.y, vu.z+dst*math.cos(thisRad)), 1)
	end

	return Endpoints
end

-- --------------------------------------------------------------
-- ----------------- GENERAL FUNCTIONS ---------------------------
-- --------------------------------------------------------------
function FtrLibDraw:CalculateRotation(vP2, vP1)
	return self:OffsetHeading(math.atan2(vP2.z - vP1.z, vP2.x - vP1.x))
end

function FtrLibDraw:Magnitude(v)
	return math.sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z))
end

function FtrLibDraw:Normalize(v)
	local m = self:Magnitude(v)
	return Vector3.New((v.x/m), (v.y/m), (v.z/m))
end

function FtrLibDraw:OffsetHeading(rot)
	local plH = GameLib.GetPlayerUnit():GetHeading()
	if plH < 0 then plH = plH * -1
	else plH = 2 * math.pi - plH end
	return math.deg(rot - plH) + 90
end

function FtrLibDraw:Render()
	if self.fDelay and self.fDelay > 0 then
		self.fDelay = self.fDelay - (os.clock()-self.fLastClock)
		self.fLastClock = os.clock()
		return true
	elseif self.fDelay and self.fDelay<=0 then
		self.fDelay = nil
	end
	if self.fDuration then
		self.fDuration = self.fDuration - (os.clock()-self.fLastClock)
		self.fLastClock = os.clock()
	end
	if self.bHidden or self.bNoUpdate then return true end
	local ret
	if     self.strType == "DrawLineToUnitFacing" then ret = self:DrawLineToUnitFacing(unpack(self.Args))
	elseif self.strType == "DrawLineBetweenUnits" then ret = self:DrawLineBetweenUnits(unpack(self.Args))
	elseif self.strType == "DrawLineBetweenUnitPoint" then ret = self:DrawLineBetweenUnitPoint(unpack(self.Args))
	elseif self.strType == "DrawLineBetweenPoints" then ret = self:DrawLineBetweenPoints(unpack(self.Args))
	elseif self.strType == "DrawCircleAroundUnit" then ret = self:DrawCircleAroundUnit(unpack(self.Args))
	elseif self.strType == "DrawArcAroundUnit" then ret = self:DrawArcAroundUnit(unpack(self.Args))
	elseif self.strType == "DrawRegPolyAroundUnit" then ret = self:DrawRegPolyAroundUnit(unpack(self.Args))
	else ret = false
	end
	if not ret then return false
	else return true end
end

function FtrLibDraw:GetDuration()
	if self.fDuration then return self.fDuration
	else return false end
end

function FtrLibDraw:Hide()
	if self.bHidden then return true end
	if self and self.Points then
		for k,v in pairs(self.Points) do
			v:Show(false)
		end
		self.bHidden = true
		return true
	end
	return false
end

function FtrLibDraw:Show()
	self.bHidden = false
	if self and self.Points then
		for k,v in pairs(self.Points) do
			v:Show(true)
		end
		return true
	end
	return false
end

function FtrLibDraw:LoadPoint(tFormat)
	local Point = Apollo.LoadForm("Libs/FtrLibDraw.xml", "Point", "InWorldHudStratum", self)

	if tFormat["BGColor"] then Point:SetBGColor(tFormat["BGColor"]) end
	if tFormat["Sprite"] then Point:SetSprite(tFormat["Sprite"]) end
	local l,t,r,b = Point:GetAnchorOffsets()
	if not tFormat["Width"] and not tFormat["Height"] then Point:SetAnchorOffsets(l,t, 25, 25) end
	if tFormat["Width"] then l,t,r,b = Point:GetAnchorOffsets() Point:SetAnchorOffsets(l,t,tFormat["Width"],b) end
	if tFormat["Height"] then l,t,r,b = Point:GetAnchorOffsets() Point:SetAnchorOffsets(l,t,r,tFormat["Height"]) end

	return Point
end

function FtrLibDraw:DistanceBetweenPoints(vector1, vector2)
	if (type(vector1)=="table") then vector1 = Vector3.New(vector1.x, vector1.y, vector1.z) end
	if (type(vector2)=="table") then vector1 = Vector3.New(vector2.x, vector2.y, vector2.z) end
	return math.sqrt(math.pow(vector2.x-vector1.x, 2)+math.pow(vector2.y-vector1.y, 2)+math.pow(vector2.z-vector1.z, 2))
end

function FtrLibDraw:GetFacingAngleToPoint(uUnit, vP)
	local pF = (math.atan2((vP.z-uUnit:GetPosition().z), (vP.x-uUnit:GetPosition().x)))
	local uF = uUnit:GetHeading()
	if (uF < 0) then uF=uF*(-1)
	else uF=2*math.pi-uF end
	return (math.deg(pF-uF)+90)
end

function FtrLibDraw:IsFacingUnit(u1, u2, tolfac)
	local pF = (math.atan2((u2:GetPosition().z-u1:GetPosition().z), (u2:GetPosition().x-u1:GetPosition().x)))
	local uF = u1:GetHeading()
	if (uF < 0) then uF=uF*(-1)
	else uF=2*math.pi-uF end
	local ang = math.floor((math.deg(pF-uF)+90)+0.5)
	local dst = self:DistanceBetweenPoints(u1:GetPosition(), u2:GetPosition())
	local tol = 1*dst/20*tolfac
	if (ang <= tol and ang >= 0) or (ang >= (tol*(-1)) and ang <= 0) then return true
	else return false end
end

function FtrLibDraw:IsBetween(vP, tPoints)
	if (not tPoints) then tPoints = self.Endpoints end

	if ((tPoints) and (#tPoints > 1)) then
		for i=1, #tPoints do
			local to
			if (i == #tPoints) then to = 1
			else to = i+1 end
			if ((vP.x-tPoints[i].x)*(tPoints[to].x-tPoints[i].x)+(vP.z-tPoints[i].z)*(tPoints[to].z-tPoints[i].z) < 0.0) then return false end
		end
	end
	return true
end

function FtrLibDraw:Destroy()
	for k,v in pairs(self.Points) do
		v:Show(false)
		v:Destroy()
		v = nil
	end
	self.Points = {}
	self.EndPoints = {}
	self.Args = nil
	self = nil

	return nil
end

function FtrLibDraw:Export()
	return {self.strName, self.strType, self.Args, self.tFormat}
end

Apollo.RegisterPackage(FtrLibDraw, "Ftr:Libs:Draw", 1, {})