require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"
local Apollo = Apollo
local GameLib = GameLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local ipairs = ipairs
local pairs = pairs
local table = table
local os = os

local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrPrototypes = Ftr:RegisterModule("Ftr:Prototypes")

function FtrPrototypes:OnLoad()
	-- ----------------------
	-- Register Handlers
	-- ----------------------
	self:RegisterEventHandler("Start", "OnStart", self)
	self:RegisterEventHandler("Stop", "OnStop", self)
	self:RegisterEventHandler("EnemyCast", "OnEnemyCast", self)
	self:RegisterEventHandler("UnitEnteredCombat", "OnUnitEnteredCombat", self)
	self:RegisterEventHandler("PartyDebuff", "OnPartyDebuff", self)
	self:RegisterStartCondition("UnitEnteredCombat", "Phagetech Commander", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Phagetech Augmentor", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Phagetech Protector", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Phagetech Fabricator", true)
end

function FtrPrototypes:OnStart()
	Print("Phagetech Prototypes engaged, good luck!")
	self:SetPhase(1)
end

function FtrPrototypes:OnStop()
	Print("Wipe on Phagetech Prototypes detected, resetting Module.")
end

function FtrPrototypes:OnEnemyCast(uUnit, uTarget, strCastname, fElapsed, fDuration)
	-- -----------------------------------------------
	-- ---------------- COMMANDER --------------------
	-- -----------------------------------------------
	if uUnit:GetName() == "Phagetech Commander" then
		if strCastname == "Destruction Protocol" then -- tank smash
			self:ShowTimer(
				"Rw_DestructionProtocol",
				"Tank Smash",
				5
			)
		elseif strCastname == "Forced Production" then -- interrupt warning
			self:ShowWarning("Commander Interrupt!")
		end
	-- -----------------------------------------------
	-- ---------------- AUGMENTOR --------------------
	-- -----------------------------------------------
	elseif uUnit:GetName() == "Phagetech Augmentor" then
		if strCastname == "Summon Repairbot" then -- healprobes
			self:ShowTimer(
				"Aug_Probes",
				("Maintenance Probes!"),
				((fDuration-fElapsed)/1000),
				{strTextColor="FFFF0000", Window="BigTimer"}
			)
		end
	-- -----------------------------------------------
	-- ---------------- PROTECTOR --------------------
	-- -----------------------------------------------
	elseif uUnit:GetName() == "Phagetech Protector" then
		if strCastname == "Gravitational Singularity" then -- singularity
			self:ShowWarning("Singularity")
		end
	-- -----------------------------------------------
	-- --------------- FABRICATOR --------------------
	-- -----------------------------------------------
	elseif uUnit:GetName() == "Phagetech Fabricator" then
		if strCastname == "Sparks" then -- sparks
			self:ShowWarning("Catch Sparks")
		elseif strCastname == "Summon Destructobots" then -- destro probes
			self:ShowTimer(
				"Fab_Probes",
				"Destro Probes!",
				((fDuration-fElapsed)/1000),
				{Window="BigTimer", strTextColor="FFFF0000"}
			)
		end
	end
end

function FtrPrototypes:OnPartyDebuff(uUnit, Debuff)
	Print(uUnit:GetName().." got "..Debuff.splEffect:GetName())
	if Debuff.splEffect:GetName() == "Malicious Uplink" then
		self:ShowTimer(
			("MaliciousUplink"..os.clock()),
			("Malicious Uplink on %s"):format(uUnit:getName()),
			10
		)
	end
end

function FtrPrototypes:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Phagetech Commander" then
				self.Bosses.Commander = {unit=uUnit, state=false}
			elseif uUnit:GetName() == "Phagetech Augmentor" then
				self.Bosses.Augmentor = {unit=uUnit, state=false}
			elseif uUnit:GetName() == "Phagetech Protector" then
				self.Bosses.Protector = {unit=uUnit, state=false}
			elseif uUnit:GetName() == "Phagetech Fabricator" then
				self.Bosses.Fabricator = {unit=uUnit, state=false}
			end
		end
	end
end

function FtrPrototypes:SetPhase(phase)
	if type(phase)=="string" and phase == "next" then
		if self.EncounterPhase >= 5 then phase = 2
		else phase = self.EncounterPhase + 1
		end
	elseif self.EncounterPhase ~= phase then
		if phase == 1 then -- Commander
			self:SetBossState("Commander", true)
			-- 20 sec timer for augmentor (maint probes)
			self:ShowTimer(
				"PhaseTimer",
				("Augmentor up"),
				20,
				{
					{"OnTimeLeft", 10, "MoveTo", "BigTimer"},
					{"OnExpire", function() self:SetPhase("next") end}
				}
			)
		end
		if phase == 2 then -- Commander+Augmentor
			self:SetBossState("Fabricator", false)
			self:SetBossState("Augmentor", true)
			-- 60 sec timer for protector (singularity)
			self:ShowTimer(
				"PhaseTimer",
				("Protector up, Commander down"),
				60,
				{
					{"OnTimeLeft", 10, "MoveTo", "BigTimer"},
					{"OnExpire", function() self:SetPhase("next") end}
				}
			)
		end
		if phase == 3 then -- Augmentor+Protector
			self:SetBossState("Commander", false)
			self:SetBossState("Protector", true)
			-- 60 sec timer for fabricator (sparks)
			self:ShowTimer(
				"PhaseTimer",
				("Fabricator up, Augmentor down"),
				60,
				{
					{"OnTimeLeft", 10, "MoveTo", "BigTimer"},
					{"OnExpire", function() self:SetPhase("next") end}
				}
			)
		end
		if phase == 4 then -- Protector+Fabricator
			self:SetBossState("Augmentor", false)
			self:SetBossState("Fabricator", true)
			-- 60 sec timer for commander (bad powerlink)
			self:ShowTimer(
				"PhaseTimer",
				("Commander up, Protector down"),
				60,
				{
					{"OnTimeLeft", 10, "MoveTo", "BigTimer"},
					{"OnExpire", function() self:SetPhase("next") end}
				}
			)
		end
		if phase == 5 then -- Fabricator+Commander
			self:SetBossState("Protector", false)
			self:SetBossState("Commander", true)
			-- 60 sec timer for augmentor (maint probes)
			self:ShowTimer(
				"PhaseTimer",
				("Protector up, Commander down"),
				60,
				{
					{"OnTimeLeft", 10, "MoveTo", "BigTimer"},
					{"OnExpire", function() self:SetPhase("next") end}
				}
			)
		end
		self.EncounterPhase = phase
	end
end

function FtrPrototypes:SetBossState(strBoss, bState)
	if self.Bosses[strBoss] then self.Bosses[strBoss].state = bState end
end