require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"
local Apollo = Apollo
local GameLib = GameLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local ipairs = ipairs
local pairs = pairs
local table = table
local os = os
local string = string

local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrPhagemaw = Ftr:RegisterModule("Ftr:Phagemaw")

function FtrPhagemaw:OnLoad()
	-- ----------------------
	-- Register Handlers
	-- ----------------------
	self:RegisterEventHandler("Start", "OnStart", self)
	self:RegisterEventHandler("Stop", "OnStop", self)
	self:RegisterEventHandler("EnemyCast", "OnEnemyCast", self)
	self:RegisterEventHandler("UnitEnteredCombat", "OnUnitEnteredCombat", self)
	self:RegisterEventHandler("Story", "OnStory", self)
	self:RegisterEventHandler("Chat", "OnChatMessage", self)
	self:RegisterStartCondition("UnitEnteredCombat", "Phage Maw", true)
end

function FtrPhagemaw:OnStart()
	Print("Phage Maw engaged, good luck!")
end

function FtrPhagemaw:OnStop()
	Print("Wipe on Phage Maw detected, resetting Module.")
end

function FtrPhagemaw:OnEnemyCast(uUnit, uTarget, strCastname, fElapsed, fDuration)
	if uUnit:GetName() == "Phage Maw" then
		if strCastname == "Aerial Bombardment" then
			self:ShowTimer(
				"Timer_GroundPhase",
				("Ground Phase"),
				fDuration/1000,
				{{"OnTimeLeft", 15, "MoveTo", "BigTimer"}}
			)
		elseif strCastname == "Crater" then
			self:ShowTimer(
				"Timer_GroundPhase",
				("Ground Phase"),
				fDuration/1000,
				{{"OnTimeLeft", 15, "MoveTo", "BigTimer"}}
			)
		end
	end
end

function FtrPhagemaw:OnStory(strMessage)
	if strMessage == "The augmented shield has been destroyed!" then
		self:DestroyTimer("Timer_GroundPhase")

		self:ShowTimer(
			"Timer_AirPhase",
			("Air Phase"),
			104,
			{{"OnTimeLeft", 15, "MoveTo", "BigTimer"}}
		)
	end
end

function FtrPhagemaw:OnChatMessage(channelCurrent, strMessage)
	if channelCurrent:GetName() == "Datachron" then
		if strMessage == "The augmented shield has been destroyed!" then
			self:DestroyTimer("Timer_GroundPhase")

			self:ShowTimer(
				"Timer_AirPhase",
				("Air Phase"),
				104,
				{{"OnTimeLeft", 15, "MoveTo", "BigTimer"}}
			)
		end
	end
end

function FtrPhagemaw:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Phage Maw" then
				self.Boss = uUnit
			end
		end
	end
end