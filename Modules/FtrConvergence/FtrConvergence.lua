require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"
local Apollo = Apollo
local GameLib = GameLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local ipairs = ipairs
local pairs = pairs
local table = table
local os = os
local Unit = Unit
local type = type

local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrConvergence = Ftr:RegisterModule("Ftr:Convergence")

function FtrConvergence:OnLoad()
	-- ----------------------
	-- Register Handlers
	-- ----------------------
	self:RegisterEventHandler("Start", "OnStart", self)
	self:RegisterEventHandler("Stop", "OnStop", self)
	self:RegisterEventHandler("EnemyCast", "OnEnemyCast", self)
	self:RegisterEventHandler("UnitEnteredCombat", "OnUnitEnteredCombat", self)
	self:RegisterEventHandler("ApplyCCState", "OnApplyCCState", self)
	self:RegisterEventHandler("RemoveCCState", "OnRemoveCCState", self)
	self:RegisterStartCondition("UnitEnteredCombat", "Golgox the Lifecrusher", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Fleshmonger Vratorg", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Ersoth Curseform", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Terax Blightweaver", true)
	self:RegisterStartCondition("UnitEnteredCombat", "Noxmind the Insidious", true)
end

function FtrConvergence:OnStart()
	Print("Phageborn Convergence engaged, good luck!")

	self.Bosses = {}
	self.MiddlePhasesDone = {}
	self.EncounterPhase = 1
	self:ShowTimer(
		"Tm_Middlephase",
		("Next Middle Phase"),
		(90),
		{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
	)
end

function FtrConvergence:OnStop()
	Print("Wipe on Phageborn Convergence detected, resetting Module.")
end

function FtrConvergence:OnApplyCCState(number, uUnit, number2)
	-- if its a boss unit and if we were in middle phase, recalc middle phase timer
	if ((uUnit:GetName() == self.Bosses.Noxmind:GetName()) or (uUnit:GetName() == self.Bosses.Golgox:GetName()) or (uUnit:GetName() == self.Bosses.Vratorg:GetName()) or
		(uUnit:GetName() == self.Bosses.Ersoth:GetName()) or (uUnit:GetName() == self.Bosses.Terax:GetName())) and (self.EncounterPhase == 2) then
		self:ShowTimer(
			"Tm_Middlephase_End",
			"Middle Phase End",
			uUnit:GetCCStateTimeRemaining(Unit.CodeEnumCCState.Vulnerability),
			{Window="BigTimer"}
		)
	end
end

function FtrConvergence:OnRemoveCCState(number, uUnit, number2)
	-- if its a boss unit and if we were in middle phase, recalc middle phase timer
	if (uUnit:GetName() == self.MiddleMob:GetName()) and (self.EncounterPhase == 2) then
		-- look for middle phase end timer
		self:SetPhase(1)
		self.MiddleMob = nil
	end
end

function FtrConvergence:OnEnemyCast(uUnit, uTarget, strCastname, fElapsed, fDuration)
	if strCastname == "Teleport" then
		self.MiddlePhasesDone[#self.MiddlePhasesDone+1] = uUnit:Name()
		self:SetPhase(2)
		self.MiddleMob = uUnit
		self:ShowWarning("Rw_Middlephase", ("%s!"):format(uUnit:GetName()), 3)

		self:ShowTimer(
			"Tm_Middlephase",
			(uUnit:GetName().." Middle"),
			((fDuration-fElapsed)/1000),
			{Window="BigTimer"}
		)
	elseif strCastname == "Gathering Energy" then
		self:SetPhase(2)
		self.MiddleMob = uUnit
		self:ShowTimer(
			"Tm_Middlephase_End",
			("Middle Phase End"),
			((fDuration-fElapsed)/1000),
			{Window="BigTimer", {"OnExpire", (function() self:SetPhase(1) end)}}
		)
	end
	-- -----------------------------------------------
	-- ---------------- Noxmind --------------------
	-- -----------------------------------------------
	if uUnit:GetName() == "Noxmind the Insidious" then
		if strCastname == "Essence Rot" then -- wave
			Apollo.RegisterTimerHandler("WaveDelay", "OnWaveDelay", self)
			Apollo.CreateTimer("WaveDelay", 1, false)
		end
	-- -----------------------------------------------
	-- ---------------- Terax --------------------
	-- -----------------------------------------------
	elseif uUnit:GetName() == "Terax Blightweaver" then
		if strCastname == "Stitching Strain" then
			self:ShowWarning("Terax Interrupt!")
			self:ShowTimer(
				"Tm_Heal",
				("Stitching Strain"),
				((fDuration-fElapsed)/1000),
				{Window="BigTimer"}
			)
		end
	-- -----------------------------------------------
	-- ---------------- Golgox -----------------------
	-- -----------------------------------------------
	elseif uUnit:GetName() == "Golgox the Lifecrusher" then
	-- -----------------------------------------------
	-- --------------- Vratorg ------------------------
	-- -----------------------------------------------
	elseif uUnit:GetName() == "Fleshmonger Vratorg" then
	end
end

function FtrConvergence:OnWaveDelay()
	local wavetarget = ""
	if     self.__Trigonometry:IsFactingUnit(self.Bosses.Noxmind, self.Bosses.Golgox) == 0 then wavetarget = "Golgox"
	elseif self.__Trigonometry:IsFactingUnit(self.Bosses.Noxmind, self.Bosses.Terax) == 0 then wavetarget = "Terax"
	elseif self.__Trigonometry:IsFactingUnit(self.Bosses.Noxmind, self.Bosses.Ersoth) == 0 then wavetarget = "Ersoth"
	elseif self.__Trigonometry:IsFactingUnit(self.Bosses.Noxmind, self.Bosses.Vratorg) == 0 then wavetarget = "Vratorg"
	end
	self:ShowWarning(("Wave on %s"):format(wavetarget))
	self:ShowDrawing("Wave", "DrawLineToUnitFacing", {self.Bosses.Noxmind, 0, 240}, {BGColor=self.__Ftr:hexToCColor("ebc806", "1"), NoUpdate=true, Delay=1.0, Duration=12.0})
end

function FtrConvergence:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Golgox the Lifecrusher" then
				self.Bosses.Golgox = uUnit
			elseif uUnit:GetName() == "Fleshmonger Vratorg" then
				self.Bosses.Vratorg = uUnit
			elseif uUnit:GetName() == "Ersoth Curseform" then
				self.Bosses.Ersoth = uUnit
			elseif uUnit:GetName() == "Terax Blightweaver" then
				self.Bosses.Terax = uUnit
			elseif uUnit:GetName() == "Noxmind the Insidious" then
				self.Bosses.Noxmind = uUnit
			end
		end
	end
end

function FtrConvergence:SetPhase(phase)
	if type(phase)=="string" and phase == "next" then
		if self.EncounterPhase == 1 then self:SetPhase(2)
		elseif self.EncounterPhase == 2 then self:SetPhase(1)
		end
	end

	if phase ~= self.EncounterPhase then
		if phase == 1 then -- normal
			Print("Back to Phase 1")
			-- get which bosses can be in middle
			local strNextBosses = ""
			-- if (#self.MiddlePhasesDone) >= 4 then
			-- 	strNextBosses = "Next Middle: Any"
			-- 	self.MiddlePhasesDone = {}
			-- else
			-- 	local tNextBosses = {Noxmind="Noxmind", Golgox="Golgox", Terax="Terax", Ersoth="Ersoth", Vratorg="Vratorg"}
			-- 	for k,v in pairs(self.MiddlePhasesDone) do
			-- 		if self.Bosses.Noxmind and v==self.Bosses.Noxmind:GetName() then tNextBosses.Noxmind = nil end
			-- 		if self.Bosses.Golgox and v==self.Bosses.Golgox:GetName() then tNextBosses.Golgox = nil end
			-- 		if self.Bosses.Terax and v==self.Bosses.Terax:GetName() then tNextBosses.Terax = nil end
			-- 		if self.Bosses.Ersoth and v==self.Bosses.Ersoth:GetName() then tNextBosses.Ersoth = nil end
			-- 		if self.Bosses.Vratorg and v==self.Bosses.Vratorg:GetName() then tNextBosses.Vratorg = nil end
			-- 	end
			-- 	for k,v in pairs(tNextBosses) do
			-- 		if v then strNextBosses = (strNextBosses .. ", " .. v) end
			-- 	end
			-- 	strNextBosses = "Next Middle: "..strNextBosses:sub(3)
			-- end
			strNextBosses = "Next Middle: Any"
			-- set middle phase timer
			self:ShowTimer(
				"Tm_Middlephase",
				strNextBosses,
				(45),
				{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
			)
			self.EncounterPhase = 1
		elseif phase == 2 then -- middle
			Print("Phase 2")
			self.EncounterPhase = 2
		end
	end

end

function FtrConvergence:SetBossState(strBoss, bState)
	if self.Bosses[strBoss] then self.Bosses[strBoss].state = bState end
end