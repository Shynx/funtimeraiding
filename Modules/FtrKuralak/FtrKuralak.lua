require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"

local FtrKuralak  = {}
local pairs = pairs
local ipairs = ipairs
local setmetatable = setmetatable
local Print = Print

local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrKuralak = Ftr:RegisterModule("Ftr:Kuralak")

function FtrKuralak:OnLoad()
	self:RegisterEventHandler("Start", "OnStart", self)
	self:RegisterEventHandler("Stop", "OnStop", self)
	self:RegisterEventHandler("Chat", "OnChatMessage", self)
	self:RegisterEventHandler("Story", "OnStory", self)
	self:RegisterEventHandler("UnitEnteredCombat", "OnUnitEnteredCombat", self)
	self:RegisterEventHandler("Timer", "OnTimer", self)
	self:RegisterStartCondition("UnitEnteredCombat", "Kuralak the Defiler", true)
end

function FtrKuralak:OnStart()
	Print("Kuralak the Defiler engaged, good luck!")
	self:SetPhase(1)
end

function FtrKuralak:OnStop()
	Print("Wipe on Kuralak detected, resetting Module.")
end

function FtrKuralak:OnTimer()
	if (self.uBoss:GetMaxHealth()/100*self.uBoss:GetHealth()) < 72 then Print("Phase 2") self:SetPhase(2) end
end

function FtrKuralak:OnChatMessage(channelCurrent, strMessage, strSender)
	if channelCurrent:GetName() == "NPC Say" then
		if strSender == "Kuralak the Defiler" then
			if strMessage == "Through the Strain you will be transformed..." then self.SetPhase(2)
			end
		end
	end
end

function FtrKuralak:OnStory(strMessage)
	if strMessage=="Kuralak the Defiler vanishes into the shadows." then -- disappear
		if self.uBoss then Print(("Kuralak Pos: %s,%s,%s"):format(self.uBoss:GetPosition().x, self.uBoss:GetPosition().y, self.uBoss:GetPosition().z))
		else Print("Cant find Kuralak") end
	elseif strMessage:find("Kuralak the Defiler returns to the Archive Core to regain strength.") then
		--start vanish timer
		self:ShowTimer(
			"Timer_Vanish",
			"Vanish",
			46,
			{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
		)
	elseif strMessage == "Kuralak the Defiler causes a violent outbreak of corruption!" then
		self:ShowWarning("Outbreak!")
		self.OutbreakCounter = self.OutbreakCounter + 1
		if self.SecondOutbreak then self.SecondOutbreak = false
		else self.SecondOutbreak = true end
		if self.SecondOutbreak then
			self:ShowTimer(
				"Timer_DnaSiphon",
				("Tank Interrupt"),
				19,
				{{"OnTimeLeft", 5, "MoveTo", "BigTimer"}}
			)
		end
		local siphontimer, eggtimer
		if self.OutbreakCounter == 1 then siphontimer = 19
		elseif self.OutbreakCounter == 2 then eggtimer = 10 siphontimer = 19
		elseif self.OutbreakCounter == 3 then eggtimer = 39 siphontimer = 19
		elseif self.OutbreakCounter == 5 then eggtimer = 15 siphontimer = 19
		end

		if eggtimer then
			self:ShowTimer(
				"Timer_Eggs",
				("Break Eggs"),
				eggtimer,
				{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
			)
		end
		if siphontimer then
			self:ShowTimer(
				"Timer_Siphon",
				("DNA Siphon CD"),
				siphontimer,
				{{"OnTimeLeft", 5, "MoveTo", "BigTimer"}}
			)
		end
		-- start outbreak timer
		self:ShowTimer(
			"Timer_Outbreak",
			("Outbreak"),
			43,
			{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
		)
	elseif strMessage == "The corruption begins to fester!" then -- eggs
		self:ShowWarning("Break Eggs!")
	elseif strMessage:find("has been anesthetized!") then -- tank interrupt
		self:ShowWarning("Tank Interrupt!")
	elseif strMessage:find("'s death spreads the corruption") then -- death with buff
		self:ShowWarning(("%s died, extra Debuff!"):format(strMessage:sub(-31)))
	end
end

function FtrKuralak:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Kuralak the Defiler" then self.uBoss = uUnit end
		end
	end
end

function FtrKuralak:SetPhase(phase)
	if self.EncounterPhase ~= phase then
		if phase == 2 then
			self.SecondOutbreak = false
			self.OutbreakCounter = 0
			-- kill vanish timer
			self:DestroyTimer("Timer_Vanish")
			self:ShowTimer(
				"Timer_Outbreak",
				"Outbreak CD",
				17,
				{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
			)
		end
		if self.EncounterPhase ~= phase then self.EncounterPhase = phase
		else return end
	end
end