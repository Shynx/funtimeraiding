require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"
local Apollo = Apollo
local GameLib = GameLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local ipairs = ipairs
local pairs = pairs
local table = table
local os = os

local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrExperiment = Ftr:RegisterModule("Ftr:Experiment")

function FtrExperiment:OnLoad()
	-- ----------------------
	-- Register Handlers
	-- ----------------------
	self:RegisterEventHandler("Start", "OnStart", self)
	self:RegisterEventHandler("Stop", "OnStop", self)
	self:RegisterEventHandler("EnemyCast", "OnEnemyCast", self)
	self:RegisterEventHandler("PartyDebuff", "OnPartyDebuff", self)
	self:RegisterEventHandler("Story", "OnStory", self)
	self:RegisterStartCondition("UnitEnteredCombat", "Experiment X-89", true)
end

function FtrExperiment:OnStart()
	Print("Experiment X-89 engaged, good luck!")
end

function FtrExperiment:OnStop()
	Print("Wipe on Experiment X-89 detected, resetting Module.")
end

function FtrExperiment:OnEnemyCast(uUnit, uTarget, strCastname, fElapsed, fDuration)
	if uUnit:GetName() == "Experiment X-89" then
		if strCastname == "Resounding Shout" then
			self:ShowWarning("Knockback!")
		end
	end
end

function FtrExperiment:OnPartyDebuff(uUnit, Debuff)
	if Debuff.splEffect:GetName() == "Corruption Globule" then
		self:ShowWarning(("Small Bomb on %s!"):format(uUnit:getName()))
	end
end

function FtrExperiment:OnStory(strMessage)
	if strMessage:find("Experiment X-89 has placed a bomb on ") then -- big bomb warning
		-- get target name
		local strTargetName = (strMessage:sub(37)):sub(-1)
		--if member is currently boss target, say "tankswitch" instead
		local warntext
		if self.uBoss and self.uBoss:GetTarget() and self.uBoss:GetTarget():GetName() == strTargetName then warntext = "Tankswitch!"
		else warntext = ("Big Bomb on %s!"):format(strTargetName) end

		-- raidwarning
		self:ShowWarning(warntext)
		-- timer
		self:ShowTimer("Timer_BigBomb", ("Big Bomb on %s!"):format(strTargetName), 10)
	end
end

function FtrExperiment:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Experiment X-89" then
				self.Boss = uUnit
			end
		end
	end
end