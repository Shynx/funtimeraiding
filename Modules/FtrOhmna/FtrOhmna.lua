require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"
local Apollo = Apollo
local GameLib = GameLib
local GroupLib = GroupLib
local Sound = Sound
local PublicEvent = PublicEvent
local unpack = unpack
local Print = Print
local math = math
local ipairs = ipairs
local pairs = pairs
local table = table
local Vector3 = Vector3
local os = os

local tLoadConditions = {
	{"SubZoneChanged", "%", "Techno-Phaged Laboratories"},
	{"SubZoneChanged", "%", "Bio-Phage Containment Pools"}
}
local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrOhmna = Ftr:RegisterModule("Ftr:Ohmna", tLoadConditions)

function FtrOhmna:OnLoad()
	Print("Ftr:Ohmna loaded.")
	self.Generators = {
		North=Vector3.New(2793, -448, -212),
		East=Vector3.New(2884, -448, -120),
		South=Vector3.New(2793, -448, -29),
		West=Vector3.New(2701, -450, -120)
	}

	-- ----------------------
	-- Register Handlers
	-- ----------------------
	self:RegisterEventHandler("Start", "OnStart")
	self:RegisterEventHandler("Stop", "OnStop")
	self:RegisterEventHandler("Timer", "OnTimer")
	self:RegisterEventHandler("EnemyCast", "OnEnemyCast")
	self:RegisterEventHandler("UnitEnteredCombat", "OnUnitEnteredCombat")
	self:RegisterEventHandler("Story", "OnStoryShow")
	--self:RegisterEventHandler("Health", "OnPhase4Soon", "Dreadphage Ohmna", "18%")

	self:RegisterStartCondition("UnitEnteredCombat", "Dreadphage Ohmna", true)
	self:RegisterStopCondition("UnitEnteredCombat", "Dreadphage Ohmna", false)

	Apollo.RegisterSlashCommand("ftrotest", "OnTest", self)
	Apollo.LoadSprites("Modules/FtrOhmna/FtrOhmnaSprites.xml", "FtrOhmnaSprites")
end

function FtrOhmna:OnTest()
	--self:NewLeechWarning()
	-- self:ShowWarning(os.time())
	-- self.GlobalLeechcount = self.GlobalLeechcount +1
	-- self.PhaseLeechcount = self.PhaseLeechcount +1
	-- self:ShowTimer(
	-- 	("Leech_"..self.GlobalLeechcount),
	-- 	("#%s @"):format(self.PhaseLeechcount),
	-- 	24.5,
	-- 	{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
	-- )
	local test = self.__Ftr.FtrLibDraw:GetUnitCardinalFacing(self.uPlayer)
	Print(test)
end

function FtrOhmna:OnStart()
	Print("Ohmna engaged, good luck!")
	self.EncounterPreviousPhase = 0
	self.GlobalLeechcount = 1
	self.PhaseLeechcount = 1
	self.LowestGenNext = true
	self:SetPhase(1)

	self:ShowWarning("Ohmna engaged!")
	self:ShowTimer(
		("Leech_"..self.GlobalLeechcount),
		("#%s @"):format(self.PhaseLeechcount),
		24.5,
		{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
	)
	self:UpdateGenerators()
	--[[ self.tWarnings[#self.tWarnings+1] = self.__Ftr.FtrRaidwarning.create(	"Phase1Timer",
																("Phase 1"),
																0,
																{strType="countup"},
																self.__Ftr.wSmallTimerWindow:FindChild("WarningWrapper"),
																self.__Ftr,
																"Ftr.xml",
																"SmallTimerWindow.WarningWrapper.Warning") ]]
end

function FtrOhmna:OnStop()
	Print("Wipe on Ohmna detected, resetting Module.")
end

function FtrOhmna:OnTimer()
	self:SetRaidmarks()
	self:UpdateGenerators()

	if self.uPlayer:GetHealth() <= 0 then self:DestroyAllDrawings() end
end

function FtrOhmna:OnEnemyCast(uUnit, uTarget, strCastname, fElapsed, fDuration)
	if uUnit:GetName() == "Dreadphage Ohmna" then
		if strCastname == "Genetic Torrent" then
			local dir = self:GetCardinalByAngle(self.__Trigonometry:GetUnitFacingAngle(uUnit))
			self:ShowWarning(("Beam %s!"):format(dir))
			-- todo: if phase 4, remove lines and draw circles around maws
		elseif strCastname == "Devour" then
			-- self:ShowWarning("Interrupt Ohmna!")
		end
	elseif uUnit:GetName() == "Ravenous Maw of the Dreadphage" and strCastname == "Devour" then
		-- if player is less than 15m away from it
		local wrn
		if math.floor(uUnit:GetPosition().x) == 2758 then wrn = "SOUTHWEST INTERRUPT!"
		elseif math.floor(uUnit:GetPosition().x) == 2793 then wrn = "NORTH INTERRUPT!"
		elseif math.floor(uUnit:GetPosition().x) == 2827 then wrn = "SOUTHEAST INTERRUPT!"
		end

		--if self.__Ftr.FtrLibDraw:DistanceBetweenTwoPoints(self.__Ftr.uPlayer:GetPosition(), uUnit:GetPosition()) <= 25 then
			self:ShowWarning(wrn)
		--end
	end
end

function FtrOhmna:OnStory(strMessage)
	if strMessage == "Dreadphage Ohmna submerges into the Bio-Phage pools" then -- submerge text
		self:SetPhase(2)
	elseif strMessage == "The Archives tremble as Dreadphage Ohmna's tentacles surface around you!" then
		self:SetPhase(3)
	elseif strMessage == "The Archives quake with the furious might of the Dreadphage!" then
		self:SetPhase(4)
	end
end

function FtrOhmna:OnPhase4Soon(uUnit)
	self:ShowWarning("Phase 4 soon!")
end

function FtrOhmna:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Dreadphage Ohmna" then
				self:ShowDrawing("Ohmna_1", "DrawLineToUnitFacing", {uUnit, 0, 30}, {BGColor=self.__Ftr:hexToCColor("ebc806", "1")})
				self:ShowDrawing("Ohmna_2", "DrawLineToUnitFacing", {uUnit, 120, 30}, {BGColor=self.__Ftr:hexToCColor("0692eb", "1")})
				self:ShowDrawing("Ohmna_3", "DrawLineToUnitFacing", {uUnit, -120, 30}, {BGColor=self.__Ftr:hexToCColor("0692eb", "1")})
				if self.EncounterPhase == 2 then
					if self.EncounterPreviousPhase==3 then self:SetPhase(3)
					else self:SetPhase(1) end
				end
			elseif uUnit:GetName() == "Ravenous Maw of the Dreadphage" then
				self:ShowDrawing("Maw_"..uUnit:GetId().."_1", "DrawLineToUnitFacing", {uUnit, 0, 30}, {BGColor=self.__Ftr:hexToCColor("ebc806", "1")})
				self:ShowDrawing("Maw_"..uUnit:GetId().."_2", "DrawLineToUnitFacing", {uUnit, 120, 30}, {BGColor=self.__Ftr:hexToCColor("0692eb", "1")})
				self:ShowDrawing("Maw_"..uUnit:GetId().."_3", "DrawLineToUnitFacing", {uUnit, -120, 30}, {BGColor=self.__Ftr:hexToCColor("0692eb", "1")})
				self:SetPhase(4)
			elseif uUnit:GetName() == "Tentacle of Ohmna" then
				if (self.EncounterPhase == 1 or self.EncounterPhase == 2) then self:SetPhase(3) end
			elseif uUnit:GetName():find("Genetic Mutation") then
				self.nP2AddCount = self.nP2AddCount +1
			elseif uUnit:GetName() == "Phageborn Plasma Leech" then
				local strDir
				if     (math.floor(uUnit:GetPosition().x) == self.Generators["South"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["South"].z) then strDir = "South"
				elseif (math.floor(uUnit:GetPosition().x) == self.Generators["North"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["North"].z) then strDir = "North"
				elseif (math.floor(uUnit:GetPosition().x) == self.Generators["West"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["West"].z) then strDir = "West"
				elseif (math.floor(uUnit:GetPosition().x) == self.Generators["East"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["East"].z) then strDir = "East"
				end
				self:NewLeechWarning()
				self:ShowDrawing(("Leech_"..strDir), "DrawLineBetweenUnitPoint", {self.uPlayer, uUnit:GetPosition()},
					{Sprite="FtrOhmnaSprites:Squid", BGColor=self.__Ftr:hexToCColor("cb2929", "1"), DistanceFactor=8, Width=10, Height=15})
				self:ShowWarning(("Leech %s!"):format(strDir))
			end
		else
			if uUnit:GetName() == "Phageborn Plasma Leech" then
				local strDir
				if     (math.floor(uUnit:GetPosition().x) == self.Generators["South"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["South"].z) then strDir = "South"
				elseif (math.floor(uUnit:GetPosition().x) == self.Generators["North"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["North"].z) then strDir = "North"
				elseif (math.floor(uUnit:GetPosition().x) == self.Generators["West"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["West"].z) then strDir = "West"
				elseif (math.floor(uUnit:GetPosition().x) == self.Generators["East"].x) and (math.floor(uUnit:GetPosition().z) == self.Generators["East"].z) then strDir = "East"
				end
				-- remove red line
				self:DestroyDrawing(("Leech_"..strDir))
			elseif uUnit:GetName():find("Genetic Mutation") then
				self.nP2AddCount = self.nP2AddCount - 1
				Print("add died, "..self.nP2AddCount.." left")
				if (self.nP2AddCount < 1) then
					if self.EncounterPreviousPhase==3 then self:SetPhase(3)
					else self:SetPhase(1) end
				end
			elseif uUnit:GetName() == "Ravenous Maw of the Dreadphage" then
				self:DestroyDrawing(("Maw_"..uUnit:GetId().."_1"))
				self:DestroyDrawing(("Maw_"..uUnit:GetId().."_2"))
				self:DestroyDrawing(("Maw_"..uUnit:GetId().."_3"))
				self:SetPhase(5)
			end
		end
	end
end

function FtrOhmna:SetRaidmarks()
	-- for i=1, GroupLib.GetMemberCount() do
	-- 	if GroupLib.GetUnitForGroupMember(i) and self.ModuleRunning then
	-- 		if self.EncounterPhase == 1 then -- remove marks, mark Generator Leader and two Tanks
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Jiggey" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 1 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(1) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Scyler" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 2 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(2) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Oxxen" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 3 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(3) end
	-- 			end

	-- 		elseif self.EncounterPhase == 2 then -- remove marks, mark healers
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Lyzzi" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 1 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(1) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Taerin" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 2 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(2) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Oxxen" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 3 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(3) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Malagantt" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 4 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(4) end
	-- 			end

	-- 		elseif self.EncounterPhase == 4 then -- mark all tanks
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Jiggey" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 1 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(1) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Failyn" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 2 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(2) end
	-- 			end
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Scyler" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 3 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(3) end
	-- 			end

	-- 		elseif self.EncounterPhase == 5 then -- remove marks, mark skilled player
	-- 			if GroupLib.GetUnitForGroupMember(i):GetName()=="Jiggey" then
	-- 				if GroupLib.GetUnitForGroupMember(i):GetTargetMarker() ~= 1 then GroupLib.GetUnitForGroupMember(i):SetTargetMarker(1) end
	-- 			end
	-- 		end
	-- 	end
	-- end
end

function FtrOhmna:UpdateGenerators()
	-- get where next leech will spawn
	local tNextGenerators = {}
	if self.LowestGenNext then tNextGenerators = self:GetLowestGenerator()
	else tNextGenerators = self:GetHighestGenerator() end

	-- delete old generator lines
	if not tNextGenerators["North"] and self:GetDrawing("Gen_North") then self:DestroyDrawing("Gen_North") end
	if not tNextGenerators["East"] and self:GetDrawing("Gen_East") then self:DestroyDrawing("Gen_East") end
	if not tNextGenerators["South"] and self:GetDrawing("Gen_South") then self:DestroyDrawing("Gen_South") end
	if not tNextGenerators["West"] and self:GetDrawing("Gen_West") then self:DestroyDrawing("Gen_West") end

	local strNextGenerators = ""
	for k, v in pairs(tNextGenerators) do
		-- add to text
		strNextGenerators = strNextGenerators .. " " .. k
		--draw line only if doesnt and no leech there exist
		if (not self:GetDrawing(("Leech_"..k))) then
			self:ShowDrawing(
				("Gen_"..k),
				"DrawLineBetweenUnitPoint",
				{self.uPlayer, self.Generators[k]},
				{DistanceFactor=4,
				 Sprite="FtrOhmnaSprites:"..k,
				 BGColor=self.__Ftr:hexToCColor("ffffff", "0.7"),
				 Width=15,
				 Height=15})
		end
	end
	-- update raid warnings
	if self:GetTimer(("Leech_"..self.GlobalLeechcount)) then
		self:GetTimer(("Leech_"..self.GlobalLeechcount)):SetText(("#%s @ %s"):format(self.PhaseLeechcount, strNextGenerators))
	end

	return strNextGenerators
end

function FtrOhmna:GetPowerLevels()
	local iNorthPower, iEastPower, iSouthPower, iWestPower
	for key, peEvent in pairs(PublicEvent.GetActiveEvents()) do
		if peEvent:GetName() == "From Archives into Armies" then
			for idObjective, peoObjective in pairs(peEvent:GetObjectives()) do
				-- if peoObjective:ShowPercent() or peoObjective:ShowHealthBar() then
				if 		peoObjective:GetShortDescription() == "North Power Core Energy" then iNorthPower = peoObjective:GetCount()
					--local iRequired = peoObjective:GetRequiredCount()
				elseif 	peoObjective:GetShortDescription() == "East Power Core Energy" 	then iEastPower  = peoObjective:GetCount()
				elseif 	peoObjective:GetShortDescription() == "South Power Core Energy" then iSouthPower = peoObjective:GetCount()
				elseif 	peoObjective:GetShortDescription() == "West Power Core Energy" 	then iWestPower  = peoObjective:GetCount()
				end
			end
		end
	end

	return {North=iNorthPower, East=iEastPower, South=iSouthPower, West=iWestPower}
end

function FtrOhmna:GetLowestGenerator()
	local tPowerLevels = self:GetPowerLevels()
	local pos = 0
	local lowestlevel = 0
	local tReturn = {}

	for k,v in self.__Ftr:spairs(tPowerLevels, function(t,a,b) return t[b] < t[a] end) do
		if v > 0 then
			if pos < 1 then
				tReturn[k]=v
				lowestlevel=v
			else
				if v < lowestlevel then
					tReturn = {}
					tReturn[k]=v
					lowestlevel=v
				elseif v == lowestlevel then tReturn[k]=v
				end
			end
			pos = pos + 1
		end
	end

	return tReturn
end

function FtrOhmna:GetHighestGenerator()
	local tPowerLevels = self:GetPowerLevels()
	local pos = 0
	local highestlevel=100
	local tReturn = {}

	for k,v in self.__Ftr:spairs(tPowerLevels, function(t,a,b) return t[b] < t[a] end) do
		if v > 0 then
			if pos < 1 then
				tReturn[k]=v
				highestlevel=v
			else
				if v > highestlevel then
					tReturn = {}
					tReturn[k]=v
					highestlevel=v
				elseif v == highestlevel then tReturn[k]=v
				end
			end
			pos = pos + 1
		end
	end

	return tReturn
end

function FtrOhmna:GetCardinalByAngle(ang)
	    if ((ang > 337.5) or (ang <= 22.5)) then return "North"
	elseif ((ang > 22.5)  and (ang <= 67.5)) then return "Northeast"
	elseif ((ang > 67.5) and (ang <= 112.5)) then return "East"
	elseif ((ang > 112.5) and (ang <= 157.5)) then return "Southeast"
	elseif ((ang > 157.5) and (ang <= 202.5)) then return "South"
	elseif ((ang > 202.5) and (ang <= 247.5)) then return "Southwest"
	elseif ((ang > 247.5) and (ang <= 292.5)) then return "West"
	elseif ((ang > 292.5) and (ang <= 337.5)) then return "Northwest"
	else return ""
	end
end

function FtrOhmna:SetPhase(phase)
	if not self.EncounterPhase then self.EncounterPhase = 1 end

	if phase ~= self.EncounterPhase then
		Print(("Switch to Phase %s!"):format(phase))
		self.EncounterPreviousPhase = self.EncounterPhase

		if phase == 1 then
			self:GetDrawing("Ohmna_1"):Show()
			self:GetDrawing("Ohmna_2"):Show()
			self:GetDrawing("Ohmna_3"):Show()
			local leechtimer
			if self.EncounterPhase == 2 then leechtimer = 34
			else leechtimer = 24.5 end
			self.PhaseLeechcount = 1

			self:ShowTimer(
				("Leech_"..self.GlobalLeechcount),
				("#%s @"):format(self.PhaseLeechcount),
				leechtimer,
				{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
			)
			self:UpdateGenerators()
		elseif phase == 2 then
			self.nP2AddCount = 0
			-- hide ohmna lines
			self:GetDrawing("Ohmna_1"):Hide()
			self:GetDrawing("Ohmna_2"):Hide()
			self:GetDrawing("Ohmna_3"):Hide()
			-- destroy any leech warnings still up
			self:DestroyWarning(self:FindWarning("Leech"))
		elseif phase == 3 then
			self:GetDrawing("Ohmna_1"):Show()
			self:GetDrawing("Ohmna_2"):Show()
			self:GetDrawing("Ohmna_3"):Show()
			if self.EncounterPhase == 2 then
				self.PhaseLeechcount = 1
				self:ShowTimer(
					("Leech_"..self.GlobalLeechcount),
					("#%s @"):format(self.PhaseLeechcount),
					34,
					{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
				)
				self:UpdateGenerators()
			end
		elseif phase == 4 then
			self:GetDrawing("Ohmna_1"):Hide()
			self:GetDrawing("Ohmna_2"):Hide()
			self:GetDrawing("Ohmna_3"):Hide()
			-- local FollowupSpout = {
			-- "FollowupSpout", "Spout", 40,
			-- {	strBarFillColor="FFFF0000",
			-- 	{"OnExpire", "Recall"},
			-- 	{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
			-- }
			-- self:ShowTimer(
			-- 	("Spout"),
			-- 	"Spout",
			-- 	20,
			-- 	{{"OnTimeLeft", 10, "MoveTo", "BigTimer"},
			-- 	 {"OnExpire", 10, "MoveTo", "BigTimer"}}
			-- )
		elseif phase == 5 then
		end
		self.EncounterPhase = phase
	end
end

function FtrOhmna:NewLeechWarning()
	-- destroy old timer if still up
	self:DestroyTimer(("Leech_"..self.GlobalLeechcount))

	self.GlobalLeechcount = self.GlobalLeechcount + 1
	self.PhaseLeechcount = self.PhaseLeechcount + 1

	if self.LowestGenNext then self.LowestGenNext = false
	else self.LowestGenNext = true end

	-- create leech warning
	if self.EncounterPhase ~= 5 then
		if (self.EncounterPhase == 1 and self.PhaseLeechcount > 4) then -- nothing
		else
			self:ShowTimer(
				("Leech_"..self.GlobalLeechcount),
				("#%s @"):format(self.PhaseLeechcount),
				24.5,
				{{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
			)
			self:UpdateGenerators()
		end
	end
end