require "Window"
require "Apollo"
require "GameLib"
require "GroupLib"
require "CColor"
local Apollo = Apollo
local GameLib = GameLib
local setmetatable = setmetatable
local unpack = unpack
local Print = Print
local math = math
local ipairs = ipairs
local pairs = pairs
local table = table
local os = os

-- +10sec Go Down!
-- +15sec first wave

-- +25sec first probe
-- +35sec sec probe
-- +35sec 1st Heal Switch
-- +45sec third probe (reset group 1)
-- +60 2nd Heal Switch
-- +65 sec second wave

-- +75sec first probe
-- +85sec second probe
-- +85 1st Heal Switch
-- +95sec third probe (reset group 2)
-- +110 2nd Heal Switch

-- +115sec miniboss

-- +135sec 1st Heal Switch
-- +125sec first probe
-- +135sec sec probe
-- +145sec third probe (reset group 1)

-- +148.3sec first wave

-- +148.3sec first probe
-- +158.3sec second probe
-- +160 2nd Heal Switch
-- +168.3sec third probe (reset group 2)

-- +181.6 second wave
-- +185 1st heal Switch
-- +191.6sec first probe
-- +201.6sec second probe
-- +210 2nd Heal Switch
-- +211.6sec third probe

-- +215sec miniboss

local tLoadConditions = {
	{"SubZoneChanged", "%", "Halls of the Infinite Mind"}
}
local Ftr = Apollo.GetAddon("FunTimeRaiding")
local FtrSystemDaemons = Ftr:RegisterModule("Ftr:SystemDaemons", tLoadConditions)

function FtrSystemDaemons:OnLoad()
	Print(self:__GetName().." loaded.")
	-- ----------------------
	-- Register Handlers
	-- ----------------------
	self:RegisterEventHandler("Start", "OnStart")
	self:RegisterEventHandler("Stop", "OnStop")
	self:RegisterEventHandler("Resume", "OnResume")
	self:RegisterEventHandler("EnemyCast", "OnEnemyCast")
	self:RegisterEventHandler("SubZoneChanged", "OnZoneChange")
	--self:RegisterEventHandler("Health", "OnLowHealth", "Ultraform Charger", "95%")

	self:RegisterStartCondition("UnitEnteredCombat", "Null System Daemon")
	self:RegisterStartCondition("UnitEnteredCombat", "Binary System Daemon")
	self:RegisterStopCondition("SubZoneChanged", "%", "Datascape")
	self:RegisterStopCondition("UnitEnteredCombat", "Binary System Daemon", false)
	self:RegisterStopCondition("UnitEnteredCombat", "Null System Daemon", false)

	Apollo.RegisterSlashCommand("ftrsdtest", "OnTest", self)
end

function FtrSystemDaemons:OnStart()
	Print("System Daemons engaged, good luck!")
		self.NullSurgeCount=1
		self.BinSurgeCount=1

	local followupFirstGroup = {
		"FollowupFirstGroup", "Group 1 Reset", 102.5,
		{	strBarFillColor="FFFF0000",
			{"OnExpire", "Recall"},
			{"OnExpire", (function() self:ShowWarning("Group 1 Reset!") end)},
			{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
	}
	local followupSecGroup = {
		"FollowupSecGroup", "Group 2 Reset", 102.5,
		{	strBarFillColor="FFFF0000",
			{"OnExpire", "Recall"},
			{"OnExpire", (function() self:ShowWarning("Group 2 Reset!") end)},
			{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
	}
	local followupWave = {"FollowupWave", "Second Wave", 50, {strBarFillColor="FF0041d5", {"OnTimeLeft", 10, "MoveTo", "BigTimer"}}}
	local followupSecWaveTwo = {"FollowupWaveOne", "Second Wave", 50, {strBarFillColor="FF0041d5", {"OnTimeLeft", 5, "MoveTo", "BigTimer"}} }
	local followupSecWaveOne = {"FollowupWaveTwo", "First Wave", 50, {strBarFillColor="FF0041d5",
			{"OnExpire", (function() self:ShowTimer(unpack(followupSecWaveTwo)) end)},
			{"OnTimeLeft", 5, "MoveTo", "BigTimer"}}
	}
	local followupMiniboss = {
		"FollowUpMiniboss", "Miniboss", 100,
		{	strBarFillColor="FF68199b",
			{"OnExpire", "Recall"},
			{"OnExpire", (function() self:ShowTimer(unpack(followupSecWaveOne)) end)},
			{"OnTimeLeft", 10, "MoveTo", "BigTimer"}}
	}

	self:ShowTimer("FirstGroup", "Go Down!", 10, {strBarFillColor="FF319b19", {"OnExpire", (
		function() self:FirstHealNext()	end)}, {"OnTimeLeft", 5, "MoveTo", "BigTimer"}})
	self:ShowTimer("Miniboss", "Minibosses spawning", 115, {strBarFillColor="FF68199b",{"OnExpire", (function()
			self:ShowTimer(unpack(followupMiniboss))
			self:ShowTimer(unpack(followupSecWaveOne))
		end)}, {"OnTimeLeft", 5, "MoveTo", "BigTimer"}})
	--self:ShowTimer("PowerSurge_Null", ("South: Surge #%s coming up!"):format(self.NullSurgeCount),	0, {strType="countup"})
	--self:ShowTimer("PowerSurge_Bin", ("North: Surge #%s coming up!"):format(self.BinSurgeCount), 0, {strType="countup"})
	self:ShowTimer("Group2Reset", "Group 2 Reset", 92.5, {{"OnExpire", bMute=true, (function() self:ShowWarning("Group 2 Reset!") self:ShowTimer(unpack(followupSecGroup)) end)}, {"OnTimeLeft", 5, "MoveTo", "BigTimer"}})
	self:ShowTimer("Group1Reset", "Group 1 Reset", 42, {{"OnExpire", bMute=true, (function() self:ShowWarning("Group 1 Reset!") self:ShowTimer(unpack(followupFirstGroup)) end)}, {"OnTimeLeft", 5, "MoveTo", "BigTimer"}})
	self:ShowTimer("FirstWave", "First Wave", 15, {strBarFillColor="FF0041d5",{"OnExpire", (function() self:ShowTimer(unpack(followupWave)) end)}, {"OnTimeLeft", 5, "MoveTo", "BigTimer"}})
end

function FtrSystemDaemons:FirstHealNext()
	self:ShowTimer(
		"HealSwitch1", "1st Heal Switch", 25,
		{	strBarFillColor="FF319b19", bMute=true,
			{"OnExpire", (function() self:SecHealNext() end)}, -- reset surge counts
			{"OnTimeLeft", 10, "MoveTo", "BigTimer"}})
end

function FtrSystemDaemons:SecHealNext()
	self:ShowTimer(
		"HealSwitch2", "2nd Heal Switch", 25,
		{	strBarFillColor="FF319b19",bMute=true,
			{"OnExpire", (function() self:FirstHealNext() end)}, -- reset surge counts
			{"OnTimeLeft", 10, "MoveTo", "BigTimer"}})
end

function FtrSystemDaemons:OnLowHealth(uUnit)
	Print("Low Health triggered")
end

function FtrSystemDaemons:OnStop()
	Print("Wipe on System Daemons detected, resetting Module.")
end

function FtrSystemDaemons:OnResume()
	Print("Resuming System Daemons Encounter.")
end

function FtrSystemDaemons:OnEnemyCast(uUnit, uTarget, strCastname, fElapsed, fDuration)
	if uUnit:GetName() == "Null System Daemon" then
		if strCastname:find("Power Surge") then
			if self.NullSurgeCount then self.NullSurgeCount = self.NullSurgeCount+1
			else self.NullSurgeCount=1 end
			if (self.NullSurgeCount>=4) then self.NullSurgeCount = 1 end
			--self:ShowTimer("PowerSurge_Null", ("South: Surge #%s coming up!"):format(self.NullSurgeCount), 0, {strType="countup"})
		elseif strCastname == "Disconnect" then
			self:ShowWarning("Tankport South!")
		end
	elseif uUnit:GetName() == "Binary System Daemon" then
		if strCastname:find("Power Surge") then
			if self.BinSurgeCount then self.BinSurgeCount = self.BinSurgeCount+1
			else self.BinSurgeCount=1 end
			if (self.BinSurgeCount>=4) then self.BinSurgeCount = 1 end
			--self:ShowTimer("PowerSurge_Bin", ("North: Surge #%s coming up!"):format(self.BinSurgeCount), 0, {strType="countup"})
		elseif strCastname == "Disconnect" then
			self:ShowWarning("Tankport North!")
		end
	end
end

function FtrSystemDaemons:OnTimer()
	--if null is in range, show cast counter for him
	if self.Ftr.FtrLibDraw:DistanceBetweenTwoPoints(self.Ftr.uPlayer:GetPosition(), self:GetUnit("Null System Daemon")) > 25 then
		for k, v in pairs(self.tWarnings) do
			-- update count
			-- if v:GetName() == "FollowUp" and v.GetDuration >= 50 then self.BinSurgeCount=0 self.NullSurgeCount=0 end
			--[[if v:GetName() == "Rw_PowerSurge_Null" then
				v:MoveTo(	self.Ftr.wSmallTimerWindow:FindChild("WarningWrapper"),
																	self.Ftr,
																	"Ftr.xml",
																	"SmallTimerWindow.WarningWrapper.Warning")
				end]]
		end
	else
		for k, v in pairs(self.tWarnings) do
			--[[if v:GetName() == "Rw_PowerSurge_Null" then v:MoveTo(	self.Ftr.wSmallTimerWindow:FindChild("WarningWrapper"),
																	self.Ftr,
																	"Ftr.xml",
																	"SmallTimerWindow.WarningWrapper.Warning") end]]
		end
	end
	if self.Ftr.FtrLibDraw:DistanceBetweenTwoPoints(self.Ftr.uPlayer:GetPosition(), self:GetUnit("Binary System Daemon")) > 25 then
		for k, v in pairs(self.tWarnings) do
			--[[if v:GetName() == "Rw_PowerSurge_Bin" then v:MoveTo(	self.Ftr.wSmallTimerWindow:FindChild("WarningWrapper"),
																	self.Ftr,
																	"Ftr.xml",
																	"SmallTimerWindow.WarningWrapper.Warning") end]]
		end
	else
		for k, v in pairs(self.tWarnings) do
			--[[if v:GetName() == "Rw_PowerSurge_Null" then v:MoveTo(	self.Ftr.wSmallTimerWindow:FindChild("WarningWrapper"),
																	self.Ftr,
																	"Ftr.xml",
																	"SmallTimerWindow.WarningWrapper.Warning") end]]
		end
	end
end

function FtrSystemDaemons:OnStoryShow(eWindowType, tLines, nDisplayLength)
	--[[ for idx, strCurr in ipairs(tLines) do
		if strCurr then
			if strCurr == "INITIALIZING LOWER GENERATOR ROOMS." then -- get down warning
				-- get down warning
			elseif strCurr == "COMMENCE INTRUDER ERADICATION SEQUENCE." then
				-- circle thingy on me?
			end
		end
	end ]]
end

function FtrSystemDaemons:OnUnitEnteredCombat(uUnit, bInCombat)
	if uUnit then
		if bInCombat then
			if uUnit:GetName() == "Conduction Unit Mk. I" or uUnit:GetName() == "Conduction Unit Mk. II" or uUnit:GetName() == "Conduction Unit Mk. III" then
				--self:AddProbe()
			end
		end
	end
end

function FtrSystemDaemons:OnChangeZoneName(oVar, strNewZone)
	--[[ if strNewZone then
		Print(("New Zone Name: %s"):format(strNewZone))
		if strNewZone == "Upper Infinite Generator Core" then
			Print("Ported to North Generator Room")
			self:SendStackStart(strNewZone)
		elseif strNewZone == "Lower Infinite Generator Core" then
			Print("Ported to South Generator Room")
			self:SendStackStart(strNewZone)
		elseif strNewZone == "Halls of the Infinite Mind" then
			Print("Ported to Bosses")
			self:SendStackStop()
		elseif strNewZone == "Datascape" then
			self:StopModule()
		end
	end ]]
end

function FtrSystemDaemons:ResetPowerSurge()
	-- self.NullSurgeCount = 1
	-- self:ShowTimer("PowerSurge_Null", ("South: Surge #%s coming up!"):format(self.NullSurgeCount), 0, {strType="countup"})
	-- self.BinSurgeCount = 1
	-- self:ShowTimer("PowerSurge_Bin", ("North: Surge #%s coming up!"):format(self.BinSurgeCount), 0, {strType="countup"})
end

function FtrSystemDaemons:SetPhase(phase)
	--[[ if phase ~= self.EncounterPhase then
		Print(("Switch to Phase %s!"):format(phase))
		if phase == 1 then
			self.EncounterPhase = 1
		elseif phase == 2 then
			self.EncounterPhase = 2
		elseif phase == 4 then
			self.EncounterPhase = 4
		end
	end ]]
end

function FtrSystemDaemons:SendStackStart(strZoneName)
	--[[ self.Ftr:SendCommMessage(	{	type = "SystemDaemons:StackStart",
								strRoom = strZoneName,
								sSender = self.Ftr.uPlayer:GetName(),
								fTimestamp = GameLib:GetGameTime()			})]]
end

function FtrSystemDaemons:AddProbe()
	-- create probe warning
	--[[ self.tWarnings[#self.tWarnings] = self.Ftr.FtrRaidwarning.create("Probe",
															("New Probes"),
															10,
															"countdown",
															self.Ftr.wBigTimerWindow,
															self.Ftr) ]]
end